<?php
	include '../includes/config.php';
	include '../includes/head.php';
?>
<script>
    if ($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes'){$(location).attr('href', ara);}
</script>
<?php
	include '../includes/header_nav.php';
?>
<div class="main-content bastards-copy">
<div>
	<h1 class="highlight mobile-hide">The FAT bastards</h1>
</div>
<div class="clearfix"></div>
</div>
<div class="wine-headline hidden-desktop">
	<h1>Very Important bastards</h1>
</div>
<div class="wines">
	<div>
<div class="wine-container vib">
	<div class="wine-left">
		<img src="../pieces/vib/ciao/2.jpg" alt="">
	</div>
	<div class="wine-right">
		<div class="top-double">
			<div class="background-strip fb-chardonnay">
		<h1 class="double" >Ciao</h1>
		</div>
		</div>
		<div class="bottom">
		<div class="wine-button-container"><a href="#fbChardonay" rel="vib" class="vib-fancy"><button class="chardonnay">Read More</button></a></div>
	</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="wine-divider"></div>
<div class="wine-container vib">
	<div class="wine-left">
		<img src="../pieces/vib/doppio-zero/3.jpg" alt="">
	</div>
	<div class="wine-right">
		<div class="top-double">
			<div class="background-strip fb-chardonnay">
		<h1 class="double" >Doppio Zero</h1>
		</div>
		</div>
		<div class="bottom">
		<div class="wine-button-container"><a href="#savBlanc" rel="vib" class="vib-fancy"><button class="sauvignon">Read More</button></a></div>
	</div>
	</div>
	<div class="clearfix"></div>

</div>
<div class="wine-divider"></div>
<div class="wine-container vib">
	<div class="wine-left">
		<img src="../pieces/vib/fishmonger-rosebank/2.jpg" alt="">
	</div>
	<div class="wine-right">
		<div class="top-double">
			<div class="background-strip fb-chardonnay">
		<h1 class="double" >Fishmonger</h1>
		</div>
		</div>
		<div class="bottom">
		<div class="wine-button-container"><a href="#piotNoirRose" rel="vib" class="vib-fancy"><button class="pinot-rose">Read More</button></a></div>
	</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="wine-divider"></div>
<div class="wine-container vib">
	<div class="wine-left">
		<img src="../pieces/vib/full-stop-cafe/2.jpg" alt="">
	</div>
	<div class="wine-right">
		<div class="top-double">
			<div class="background-strip fb-chardonnay">
		<h1 class="double" >Full Stop Cafe</h1>
		</div>
		</div>
				<div class="bottom">
		<div class="wine-button-container"><a href="#piotNoir" rel="vib" class="vib-fancy"><button class="pinot-noir">Read More</button></a></div>
	</div>
	</div>
	<div class="clearfix"></div>

</div>
<div class="wine-divider"></div>
<div class="wine-container vib">
	<div class="wine-left">
		<img src="../pieces/vib/ghazal/2.jpg" alt="">
	</div>
	<div class="wine-right">
		<div class="top-double">
			<div class="background-strip fb-chardonnay">
		<h1 class="double" >Ghazal</h1>
		</div>
		</div>
				<div class="bottom">
		<div class="wine-button-container"><a href="#pinotage" rel="vib" class="vib-fancy"><button class="pinotage">Read More</button></a></div>
	</div>
	</div>
	<div class="clearfix"></div>

</div>
<div class="wine-divider"></div>
<div class="wine-container vib">
	<div class="wine-left">
		<img src="../pieces/vib/jbs-corner-melrose-arch/2.jpg" alt="">
	</div>
	<div class="wine-right">
		<div class="top-double">
			<div class="background-strip fb-chardonnay">
		<h1 class="double" >Jb's Corner</h1>
		</div>
		</div>
			<div class="bottom">
		<div class="wine-button-container"><a href="#cabSav" rel="vib" class="vib-fancy"><button class="cab-sav">Read More</button></a></div>
	</div>
	</div>
	<div class="clearfix"></div>

</div>
<div class="wine-divider"></div>
<div class="wine-container vib">
	<div class="wine-left">
		<img src="../pieces/vib/the-baron/2.jpg" alt="">
	</div>
	<div class="wine-right">
		<div class="top-double">
			<div class="background-strip fb-chardonnay">
		<h1 class="double" >Baron</h1>
		</div>
		</div>
			<div class="bottom">
		<div class="wine-button-container"><a href="#merlot" rel="vib" class="vib-fancy"><button class="merlot">Read More</button></a></div>
	</div>
	</div>
	<div class="clearfix"></div>

</div>
<div class="wine-divider"></div>
<div class="wine-container vib">
	<div class="wine-left">
		<img src="../pieces/vib/the-bull-run/2.jpg" alt="">
	</div>
	<div class="wine-right">
		<div class="top-double">
			<div class="background-strip fb-chardonnay">
		<h1 class="double" >Bull Run</h1>
		</div>
		</div>
				<div class="bottom">
		<div class="wine-button-container"><a href="#shiraz" rel="vib" class="vib-fancy"><button class="shiraz">Read More</button></a></div>
	</div>
	</div>
	<div class="clearfix"></div>

</div>
<div class="wine-divider"></div>


<div class="wine-container vib">
	<div class="wine-left">
		<img src="../pieces/vib/the-harbour-fish-grill/2.jpg" alt="">
	</div>
	<div class="wine-right">
		<div class="top-double">
			<div class="background-strip fb-chardonnay">
		<h1 class="double" >Harbour Fish Grill</h1>
		</div>
		</div>
			<div class="bottom">
		<div class="wine-button-container"><a href="#fish-grill" rel="vib" class="vib-fancy"><button class="fish-grill">Read More</button></a></div>
	</div>
	</div>
	<div class="clearfix"></div>

</div>
<div class="wine-divider"></div>



<div class="wine-container vib">
	<div class="wine-left">
		<img src="../pieces/vib/thrive-cafe/2.jpg" alt="">
	</div>
	<div class="wine-right">
		<div class="top-double">
			<div class="background-strip fb-chardonnay">
		<h1 class="double" >Thrive Cafe</h1>
		</div>
		</div>
			<div class="bottom">
		<div class="wine-button-container"><a href="#thrive-cafe" rel="vib" class="vib-fancy"><button class="thrive-cafe">Read More</button></a></div>
	</div>
	</div>
	<div class="clearfix"></div>

</div>
<div class="wine-divider"></div>

<div class="wine-container vib">
	<div class="wine-left">
		<img src="../pieces/vib/trumps/2.jpg" alt="">
	</div>
	<div class="wine-right">
		<div class="top-double">
			<div class="background-strip fb-chardonnay">
		<h1 class="double" >Trumps Grillhouse</h1>
		</div>
		</div>
			<div class="bottom">
		<div class="wine-button-container"><a href="#trumps" rel="vib" class="vib-fancy"><button class="trumps">Read More</button></a></div>
	</div>
	</div>
	<div class="clearfix"></div>

</div>
<div class="wine-divider"></div>

<div class="wine-container vib">
	<div class="wine-left">
		<img src="../pieces/vib/turn-n-tender/2.jpg" alt="">
	</div>
	<div class="wine-right">
		<div class="top-double">
			<div class="background-strip fb-chardonnay">
		<h1 class="double" >Turn 'n Tender</h1>
		</div>
		</div>
			<div class="bottom">
		<div class="wine-button-container"><a href="#tender" rel="vib" class="vib-fancy"><button class="tender">Read More</button></a></div>
	</div>
	</div>
	<div class="clearfix"></div>

</div>
<div class="wine-divider"></div>

<div class="wine-container vib stamp-container">
<img class="stamp" src="../elements/stamp_3.png" alt="">
</div>

</div>

<div class="clearfix"></div>
		</div>
	</div>
<?php
 include '../includes/page_pieces/vib_lightboxes.php';
 include '../includes/page_pieces/wine_packshots.php';
include '../includes/footer.php';
?>
<script>
	$('a.vib').css({
		'font-weight': '800',
		'color': '#EAB332'
	});

	$('.wine-container button').hover(function(){
		$('.background-strip').removeClass('active');
		$(this).parents('.wine-container').find('.background-strip').toggleClass('active');
	});
$('.thumbs img').click(function(){
	var src = $(this).attr('src').toString().replace('-thumb.jpg', '.jpg');
	console.log(src);
		var width = $(this).parents('.wine-lightbox').find('.large-img').innerWidth();
		var height = $(this).parents('.wine-lightbox').find('.large-img').innerHeight();
		$(this).parents('.wine-lightbox').find('.large-img').attr('src', src);
		$(this).parents('.wine-lightbox').find('.large-img').css({'width': width, 'height': height});
});
</script>
