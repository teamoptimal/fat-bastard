</div>
<!-- end #page -->
<footer>
<div class="footer-nav">
	<ul>
		<li><a class="home" alt="Home" href="<?= $base_path; ?>index.php">HOME</a> | </li>
		<li><a class="live-large" alt="Live Large" href="<?= $base_path; ?>pages/live_large.php">LIVE LARGE</a> | </li>
		<li><a class="bastards" alt="The FAT bastards" href="<?= $base_path; ?>pages/the_fat_bastards.php">THE FAT BASTARDS</a> | </li>
		<li><a class="vib" alt="The Very Important Bastards" href="<?= $base_path;?>pages/very_important_bastards.php">VIB</a> | </li>
		<li><a class="get-in-touch" alt="Get In Touch" href="<?= $base_path; ?>pages/contact.php">GET IN TOUCH</a> | </li>
		<!-- <li><a class="tandc-mav" alt="Terms and Conditions" href="http://www.fatbastardwine.co.za/tandc.html" target="_blank">TERMS &AMP; CONDITIONS</a></li> -->
		<li><a class="tandc-mav" alt="Terms and Conditions" href="<?= $base_path; ?>tandc.html" target="_blank">TERMS &AMP; CONDITIONS</a></li>
	</ul>
</div>
<div class="copyright">
	<div class="copyright-inner">
		<p><i class="fa fa-copyright"></i> 2017 FAT bastard. South Africa. All rights reserved
		<span class="hide-mobile">
<span class="divider">|</span>
					Website by <a target="_blank" href="http://designguru.co.za">Design Guru </a>&amp; <a target="_blank" href="http://www.derrickcapetown.com/">Derrick</a></span>
				</p>
		<a target="_blank" href="http://www.ara.co.za">
			<img class="ara-img" src="<?php echo $base_path;?>elements/ara.png" alt="">
		</a>
	</div>

</div>

	<div id="araModal" class="ara_modal">

	  <!-- Modal content -->
	  <div class="ara_model_outer_border">
	    <div class="ara_modal-content">
	      <div class="ara_top">
	        <div class="ara_hippo"></div>
	      </div>
	      <div class="ara_copy">
	        <div class="ara-copy">
	          <p>To enjoy FAT bastard, you need to be able to <span class="highlight">LIVE LIKE ONE</span>. <br> Living <span class="highlight" style="font-size: 28px;">LARGE</span> requires a salubrious take on the good life. </p>
	          <div class="spacer"></div>
	          <p><i>It comes with <span class="highlight">experience</span> so if you're not there yet, keep at it.</i></p>
	        </div>

	        <div class="ara_born">
	          <!-- <h2>When were you born?</h2> -->
			  <h3>YOU MUST BE OF LEGAL DRINKING AGE TO VISIT THIS WEBSITE:</h3>

			  
			  
			  
	          <form id="age_form" name="age_form">

				<p class="ara_age_not">Please select the box if you are of legal drinking age.</p>

				<p class="ara_age_not" id="countryError">You must be a resident of South Africa to access this portal.</p>

				<p class="ara_age_not" id="ageError">You must be over the age of 18 to access this portal.</p>

			<label>
			<select id="selectCountry" name="country">
						<option value="Afghanistan">Afghanistan</option>
						<option value="Albania">Albania</option>
						<option value="Algeria">Algeria</option>
						<option value="Andorra">Andorra</option>
						<option value="Antigua and Barbuda">Antigua and Barbuda</option>
						<option value="Argentina">Argentina</option>
						<option value="Armenia">Armenia</option>
						<option value="Australia">Australia</option>
						<option value="Austria">Austria</option>
						<option value="Azerbaijan">Azerbaijan</option>
						<option value="Bahamas">Bahamas</option>
						<option value="Bahrain">Bahrain</option>
						<option value="Bangladesh">Bangladesh</option>
						<option value="Barbados">Barbados</option>
						<option value="Belarus">Belarus</option>
						<option value="Belgium">Belgium</option>
						<option value="Belize">Belize</option>
						<option value="Benin">Benin</option>
						<option value="Bhutan">Bhutan</option>
						<option value="Bolivia">Bolivia</option>
						<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
						<option value="Botswana">Botswana</option>
						<option value="Brazil">Brazil</option>
						<option value="Brunei">Brunei</option>
						<option value="Bulgaria">Bulgaria</option>
						<option value="Burkina Faso">Burkina Faso</option>
						<option value="Burundi">Burundi</option>
						<option value="Cambodia">Cambodia</option>
						<option value="Cameroon">Cameroon</option>
						<option value="Canada">Canada</option>
						<option value="Cape Verde">Cape Verde</option>
						<option value="Central African Republic">Central African Republic</option>
						<option value="Chad">Chad</option>
						<option value="Chile">Chile</option>
						<option value="China">China</option>
						<option value="Colombia">Colombia</option>
						<option value="Comoros">Comoros</option>
						<option value="Congo">Congo</option>
						<option value="Costa Rica">Costa Rica</option>
						<option value="Cote d'Ivoire">Cote d'Ivoire</option>
						<option value="Croatia">Croatia</option>
						<option value="Cuba">Cuba</option>
						<option value="Cyprus">Cyprus</option>
						<option value="Czech Republic">Czech Republic</option>
						<option value="Denmark">Denmark</option>
						<option value="Djibouti">Djibouti</option>
						<option value="Dominica">Dominica</option>
						<option value="Dominican Republic">Dominican Republic</option>
						<option value="East Timor">East Timor</option>
						<option value="Ecuador">Ecuador</option>
						<option value="Egypt">Egypt</option>
						<option value="El Salvador">El Salvador</option>
						<option value="Equatorial Guinea">Equatorial Guinea</option>
						<option value="Eritrea">Eritrea</option>
						<option value="Estonia">Estonia</option>
						<option value="Ethiopia">Ethiopia</option>
						<option value="Fiji">Fiji</option>
						<option value="Finland">Finland</option>
						<option value="France">France</option>
						<option value="Gabon">Gabon</option>
						<option value="Gambia">Gambia</option>
						<option value="Georgia">Georgia</option>
						<option value="Germany">Germany</option>
						<option value="Ghana">Ghana</option>
						<option value="Greece">Greece</option>
						<option value="Grenada">Grenada</option>
						<option value="Guatemala">Guatemala</option>
						<option value="Guinea">Guinea</option>
						<option value="Guinea-Bissau">Guinea-Bissau</option>
						<option value="Guyana">Guyana</option>
						<option value="Haiti">Haiti</option>
						<option value="Honduras">Honduras</option>
						<option value="Hong Kong">Hong Kong</option>
						<option value="Hungary">Hungary</option>
						<option value="Iceland">Iceland</option>
						<option value="India">India</option>
						<option value="Indonesia">Indonesia</option>
						<option value="Iran">Iran</option>
						<option value="Iraq">Iraq</option>
						<option value="Ireland">Ireland</option>
						<option value="Israel">Israel</option>
						<option value="Italy">Italy</option>
						<option value="Jamaica">Jamaica</option>
						<option value="Japan">Japan</option>
						<option value="Jordan">Jordan</option>
						<option value="Kazakhstan">Kazakhstan</option>
						<option value="Kenya">Kenya</option>
						<option value="Kiribati">Kiribati</option>
						<option value="North Korea">North Korea</option>
						<option value="South Korea">South Korea</option>
						<option value="Kuwait">Kuwait</option>
						<option value="Kyrgyzstan">Kyrgyzstan</option>
						<option value="Laos">Laos</option>
						<option value="Latvia">Latvia</option>
						<option value="Lebanon">Lebanon</option>
						<option value="Lesotho">Lesotho</option>
						<option value="Liberia">Liberia</option>
						<option value="Libya">Libya</option>
						<option value="Liechtenstein">Liechtenstein</option>
						<option value="Lithuania">Lithuania</option>
						<option value="Luxembourg">Luxembourg</option>
						<option value="Macedonia">Macedonia</option>
						<option value="Madagascar">Madagascar</option>
						<option value="Malawi">Malawi</option>
						<option value="Malaysia">Malaysia</option>
						<option value="Maldives">Maldives</option>
						<option value="Mali">Mali</option>
						<option value="Malta">Malta</option>
						<option value="Marshall Islands">Marshall Islands</option>
						<option value="Mauritania">Mauritania</option>
						<option value="Mauritius">Mauritius</option>
						<option value="Mexico">Mexico</option>
						<option value="Micronesia">Micronesia</option>
						<option value="Moldova">Moldova</option>
						<option value="Monaco">Monaco</option>
						<option value="Mongolia">Mongolia</option>
						<option value="Montenegro">Montenegro</option>
						<option value="Morocco">Morocco</option>
						<option value="Mozambique">Mozambique</option>
						<option value="Myanmar">Myanmar</option>
						<option value="Namibia">Namibia</option>
						<option value="Nauru">Nauru</option>
						<option value="Nepal">Nepal</option>
						<option value="Netherlands">Netherlands</option>
						<option value="New Zealand">New Zealand</option>
						<option value="Nicaragua">Nicaragua</option>
						<option value="Niger">Niger</option>
						<option value="Nigeria">Nigeria</option>
						<option value="Norway">Norway</option>
						<option value="Oman">Oman</option>
						<option value="Pakistan">Pakistan</option>
						<option value="Palau">Palau</option>
						<option value="Panama">Panama</option>
						<option value="Papua New Guinea">Papua New Guinea</option>
						<option value="Paraguay">Paraguay</option>
						<option value="Peru">Peru</option>
						<option value="Philippines">Philippines</option>
						<option value="Poland">Poland</option>
						<option value="Portugal">Portugal</option>
						<option value="Puerto Rico">Puerto Rico</option>
						<option value="Qatar">Qatar</option>
						<option value="Romania">Romania</option>
						<option value="Russia">Russia</option>
						<option value="Rwanda">Rwanda</option>
						<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
						<option value="Saint Lucia">Saint Lucia</option>
						<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
						<option value="Samoa">Samoa</option>
						<option value="San Marino">San Marino</option>
						<option value="Sao Tome and Principe">Sao Tome and Principe</option>
						<option value="Saudi Arabia">Saudi Arabia</option>
						<option value="Senegal">Senegal</option>
						<option value="Serbia and Montenegro">Serbia and Montenegro</option>
						<option value="Seychelles">Seychelles</option>
						<option value="Sierra Leone">Sierra Leone</option>
						<option value="Singapore">Singapore</option>
						<option value="Slovakia">Slovakia</option>
						<option value="Slovenia">Slovenia</option>
						<option value="Solomon Islands">Solomon Islands</option>
						<option value="Somalia">Somalia</option>
						<option value="South Africa" selected="selected">South Africa</option>
						<option value="Spain">Spain</option>
						<option value="Sri Lanka">Sri Lanka</option>
						<option value="Sudan">Sudan</option>
						<option value="Suriname">Suriname</option>
						<option value="Swaziland">Swaziland</option>
						<option value="Sweden">Sweden</option>
						<option value="Switzerland">Switzerland</option>
						<option value="Syria">Syria</option>
						<option value="Taiwan">Taiwan</option>
						<option value="Tajikistan">Tajikistan</option>
						<option value="Tanzania">Tanzania</option>
						<option value="Thailand">Thailand</option>
						<option value="Togo">Togo</option>
						<option value="Tonga">Tonga</option>
						<option value="Trinidad and Tobago">Trinidad and Tobago</option>
						<option value="Tunisia">Tunisia</option>
						<option value="Turkey">Turkey</option>
						<option value="Turkmenistan">Turkmenistan</option>
						<option value="Tuvalu">Tuvalu</option>
						<option value="Uganda">Uganda</option>
						<option value="Ukraine">Ukraine</option>
						<option value="United Arab Emirates">United Arab Emirates</option>
						<option value="United Kingdom">United Kingdom</option>
						<option value="United States">United States</option>
						<option value="Uruguay">Uruguay</option>
						<option value="Uzbekistan">Uzbekistan</option>
						<option value="Vanuatu">Vanuatu</option>
						<option value="Vatican City">Vatican City</option>
						<option value="Venezuela">Venezuela</option>
						<option value="Vietnam">Vietnam</option>
						<option value="Yemen">Yemen</option>
						<option value="Zambia">Zambia</option>
						<option value="Zimbabwe">Zimbabwe</option>
                    </select>
					</label>

					<label>
						<input type="text" id="dateYear" placeholder="YYYY" name="dateYear" maxlength="4" onkeypress="returnDate(event, 'year')">
						<input type="text" id="dateMonth" placeholder="MM" name="dateMonth" maxlength="2"  onkeypress="returnDate(event, 'month')">
						<input type="text" id="dateDay" placeholder="DD" name="dateDay" maxlength="2" onkeypress="returnDate(event, 'day')">
					</label>

							

	            <input type="submit" id="ara_submit" value="Submit" onclick="ageSubmit()">
			  </form>

	          <p class="ara_legal">
	            Not for sale to persons under the age of 18. <br>
	            Drink responsibly.
	          </p>
	          <p class="ara_web">
	            To view the FAT bastard Website for Europe and Asia <a href="http://international.fatbastard.com/">click here</a>. <br>
	            To view the FAT bastard Website for US and Canada <a href="http://fatbastard.com/?country=us">click here</a>.
	          </p>
	          <br style="clear:both;">
	        </div>
	      </div>
	      <!-- <div class="contact-copy competition-copy" id="popup_container">
	      <p id="popup_heading">ARA</p>
	      <p id="popup_content"><i>A LUXURIOUS TRIP FOR TWO ABOARD</i></p>
	      <p id="popup_rovos_copy">THE ROVOS RAIL</p>
	      <hr id="enter_line">
	      <p id="enter_now"><a href="/pages/rovosrail.php"><i><span class="highlight">ENTER NOW!</span></i></a></p>
	      </div> -->
	    </div>
	  </div>

	</div>

	<div id="fixedBand">

		<p id="fixedBandP">You must be over the age of 18 to access this site.</p>

	</div>

</footer>

<script src="/scripts/bxslider/jquery.bxslider.js"></script>

<script src="<?= $base_path;?>scripts/jquery-ui.custom.js"></script>
<script src="<?= $base_path;?>scripts/jquery.mobile-menu.js"></script>
<script src="<?= $base_path;?>scripts/navtabs.js"></script>
<script src="<?= $base_path;?>scripts/jquery.fancybox.pack.js"></script>
<script src="<?= $base_path;?>scripts/placeholder.min.js"></script>

<script src="<?= $base_path;?>scripts/jquery.tinycolorpicker.js"></script>
<script src="<?= $base_path;?>scripts/smooth-scroll.js"></script>
<script src="<?= $base_path;?>scripts/jquery.validate.js"></script>
<script src="<?= $base_path;?>scripts/html2canvas.js"></script>

<script src="<?= $base_path;?>scripts/gevann.js"></script>

<script src="<?= $base_path;?>scripts/jquery.fancybox.js"></script>
<script src="<?= $base_path;?>scripts/postcard.js"></script>
<script src="<?= $base_path;?>scripts/theme.js"></script>
<script src="<?= $base_path;?>scripts/postcard-desktop-ui.js"></script>

<script>
$('input, textarea').placeholder();
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61835092-1', 'auto');
  ga('send', 'pageview');

</script>



<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>


<!--
<script type="text/javascript">
  $( document ).ready(function() {

    var keys = {32: 1,37: 1, 38: 1, 39: 1, 40: 1};
        function preventDefault(e) {
      e = e || window.event;
      if (e.preventDefault)
          e.preventDefault();
      e.returnValue = false;  
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    function disableScroll() {
      if (window.addEventListener) // older FF
          window.addEventListener('DOMMouseScroll', preventDefault, false);
      window.onwheel = preventDefault; // modern standard
      window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
      window.ontouchmove  = preventDefault; // mobile
      document.onkeydown  = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null; 
        window.onwheel = null; 
        window.ontouchmove = null;  
        document.onkeydown = null;  
    }

    if ($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes') {
      var ara_modal = document.getElementById('araModal');
      ara_modal.style.display = "block";
      //disableScroll();
      // $(location).attr('href', ara);
    }

   //  $('#age_form').on('submit', function(event) {
   //    event.preventDefault();

   //    var day = $('#ara_daydropdown').val(),
   //        month = $('#ara_monthdropdown').val(),
   //        year = $('#ara_yeardropdown').val(),
   //        //dob = year+"-"+month+"-"+day+'T12:00:00';
   //        //dob = year+"-"+month+"-"+day;
			// // console.log('dob: '+dob);
   //        dob = new Date(year,month,day);

   //    var today = new Date();
   //    var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
   //    // console.log('today: '+today);
   //    // console.log('dob: '+dob);
   //    // console.log(day+', '+month+', '+year+', '+age);
      
   //    if(age > 17) {
   //      ara_modal.style.display = "none";
   //      //enableScroll();

   //      document.cookie="is_legal=yes";

   //      $('.ara_age_not').hide();
   //    } else {
   //      $('.ara_age_not').show();
   //    }

   //  });

	$('#age_form').on('submit', function(event) {
      event.preventDefault();
      
      if($('#legal_age').is(':checked')) {
        ara_modal.style.display = "none";
        //enableScroll();

        document.cookie="is_legal=yes";

        $('.ara_age_not').hide();
      } else {
        $('.ara_age_not').show();
      }

    });


  });
</script>

-->

<script type="text/javascript">
  $( document ).ready(function() {

    var keys = {32: 1,37: 1, 38: 1, 39: 1, 40: 1};
        function preventDefault(e) {
      e = e || window.event;
      if (e.preventDefault)
          e.preventDefault();
      e.returnValue = false;  
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }



    function disableScroll() {
      if (window.addEventListener) // older FF
          window.addEventListener('DOMMouseScroll', preventDefault, false);
      window.onwheel = preventDefault; // modern standard
      window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
      window.ontouchmove  = preventDefault; // mobile
      document.onkeydown  = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null; 
        window.onwheel = null; 
        window.ontouchmove = null; 
        document.onkeydown = null; 
    }
			
   //  $('#age_form').on('submit', function(event) {
   //    event.preventDefault();

   //    var day = $('#ara_daydropdown').val(),
   //        month = $('#ara_monthdropdown').val(),
   //        year = $('#ara_yeardropdown').val(),
   //        //dob = year+"-"+month+"-"+day+'T12:00:00';
   //        //dob = year+"-"+month+"-"+day;
			// // console.log('dob: '+dob);
   //        dob = new Date(year,month,day);

   //    var today = new Date();
   //    var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
   //    // console.log('today: '+today);
   //    // console.log('dob: '+dob);
   //    // console.log(day+', '+month+', '+year+', '+age);
      
   //    if(age > 17) {
   //      ara_modal.style.display = "none";
   //      //enableScroll();

   //      document.cookie="is_legal=yes";

   //      $('.ara_age_not').hide();
   //    } else {
   //      $('.ara_age_not').show();
   //    }

   //  });

	 
  });
	

	 // ON DATE INPUT, RETURN ONLY VALID CHARS
	 function returnDate(event, type) {
        
        switch(type) {
            case "year":
                var curInput = $("#dateYear");
                break;
            case "month":
                var curInput = $("#dateMonth");
                break;
            case "day":
                var curInput = $("#dateDay");
                break;
        }

        // IF MAX LENGTH OF FIELD
        if (curInput.val().length >= curInput.attr("maxLength") - 1) {
            // REMOVE CARET
            curInput.css("caret-color", "transparent");
        } else {
            // ADD CARET
            curInput.css("caret-color", "#000000");
        }

        // IF NUMBER ENTERED
        if (event.charCode >= 48 && event.charCode <= 57) {
            // RETURN NUMBER
            return event.keyCode;
        } else {
            // RETURN NOTHING
            event.preventDefault();
        }
    }

    // CALCULATE DATE GIVEN DATE OBJECT AS ARG
    function getAge(birthDateObject) {
        var today = new Date();
        var age = today.getFullYear() - birthDateObject.getFullYear();
        var m = today.getMonth() - birthDateObject.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDateObject.getDate())) {
                age--;
        }
        return age;
    }

    // AGE GATE SUBMIT
    function ageSubmit() {
		event.preventDefault();


		// COUNTRY DROPDOWN SELECTION
		var selectedCountry = $("#selectCountry :selected").text();

        // IF ALL DATE FIELDS WERE FILLED
        if ($("#dateYear").val() != "" && $("#dateMonth").val() != "" && $("#dateDay").val() != "") {

            // IF ANY DATE FIELDS OUT OF BOUNDS
            if (parseInt($("#dateMonth").val(), 10) > 12 || parseInt($("#dateDay").val(), 10) > 31 || parseInt($("#dateMonth").val(), 10) <= 0 || parseInt($("#dateDay").val(), 10) <= 0) {
                // INVALID DATE
                $("#ageError").show();

				console.log("invalid date");
            } else {

				console.log("valid date");

                // VALID DATE
                var selectedDate = new Date($("#dateYear").val(), $("#dateMonth").val(), $("#dateDay").val());

				if (selectedCountry == "United States") {

					$("#fixedBandP").html("You must be over the age of 21 to access this portal.");

					console.log("USA");

					if (getAge(selectedDate) >= 21) {
						$("#ageError").hide();

						ara_modal.style.display = "none";
						//enableScroll();

						document.cookie="is_legal=yes";
					} else {
						$("#ageError").html("You must be over the age of 21 to access this portal.");
						$("#ageError").show();
					}

				} else {

					console.log("South Africa");

					$("#fixedBandP").html("You must be over the age of 18 to access this portal.");

					if (getAge(selectedDate) >= 18) {
						$("#ageError").hide();

						ara_modal.style.display = "none";
						//enableScroll();

						document.cookie="is_legal=yes";
					} else {
						$("#ageError").html("You must be over the age of 18 to access this portal.");
						$("#ageError").show();
					}

				}

            }

        } else {
            // INVALID DATE
            $("#ageError").show();
        }
        
    }

</script>

</body>
</html>
