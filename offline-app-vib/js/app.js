window.onload = function() {
  // terms and conditions lightbox
  $('#open_terms').click(function() {
    $('#terms').fadeIn();
  });

  $('#terms-close').click(function() {
    $('#terms').fadeOut();
  });

  // Display the todo items.
  todoDB.open(refreshTodos);

  // Get references to the form elements.
  var newTodoForm = document.getElementById('competition_form');

  // Handle new todo item form submissions.
  if (newTodoForm) {

    // add functionality to checkbox
    var conditionsBox = document.getElementById("squaredTwo-conditions");
    var subscribeEmailBox = document.getElementById("user_subscribe_email");
    var subscribeSMSBox = document.getElementById("user_subscribe_sms");
    var gendermaleBox = document.getElementById("user_gender_male");
    var genderfemaleBox = document.getElementById("user_gender_female");
    var ageBox = document.getElementById("squaredTwo-age");

    // Terms and conditions
    $("#squaredTwo-conditions").click(function() {
      if (conditionsBox.value === "1") {
        conditionsBox.value = "0";
        $("#squaredTwo-conditions").attr('checked', false);
      } else if (conditionsBox.value === "0") {
        conditionsBox.value = "1";
        $("#squaredTwo-conditions").attr('checked', true);
      }
    });

    $("#user_subscribe_email").click(function() {
      if (subscribeEmailBox.value === "1") {
        subscribeEmailBox.value = "0";
        $("#user_subscribe_email").attr('checked', false);
      } else if (subscribeEmailBox.value === "0") {
        subscribeEmailBox.value = "1";
        $("#user_subscribe_email").attr('checked', true);
      }
    });

    $("#user_subscribe_sms").click(function() {
      if (subscribeSMSBox.value === "1") {
        subscribeSMSBox.value = "0";
        $("#user_subscribe_sms").attr('checked', false);
      } else if (subscribeSMSBox.value === "0") {
        subscribeSMSBox.value = "1";
        $("#user_subscribe_sms").attr('checked', true);
      }
    });

    $("#user_gender_male").click(function() {
      $("#user_gender_male").attr('checked', true);
      $("#user_gender_female").attr('checked', false);
      // if (gendermaleBox.value === "1") {
      //   gendermaleBox.value = "0";
      //   $("#user_gender_male").attr('checked', false);
      //   // $("#user_gender_female").attr('checked', true);
      // } else if (gendermaleBox.value === "0") {
      //   gendermaleBox.value = "1";
      //   $("#user_gender_male").attr('checked', true);
      //   // $("#user_gender_female").attr('checked', false);
      // }
    });

    $("#user_gender_female").click(function() {
      $("#user_gender_female").attr('checked', true);
      $("#user_gender_male").attr('checked', false);
      // if (genderfemaleBox.value === "1") {
      //   genderfemaleBox.value = "0";
      //   $("#user_gender_female").attr('checked', false);
      //   // $("#user_gender_male").attr('checked', true);
      // } else if (genderfemaleBox.value === "0") {
      //   genderfemaleBox.value = "1";
      //   $("#user_gender_female").attr('checked', true);
      //   // $("#user_gender_male").attr('checked', false);
      // }
    });


    // news!!
    $("#squaredTwo-age").click(function() {
      if (ageBox.value === "1") {
        ageBox.value = "0";
        $("#squaredTwo-age").attr('checked', false);
      } else if (ageBox.value === "0") {
        ageBox.value = "1";
        $("#squaredTwo-age").attr('checked', true);
      }
    });


    newTodoForm.onsubmit = function(e) {
      e.preventDefault();

      // var gender = $("input[name='user_gender']:checked").val();


      //grab the values
      var name = document.getElementById('user_name').value,
        surname = document.getElementById('user_surname').value,
        email = document.getElementById('user_email').value,
        tel = document.getElementById('user_tel').value,
        year = document.getElementById('yeardropdown').value,
        month = document.getElementById('monthdropdown').value,
        day = document.getElementById('daydropdown').value,
        // gender = 'm',
        // gender = document.getElementById('user_gender').value,
        gender = $('input[name="user_gender"]:checked').val(),
        location = document.getElementById('user_location').value,
        subscribe_email = document.getElementById('user_subscribe_email').value,
        subscribe_sms = document.getElementById('user_subscribe_sms').value,
        tandc = document.getElementById('squaredTwo-conditions').value,
        age = 18,
        //check age agains date
        mydate = new Date();
      mydate.setFullYear(year, month - 1, day);
      var currdate = new Date();
      currdate.setFullYear(currdate.getFullYear() - age);
      // Create a timestamp for the todo item.
      var timestamp = new Date();
      timestamp = timestamp.toString();

      // console.log(news);
      // regex validate email
      function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
      }
      // save email validation result to boolean value
      var validEmail = validateEmail(email);

      // validate phone
      function validateContact(tel) {
        if (tel.length !== 10) {
          return false;
        } else {
          return true;
        }
      }

      // save phone validation result to boolean value
      var validContact = validateContact(tel);

      // displays message if field invalid
      function valid(message) {
        $(".validation").text(message);
        setTimeout(function() {
          $(".validation").text("");
        }, 2000);
      }

      // check the fields (ideally this should be a case not elseif)
      if (name === undefined || name === "") {
        valid("please fill out your name");
        } else if (surname === undefined || surname === "") {
          valid("please fill out your surname");
        } else if (email === undefined || email === "" || validEmail === false) {
          valid("please provide in a valid email address");
        } else if (email === undefined || email === "" || validContact === false) {
          valid("please provide in a valid contact number");
        } else if (year === undefined || year === "") {
          valid("please select a year");
        } else if (month === undefined || month === "") {
          valid("please select a month");
        } else if (day === undefined || day === "") {
          valid("please select a day");
        } else if (location === undefined || location === "") {
          valid("please select a location");
        } else if ((currdate - mydate) < age) {
          valid("Oops, it seems that’s not enough for a FAT bastard. Please check your details and try again.");
        } else if (conditionsBox.value !== "1") {
          valid("please accept the terms and conditions");
      } else {

        // format date of birth for sumission
        dateOfBirth = year + "-" + month + "-" + day;

        // Create the todo item.
        todoDB.createTodo(name, surname, email, tel, gender, dateOfBirth, location, subscribe_email, subscribe_sms, timestamp, function(todo) {
          refreshTodos();
        });

        var fields = [];

        fields['first_name'] = name;
        fields['last_name'] = surname;
        fields['email'] = email;
        fields['mobile'] = tel;
        fields['gender'] = gender;
        fields['dob'] = year+"-"+month+"-"+day;
        fields['province'] = location;
        fields['opt_in_email'] = subscribe_email;
        fields['opt_in_sms'] = subscribe_email;        

        //console.log(fields);

        var string = "first_name="+name+"&last_name="+surname;

        $.ajax({
          type: "POST",
          // dataType: 'json', 
          data: {first_name: name, last_name: surname, email: email, mobile: tel, gender: gender, dob: fields['dob'], province: location, opt_in_email: fields['opt_in_email'], opt_in_sms: fields['opt_in_sms']},
          //data: string,
          url: 'fatb.php',
        }).success(function(data) {
          //console.log(data);
          //alert('success');

        }).fail(function(data) {
          //alert('fail');
        });

        // show the success message
        $('#success').fadeIn('fast');
        setTimeout(function() {
          $('#success').css('display', 'none');
        }, 3000);

        // Reset the input field.
        $('#user_name').val('');
        $('#user_surname').val('');
        $('#user_email').val('');
        $('#user_tel').val('');
        $('#yeardropdown').val('');
        $('#monthdropdown').val('');
        $('#daydropdown').val('');
        $('#user_location').val('');
        $(".validation").text('');
        $("#squaredTwo-conditions").attr('checked', false);
        $("#user_subscribe_sms").attr('checked', false);
        $("#user_subscribe_email").attr('checked', false);
        $("#user_gender_male").attr('checked', false);
        $("#user_gender_female").attr('checked', false);
        if (user_subscribe_email == 0) {
          // console.log('here');
          $('#user_subscribe_email').trigger('click');
        }
        if (user_subscribe_sms == 0) {
          // console.log('here');
          $('#user_subscribe_sms').trigger('click');
        }
        // Don't send the form.
        return false;
      }
    };
  }


  $('#delete').click(function() {
    indexedDB.deleteDatabase('todos');
    $('#form-messages').text('YOUR DATABASE HAS BEEN DELETED');
    setTimeout(function() {
      window.location.href = 'http://fatbastardwine.co.za/offline-app-vib/';
    }, 2000);
  });

  // Update the list of todo items.
  function refreshTodos() {
    todoDB.fetchTodos(function(todos) {
      var todoList = document.getElementById('todo-items');
      // var todoList = document.getElementBy;
      if (todoList) {

        todoList.innerHTML = '';

        for (var i = 0; i < todos.length; i++) {
          // Read the todo items backwards (most recent first).
          var todo = todos[(todos.length - 1 - i)];
          // format data for submission on the view page
          json = '';
          json += '{"name"' + ':' + '"' + todo.name + '",' + '\n';
          json += '"surname"' + ':' + '"' + todo.surname + '",' + '\n';
          json += '"email"' + ':' + '"' + todo.email + '",' + '\n';
          json += '"tel"' + ':' + '"' + todo.tel + '",' + '\n';
          json += '"gender"' + ':' + '"' + todo.gender + '",'+ '\n';
          json += '"date_of_birth"' + ':' + '"' + todo.date_of_birth + '",' + '\n';
          json += '"location"' + ':' + '"' + todo.location + '",' + '\n';
          json += '"subscribe_email"' + ':' + '"' + todo.subscribe_email + '",'+ '\n';
          json += '"subscribe_sms"' + ':' + '"' + todo.subscribe_sms + '",'+ '\n';
          json += '"time_stamp"' + ':' + '"' + todo.timestamp + '"';
          if(i == todos.length-1)
            json += "}";
          else
            json += "},";
          // add to json variable
          todoList.innerHTML += json;
        }

        // insert into form
        $('textarea').text("[" + todoList.innerHTML + "]");
      }
    });
  }
};
