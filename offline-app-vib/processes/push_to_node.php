<?php

    // Only process POST reqeusts.
if(isset($_POST['data']) && !empty($_POST['data']))
{
        // Get the form fields and remove whitespace.
        $data = json_decode($_POST["data"]);

        foreach ($data as $key => $obj_consumer) {

            $fields['first_name'] = $obj_consumer->name;
            $fields['last_name'] = $obj_consumer->surname;
            $fields['email'] = $obj_consumer->email;
            $fields['gender']= $obj_consumer->gender;
            $fields['mobile']= $obj_consumer->tel;
            $fields['dob']= $obj_consumer->date_of_birth; //YYYY-MM-DD
            if(isset($obj_consumer->subscribe_email) && !empty($obj_consumer->subscribe_email))
                $fields['opt_in_email'] = 1;  //1 for checked; 0 for unchecked
            else
                $fields['opt_in_email'] = 0;  //1 for checked; 0 for unchecked

            if(isset($obj_consumer->subscribe_sms) && !empty($obj_consumer->subscribe_sms))
                $fields['opt_in_sms'] = 1;  //1 for checked; 0 for unchecked
            else
                $fields['opt_in_sms'] = 0;  //1 for checked; 0 for unchecked$fields['opt_in_email'] = 0;  //1 for checked; 0 for unchecked
            
            $fields['province']= $obj_consumer->location; //Take value from dropdown list

            $fields['interaction_type'] = 'enter';  //This is a change from the previous version
            $fields['interaction_source'] = 'web';
            $fields['return_id'] = 0; 
            $fields['id'] = 'afea51d5-f82e-11e6-ace9-001e676a4028'; //this is the unique form ID for robertson winery.

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://fb-node.optimalonline.co.za/form/consumer");
            curl_setopt($ch, CURLOPT_REFERER, 'http://www.fatbastardwine.co.za');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $user_id = curl_exec ($ch);
            curl_close ($ch);

        }
    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        header("HTTP/1.0 403 forbidden");
        echo "There was a problem with your submission, please try again.";
    }

?>
