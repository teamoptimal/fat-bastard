<?php


$fields['first_name'] = $_POST['first_name'];
$fields['last_name'] = $_POST['last_name'];
$fields['email'] = $_POST['email'];
$fields['mobile']= $_POST['mobile'];
$fields['dob']= $_POST['dob']; //YYYY-MM-DD
$fields['opt_in_email'] = $_POST['opt_in_email'];  //1 for checked; 0 for unchecked
$fields['opt_in_sms'] = $_POST['opt_in_sms'];  //1 for checked; 0 for unchecked
$fields['province']= $_POST['province']; //Take value from dropdown list

// $fields['first_name'] = 'Nicole';
// $fields['last_name'] = 'Test';
// $fields['email'] = 'nicole@optimalonline.co.za';
// $fields['mobile']= '0215557896';
// $fields['dob']= '1982-01-01'; //YYYY-MM-DD
// $fields['opt_in_email'] = 1;  //1 for checked; 0 for unchecked
// $fields['opt_in_sms'] = 1;  //1 for checked; 0 for unchecked
// $fields['province']= 'Western Cape'; //Take value from dropdown list

$fields['interaction_type'] = 'enter';  //This is a change from the previous version
$fields['interaction_source'] = 'web';
$fields['return_id'] = 0; 
$fields['id'] = 'b2ae3a65-9acd-11e6-ace9-001e676a4028'; //this is the unique form ID for robertson winery.

// foreach($social_channel as $key=>$value)
// 	$fields["social_channel[$key]"]=$value;

// foreach($interests as $key=>$value)
// 	$fields["interest[$key]"]=$value;

// echo '<pre>';
// var_dump($fields);
// exit;

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,"https://fb-node.optimalonline.co.za/form/consumer");
// curl_setopt($ch, CURLOPT_URL,"http://rw-node.node.local/form/consumer");
curl_setopt($ch, CURLOPT_REFERER, 'http://www.fatbastardwine.co.za');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$user_id = curl_exec ($ch);
curl_close ($ch);

?>
