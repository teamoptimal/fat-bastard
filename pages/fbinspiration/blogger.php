<?php

    // VALID ID
    if (isset($_GET['id']) && intval($_GET['id']) >= 0 && intval($_GET['id']) <= 8 ) {
        require 'bloggercopy.php';
        $blogger = $inspirationbloggercopyarray[$_GET['id']];

    } else {
        header("Location: http://www.fatbastardwine.co.za/pages/fbinspiration/inspiration");
        die();
    }

	session_start();
	include '../../includes/config.php';
    include '../../includes/head.php';
?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css">

<?php 
// OUTDATED
/*
<?php
if($_REQUEST['source'] && $_REQUEST['source']=='mail')
{
      //$_SESSION['country']=$_REQUEST['country'];
  setcookie('is_legal', 'yes', time() + (7200), "/");
}

	session_start();
	include '../includes/config.php';
	include '../includes/head.php';
	*/
?>
<script>
   //if($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes'){$(location).attr('href', ara);}
</script>

<?php
	include '../../includes/header_nav.php';
?>
<style type="text/css">
	.top-single
	{
		margin-top:80px;
		margin-bottom:43px; 
	}
</style>

<div id="popupBackground" onclick="closePopup()">

</div>

<video id="popupVideo" width="500" height="auto" controls style="cursor: pointer;">
        <source id="popupVideoSource" src="assets/bloggers/<?php echo $_GET['id']; ?>/video.mp4" type="video/mp4">
        Your browser does not support the video tag.
</video>

<div id="bloggerHeader" style="background-color: #<?php echo $blogger['Color']; ?>">

    <div id="bloggerHeaderText">

        <h1 id="bloggerHeaderTextRecipeName"><?php echo $blogger['Recipe Name']; ?></h1>
        <h2 id="bloggerHeaderTextVarietal"><?php echo $blogger['Varietal']; ?></h2>
        <h3 id="bloggerHeaderTextAuthor"><?php echo $blogger['Author']; ?></h3>

        <?php if($blogger['instagram'] != "") { ?>
            <a href="http://www.instagram.com/<?php echo $blogger['instagram']; ?>" target="_blank"><p class="bloggerHeaderTextAuthorDetails">www.instagram.com/<?php echo $blogger['instagram']; ?></p></a>
        <?php } ?>

         <?php if($blogger['Titles'] != "") { ?>
            <p class="bloggerHeaderTextAuthorDetails"><?php echo $blogger['Titles']; ?></p>
        <?php } ?>

        <?php if($blogger['Location'] != "") { ?>
            <p class="bloggerHeaderTextAuthorDetails"><?php echo $blogger['Location']; ?></p>
        <?php } ?>

        <?php if($blogger['Website Link'] != "") { ?>
            <a href="http://<?php echo $blogger['Website Link']; ?>" target="_blank"><p class="bloggerHeaderTextAuthorDetails">Click link for blog & website: <?php echo $blogger['Website Link']; ?></p></a>
        <?php } ?>

        <a href="inspiration.php"><div id="bloggerReturn">

            <p id="bloggerReturnText">RETURN</p>
 
        </div></a>

    </div>

    <div id="bloggerHeaderVideo">

        <?php if($blogger['Video'] == "true") { ?> 

            <video width="500" height="500" onclick="videoPlay()" style="cursor: pointer;" poster="assets/bloggers/<?php echo $_GET['id']; ?>/thumbnail.jpg">
                <source src="assets/bloggers/<?php echo $_GET['id']; ?>/video.mp4" type="video/mp4">
                Your browser does not support the video tag.
            </video>

        <?php } else { ?>
        
            <img src="assets/bloggers/<?php echo $_GET['id']; ?>/1.jpg">
        
        <?php } ?>

    </div>

    <div class="clearfix"></div>

</div>

<div id="bloggerRecipe">

    <div id="bloggerRecipeSide">

        <?php if($blogger['Recipe']['Ingredients Header'] !== "") { ?>
        
            <h4 class="bloggerRecipeHeaderText" style="color: #<?php echo $blogger['Color'];?>"><?php echo $blogger['Recipe']['Ingredients Header']; ?></h4>

            <ul>

                <?php foreach($blogger['Recipe']['Ingredients'] as $ingredient) { ?>
                
                    <li class="bloggerListItem bloggerListItem<?php echo $_GET['id']; ?>"><?php echo $ingredient; ?></li>

                <?php } ?>

            </ul>
        
        <?php } ?>

        <?php if($blogger['Recipe']['Additional Ingredients Header'] !== "") { ?>
            
            <h4 class="bloggerRecipeHeaderText"  style="color: #<?php echo $blogger['Color'];?>"><?php echo $blogger['Recipe']['Additional Ingredients Header']; ?></h4>

            <ul>

                <?php foreach($blogger['Recipe']['Additional Ingredients'] as $ingredient) { ?>
                
                    <li class="bloggerListItem bloggerListItem<?php echo $_GET['id']; ?>"><?php echo $ingredient; ?></li>

                <?php } ?>

            </ul>
    
        <?php } ?>

        <?php if($blogger['Recipe']['Additional Suggestions Header'] !== "") { ?>
            
            <h4 class="bloggerRecipeHeaderText"  style="color: #<?php echo $blogger['Color'];?>"><?php echo $blogger['Recipe']['Additional Suggestions Header']; ?></h4>

            <ul>

                <?php foreach($blogger['Recipe']['Additional Suggestions'] as $suggestion) { ?>
                
                    <li class="bloggerListItem bloggerListItem<?php echo $_GET['id']; ?>"><?php echo $suggestion; ?></li>

                <?php } ?>

            </ul>
    
        <?php } ?>

        <div style="margin: 20px 0;">

            <a class="recipeDownload" href="assets/bloggers/<?php echo $_GET['id']; ?>/recipe.pdf" target="_blank" style="margin: 20px 0; color: #<?php echo $blogger['Color'];?>; font-weight: bold;"><i class="fa fa-download" aria-hidden="true"></i> DOWNLOAD RECIPE</a>

        </div>

    </div>

    <div id="bloggerRecipeMain">

        <h4 class="bloggerRecipeHeaderText"  style="color: #<?php echo $blogger['Color'];?>">METHOD:</h4>

        <ul>

            <?php foreach($blogger['Recipe']['Method'] as $instruction) { ?>
                
                <li class="bloggerListItem bloggerListItem<?php echo $_GET['id']; ?>"><?php echo $instruction; ?></li>

            <?php } ?>

        </ul>

    </div>

    <div id="bloggerRecipeImages" style="margin-bottom: 50px">

        <?php if($blogger['Images'] !== "0") { ?>

            <div id="bloggerRecipesImagesCurrent">

                <img id="currentImage" src="assets/bloggers/<?php echo $_GET['id']; ?>/1.jpg" current-id="1" blogger-id="<?php echo $_GET['id']; ?>" max-images="<?php echo $blogger['Images']; ?>">


                <img id="imagePrev" src="assets/prev.png"> 

                <img id="imageNext" src="assets/next.png">

                <div class="clearfix"></div>

            </div>

            <!-- Slider main container -->
            
            
        <?php } ?>

    </div>

</div>


<div class="clearfix"></div>

<?php
 include '../../includes/page_pieces/wine_lightboxes.php';
 include '../../includes/page_pieces/wine_packshots.php';
include '../../includes/footer.php';
?>

 <script>
    
    if ($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes') {
      var ara_modal = document.getElementById('araModal');
      ara_modal.style.display = "block";

    }

  </script> 
<script>
	var is_safari = navigator.userAgent.indexOf("Safari") > -1;
if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)
 {

 }
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.esm.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.esm.bundle.js"></script>

 <script>

    $( document ).ready(function() {

        var bloggerID = parseInt($('#currentImage').attr("blogger-id"));

        var swiper = new Swiper('.swiper-container', {
        slidesPerView: "auto",
        preventClicks: true,
        simulateTouch: false,
        initialSlide: 0,
        loop: false,
        loopedSlides: 100,
        spaceBetween: 5,
        centeredSlides: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: false,
        },
        on: {
            
        },
        });

        $("#currentImage").attr("current-id", 1);
        $("#currentImage").attr("src", "assets/bloggers/" + bloggerID + "/1.jpg");

        $( "#imagePrev" ).on( "click", function() {
            swiper.slidePrev();
            var prevImage = parseInt($("#currentImage").attr("current-id")) - 1;

            if (prevImage <= 0) {

                prevImage = parseInt($("#currentImage").attr("max-images"));

            }
            $("#currentImage").attr("current-id", prevImage);
            $("#currentImage").attr("src", "assets/bloggers/" + bloggerID + "/" + prevImage + ".jpg");
            swiper.slideTo(prevImage, 500, false);
        });

        $( "#imageNext" ).on( "click", function() {
            var nextImage = parseInt($("#currentImage").attr("current-id")) + 1;

            if (nextImage > parseInt($("#currentImage").attr("max-images"))) {

                nextImage = 1;

            }
            $("#currentImage").attr("current-id", nextImage);
            $("#currentImage").attr("src", "assets/bloggers/" + bloggerID + "/" + nextImage + ".jpg");
            swiper.slideTo(nextImage, 500, false);
        });

    });
    
</script>

<script>

    

     function videoPlay() {
        $("#popupBackground").css("display", "block");
        $("#popupVideo").css("display", "block");

        var vid = document.getElementById("popupVideo");
        vid.currentTime = 0;
    }


    function closePopup() {
        $("#popupBackground").css("display", "none");
        $("#popupVideo").css("display", "none");

        var vid = document.getElementById("popupVideo"); 

        vid.pause();
    }

</script>

<?php 
 ?>

