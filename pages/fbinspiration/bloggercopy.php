<?php 

$inspirationbloggercopyarray = array(
    "0" => array(
        "Color" => "b50937",
        "Video" => "true",
        "Images" => "11",
        "Recipe Name" => "Spiced Plum Tarte Tatin",
        "Varietal" => "FAT <i>bastard</i> Shiraz",
        "Author" => "Bianca Davies",
        "instagram" => "beeblegum",
        "Titles" => "Photographer | Stylist | Videographer | Le Cordon Bleu Paris | Recipe Developer | Blogger",
        "Location" => "",
        "Website Link" => "lnk.bio/vMI2",
        "Recipe" => array(
            "Ingredients Header" => "INGREDIENTS",
            "Ingredients" => [
                '7 large ripe plums',
                '1 Tbsp orange juice',
                '1/4 ground cloves',
                '1 tsp ground cinnamon',
                '1 tsp vanilla seeds',
                '1 sheet puff pastry',
                '100g sugar',
                '50g butter'
            ],
            "Additional Ingredients Header" => "FOR THE SPICED SHIRAZ REDUCTION",
            "Additional Ingredients" => [
                '300ml FAT <i>bastard</i> Shiraz',
                '100g sugar',
                'The zest of 1 orange',
                '4 cloves',
                '2 star anise',
                '2 cinnamon sticks'
            ],
            "Additional Suggestions Header" => "Vanilla bean ice cream to serve",
            "Additional Suggestions" => [
            ],
            "Method" => [
                'Preheat oven to 220˚C.',
                'Choose an oven safe round saucepan / buffet casserole dish.',
                'Measure the diameter and cut out a ring from the sheet of puff
                pastry that will fit snugly in the dish. Place in the fridge.',
                'Slice plums in half and remove pips.',
                'Place in a large bowl along with the orange juice, cloves,
                cinnamon and vanilla. Mix well and set aside.',
                'In your saucepan melt the butter and add the sugar. Stir until
                they become homogenous, foamy and the sugar begins to
                caramelize. Switch off the heat and add the plums snugly. Keep
                the delicious juices from the bowl to add to your sauce later.',
                'Remove the puff pastry from the fridge and cover the plums.',
                'Tuck the edges in around the plums.',
                'Place in the oven and bake for 20-25 minutes until the pastry is
                golden brown and cooked through.',
                'While the tarte is in the oven make your sauce.',
                'Add all the sauce ingredients into a medium sized saucepan
                along with all the spices, vanilla and juices from the plum bowl.',
                'Bring to the boil and reduce to a simmer. Simmer until the sauce
                has reduced by half. Strain out all of the aromatics and pour into
                a serving vessel to cool.',
                'When the tarte is baked remove from the oven and allow to cool.',
                'Don’t let the tarte cool completely or the caramel will set and you
                won’t be able to flip it and remove it from the pan.',
                'So once the tarte is around room temperature place your dish on
                top of the pan and flip it.',
                'Serve the tarte with vanilla bean ice cream and a generous drizzle
                of the Shiraz reduction.'
            ]
        )
    ),
    "1" => array(
        "Color" => "763e5b",
        "Video" => "false",
        "Images" => "7",
        "Recipe Name" => "Chocolate Fondant With Lindor",
        "Varietal" => "FAT <i>bastard</i> Pinotage",
        "Author" => "Sam Linsell",
        "instagram" => "drizzleanddip",
        "Titles" => "Food maker | Photo Taker | Cookbook Author | Delicious Hunter | Dream Seeker | Cape Town, South Africa",
        "Location" => "",
        "Website Link" => "drizzleanddip.com",
        "Recipe" => array(
            "Ingredients Header" => "INGREDIENTS",
            "Ingredients" => [
                '200gms dark chocolate, chopped',
                '100 gms salted butter',
                '1 tsp good quality instant coffee granules or ½ tsp espresso powder',
                '2 free-range eggs',
                '2 free-range egg yolks',
                '½ cup (110gms) caster sugar',
                '¼ cup flour',
                '4 white or milk chocolate Lindt Lindor balls or 3 x squares from a slab',
                'Cocoa for dusting'
            ],
            "Additional Ingredients Header" => "",
            "Additional Ingredients" => [
                
            ],
            "Additional Suggestions Header" => "",
            "Additional Suggestions" => [
                
            ],
            "Method" => [
                'Preheat your oven to 200C / 400F and grease your 4 dariole moulds or ramekins (with aprox capacity of 250ml / 1 cup).',
                'In a small pot melt the butter, coffee and chocolate until smooth.',
                'While that is melting, using an electric beater, whisk the eggs, egg whites and sugar until pale and fluffy – about 5 minutes.',
                'Add the collocate mixture and flour and briefly mix until well combined.',
                'Dived the mixture evenly amongst the 4 moulds and lightly press the Lindt Lindor ball/squares into the mixture in the middle. Bake for 16 – 18 minutes on a baking tray until they have puffed up and firm on the top.',
                'Gently scrape a knife around the edges to loosen and tip out on a plate to serve hot.',
                'This makes 4 generous Fondants, so you could quite easily make 6 – 8 much smaller ones. Just reduce the cooking time down to 12 minutes and halve the quantity of Lindor chocolate you add to the middle.'
            ]
        )
    ),
    "2" => array(
        "Color" => "566c11",
        "Video" => "false",
        "Images" => "4",
        "Recipe Name" => "White Wine Garlic Prawns",
        "Varietal" => "FAT <i>bastard</i> Chardonnay",
        "Author" => "Alida Ryder AKA as Simply Delicious",
        "instagram" => "alidaryder",
        "Titles" => "Creator of Simply Delicious | Recipes | Video | Photography",
        "Location" => "Fan of cheeseburgers and tequila.",
        "Website Link" => "simply-delicious-food.com",
        "Recipe" => array(
            "Ingredients Header" => "Ingredients",
            "Ingredients" => [
                '1 kg prawns/shrimp deveined and peeled',
                '2 tbsp butter',
                '4 garlic cloves crushed',
                '1/3 cup Fat Bastard Chardonnay',
                '1 tbsp parsley finely chopped',
                '1-2 tbsp lemon juice',
                'salt to taste',
                'pepper to taste',
                'lemon slices to serve'
            ],
            "Additional Ingredients Header" => " ",
            "Additional Ingredients" => [
                '<b>Course</b> - Appetizer, Starter',
                '<b>Cuisine</b> - Mediterranean',
                '<b>Keywords</b> - Garlic prawns, Garlic Shrimp, White wine garlick shrimp',
                '<b>Prep Time</b> - 5 minutes',
                '<b>Cook Time</b> - 10 minutes',
                '<b>Total Time</b> - 15 minutes',
                '<b>Servings</b> - 4 People as a starter',
                '<b>Calories</b> - 323 kcal'
            ],
            "Additional Suggestions Header" => "",
            "Additional Suggestions" => [
                
            ],
            "Method" => [
                "Peel and de-vein your prawns if they aren't already, leaving the tail on.",
                "Melt the butter in a large frying pan then add the prawns in a single layer (this might mean cooking them in batches, depending on the size of your pan).",
                "Cook the prawns until golden brown and opaque on both sides then add the garlic",
                "Allow the garlic to cook for 20 seconds then pour in the wine, lemon juice, parsley and season with salt and pepper.",
                "Allow the wine to reduce for a minute or two then serve"
            ]
        )
    ),
    "3" => array(
        "Color" => "cba3ab",
        "Video" => "true",
        "Images" => "6",
        "Recipe Name" => "Colourful Tuna Poke Bowl",
        "Varietal" => "FAT <i>bastard</i> Pinot Noir Rosé",
        "Author" => "Clara Jane Bjorkman",
        "instagram" => "baking_ginger",
        "Titles" => "Blogger | Baker | Recipe Maker | Photo taker | Content Creator ",
        "Location" => "",
        "Website Link" => "baking-ginger.com",
        "Recipe" => array(
            "Ingredients Header" => "For the sauce:",
            "Ingredients" => [
                '2 Tbsp Sesame Oil',
                '1 Tbsp Soy Sauce',
                '2 tsp Honey',
                '1 Tbsp Sesame Seeds',
                '1 tsp crushed garlic',
                '1 tsp grated ginger'
            ],
            "Additional Ingredients Header" => "For the bowl:",
            "Additional Ingredients" => [
                '1 Cup Brown Rice (cooked)',
                '1/2 Avocado',
                'Handful Watercress',
                '1/2 Cup Purple Cabbage (roughly chopped)',
                '5-10 Sugarsnap Peas',
                '1 tsp Sriracha Sauce',
                '70-100g Fresh Tuna (chopped into blocks)',
                '2 tsp sesame seeds'
            ],
            "Additional Suggestions Header" => "",
            "Additional Suggestions" => [
                
            ],
            "Method" => [
                'Combine all the ingredients for the sauce in a small bowl.',
                'Add the tuna and set aside.',
                'Meanwhile, create the poke bowl by placing the rice in the bottom of a large bowl.',
                'Add the the watercress, sugarsnap peas, cabbage and avocado.',
                'Add the marinated tuna.',
                'Drizzle the sriracha sauce over the top and end off with a sprinkle of sesame seeds.',
                'Serve with FAT <i>bastard</i> Pinot Noir Rosé'
            ]
        )
    ),
    "4" => array(
        "Color" => "e5b53b",
        "Video" => "false",
        "Images" => "6",
        "Recipe Name" => "Saffron and Butternut Samp Risotto with Lamb Knuckle Stew",
        "Varietal" => "FAT <i>bastard</i> The Golden Reserve",
        "Author" => "Hein van Tonder",
        "instagram" => "heinstirred",
        "Titles" => "Photographer | Videographer | Editorial Stylist | Glitter Bitch",
        "Location" => "",
        "Website Link" => "heinstirred.com",
        "Recipe" => array(
            "Ingredients Header" => "Lamb Knuckle Stew",
            "Ingredients" => [
                '2 kg lamb knuckles',
                '2 tbsp lamb spice or bbq mix of your choice',
                'Oil for frying',
                '2 large onions, roughly chopped',
                '2 large carrots, sliced',
                '3 stalks of celery, roughly chopped',
                '4 cloves of garlic, mashed',
                'few sprigs of fresh thyme',
                '250ml Fat Bastard The Golden Reserve wine',
                '400g tinned chopped tomatoes',
                '1 tbsp tomato paste',
                '350ml hot stock',
                'Sea salt flakes and freshly ground black pepper to season',
                '1 tbsp sugar',
                'few sprigs of rosemary'
            ],
            "Additional Ingredients Header" => "Saffron and Butternut Samp Risotto",
            "Additional Ingredients" => [
               '400g samp, uncooked, soaked in water for 3 hours and drained',
               '750g butternut, cut into chunks',
               'olive oil for drizzling',
               '1 tsp chilli flakes',
               'sea salt flakes',
               'a knob of butter',
               '15ml extra virgin olive oil',
               '1 large onion, finely chopped',
               'pinch of salt',
               'leaves from 2 sprigs fresh thyme',
               '3 cloves garlic, minced',
               '250ml FAT <i>bastard</i> Sauvignon Blanc (at room temperature)',
               '2 pinches saffron',
               '1l warm vegetable stock'
            ],
            "Additional Suggestions Header" => "To finish",
            "Additional Suggestions" => [
                'a knob of butter',
                '40g grated Parmesan cheese',
                'salt and freshly ground black pepper to taste'
            ],
            "Method" => [
                'Lamb Knuckle Stew:',
                'Preheat the oven to 150 degrees C',
                'Dust the knuckles with the spice mix',
                'Heat some oil in a heavy-based casserole pot with lid and brown the knuckles in batches
                over a medium high heat',
                'Remove from the pot and set aside',
                'In the same pot, add another glug of oil and fry the onions, carrots and celery for about 5 minutes until the onions are translucent',
                'Add the garlic and thyme and fry for a further 5 minutes',
                'Pour in the red wine and scrape off all bits stick to the bottom of the pot',
                'Add the knuckles, bring to the boil and then simmer for 10 minutes with the lid off',
                'Add the tomatoes, tomato paste and stock',
                'Give it a good stir and season well with sea salt flakes and and ground black pepper',
                'Add the sugar and rosemary, place the lid on top and cook for 60 minutes',
                'Remove the lid, and cook for another 90 minutes or so until the lamb is tender and the sauce reduced and thickened',
                'Saffron and Butternut Samp Risotto:',
                'Preheat the oven to 200 degrees C',
                'Place the butternut on a baking sheet, drizzle with the olive oil and sprinkle with the chilli flakes',
                'Give a good mix and roast for 30 – 40 minutes until cooked and caramelised on the edges',
                'Remove from the oven and set aside',
                'Add the butter and olive oil to a large heavy-based pot and melt over medium-high heat',
                'Once melted add the onion and pinch of salt',
                'Cook for about 10 minutes until the onions have softened and are translucent',
                'Add the thyme and garlic and cook for another 5 minutes',
                'Add the samp and give a good stir to coat the samp with the oil and butter',
                'Add the wine in 2 stages, stirring until the wine has been absorbed after each addition',
                'Add the saffron to the stock and give it a stir',
                'Gradually add the stock, ladle by ladle, to the samp only adding the next ladle once the stock is absorbed',
                'Continue over a medium low heat until all the stock has been stirred in and absorbed',
                'The samp should be just cooked',
                '(add more stock if needed until the samp is cooked through)',
                'Stir in the knob of butter and cheese and season to taste',
                'Add the roasted butternut and gently mix in',
                'Serve with the lamb knuckle stew'
            ]
        )
    ),
    "5" => array(
        "Color" => "6e002d",
        "Video" => "false",
        "Images" => "6",
        "Recipe Name" => "Roast Fillet of Beef",
        "Varietal" => "FAT <i>bastard</i> Cabernet Sauvignon",
        "Author" => "Dianne Bibby",
        "instagram" => "bibbyskitchen",
        "Titles" => "Recipe developer |Food Stylist | Food photographer|",
        "Location" => "",
        "Website Link" => "www.bibbyskitchenat36.com",
        "Recipe" => array(
            "Ingredients Header" => "INGREDIENTS",
            "Ingredients" => [
                'olive oil, for roasting',
                '1 kg grass-fed beef fillet',
                '30ml (2 tablespoons) Dijon mustard',
                '5ml (1 teaspoon) freshly cracked black pepper',
                '1 stem rosemary, de-stalked and finely chopped',
                'Maldon sea salt flakes, to season',
                '2 shallots or 1 small brown onion, roughly chopped'
            ],
            "Additional Ingredients Header" => "Red wine sauce",
            "Additional Ingredients" => [
               '350ml FAT <i>bastard</i> Cabernet Sauvignon',
               '125ml (1/2 cup) weak beef stock',
               '3 tablespoons cold butter'
            ],
            "Additional Suggestions Header" => "Roast garlic horseradish cream",
            "Additional Suggestions" => [
                '1 fat clove garlic, wrapped in foil',
                '250g (1 cup) Crème fraîche',
                '30ml (2 tablespoons) horseradish cream',
                '15ml (1 tablespoon) lemon juice',
                '2 stems fennel, roughly chopped',
                'pinch of sea salt flakes'
            ],
            "Method" => [
                'Preheat the oven to 200º C.',
                'Pat the beef fillet dry with kitchen towel. Tie the tenderloin with butchers string at 3cm spacing’s.',
                'Rub the Dijon mustard over the fillet, coating well on all sides. Season generously with salt, black pepper, and rosemary.',
                'Heat an oven-proof pan until hot. Drizzle the fillet with olive oil and brown for 2 minutes per side. (About 4 turns in total)',
                'Add the shallots and garlic clove to the pan and transfer to the oven. For medium-rare, roast uncovered for 23 minutes. Remove the fillet from the pan, cover loosely with a foil tent and rest while you make the sauce.',
                'Remove the garlic and set aside for the cream. Place the pan on the hob, deglaze with the wine and stock. Boil hard to reduce by two thirds. Strain the sauce and discard the solids.',
                'Turn the heat off and add the butter 1 tablespoon at a time, whisking to incorporate. Once the sauce is glossy, remove from the heat and keep warm.',
                'To make the horseradish cream, press the roast garlic into a bowl. Add all the remaining ingredients and whisk to combine.',
                'To serve, drizzle the red wine sauce over the fillet and serve with dollops of Horseradish Crème fraîche.'
            ]
        )
    ),
    "6" => array(
        "Color" => "006a71",
        "Video" => "false",
        "Images" => "3",
        "Recipe Name" => "Smoked Salmon Terrine with Asparagus and Lime",
        "Varietal" => "FAT <i>bastard</i> Sauvignon Blanc",
        "Author" => "Lufuno Sinthumule AKA Chef Funi",
        "instagram" => "cookingwithfuni",
        "Titles" => "Private Chef | Lover Of Food and Wine | Food Product Influencer | Food Artist | Cook Book Author, MC | Culinary lecturer",
        "Location" => "",
        "Website Link" => "www.cookingwithfuni.com",
        "Recipe" => array(
            "Ingredients Header" => "You will need:",
            "Ingredients" => [
                '12 medium asparagus ',
                '4 gelatine leaves',
                '¼ cup water',
                '1 cup crème fraiche',
                '1 cup plain cream cheese',
                '300 g smoked salmon, sliced',
                '100 g smoked salmon, chopped',
                '3 Tsp dill, finely chopped',
                'Juice of 1 lime',
                'Salt and freshly ground pepper for taste',
                '2 limes for garnish'
            ],
            "Additional Ingredients Header" => "",
            "Additional Ingredients" => [
                
            ],
            "Additional Suggestions Header" => " ",
            "Additional Suggestions" => [
               'Gelatine leaves may be substituted with 1tbsp of gelatine powder.'
            ],
            "Method" => [
                'Bring 2 cups of water to boil in a medium saucepan. Season with coarse salt, and add the asparagus. Boil until tender 3 to 4 minutes (depending on thickness). Drain the asparagus in a colander and immediately transfer to ice water for 1 minute to stop the cooking process.',
                'Soak gelatine leaves in a small bowl of water until softened. ',
                'Meanwhile, line a small loaf pan with plastic wrap. Lay the slices of salmon widthways across the base and up the sides, whilst leaving enough hanging over the edges to fold over the top.',
                'In a medium bowl beat the crème fraiche, cottage cheese, chopped salmon and chopped dill until well combined. ',
                'Drain the water from gelatine and place in a small saucepan. Add the lime juice and place in a medium heat just until gelatine has melted, cool slightly and stir in the salmon mixture.',
                'Spoon half of the mixture into the prepared loaf pan. Lay the asparagus on the mixture along the pan, spoon the rest of the salmon mixture and smooth the top. Cover the pan with cling wrap and place in the refrigerator to set for up to 4 hours, preferably overnight.',

            ]
        )
    ),
    "7" => array(
        "Color" => "650360",
        "Video" => "false",
        "Images" => "4",
        "Recipe Name" => "Pan Seared Duck With A Red Wine and Berry Sauce",
        "Varietal" => "FAT <i>bastard</i> Merlot",
        "Author" => "Luyanda Mafanya",
        "instagram" => "@cookingwithluyanda",
        "Titles" => "Food | Lifestyle | Travel ",
        "Location" => "",
        "Website Link" => "Luyandaseatery.worpress.com",
        "Recipe" => array(
            "Ingredients Header" => "Ingredients",
            "Ingredients" => [
                '4 Duck breasts',
                '200ml fat <i>bastard</i> Merlot',
                '1/4 cup blueberries',
                '1/4 cup raspberries',
                '100ml chicken stock',
                '2 Tbsp honey',
                'Salt and pepper',
                'Olive oil'
            ],
            "Additional Ingredients Header" => "For sauce",
            "Additional Ingredients" => [
                'Add red wine, stock, honey and berries to a saucepan over medium heat. Bring to a simmer. Reduce heat and break down the berries, let the sauce reduce by half',
            ],
            "Additional Suggestions Header" => "Serves 4",
            "Additional Suggestions" => [ 
              
            ],
            "Method" => [
                'Preheat oven to 180',
                'Score duck skin in criss cross pattern, season duck with salt & pepper',
                'Heat oil in a pan until oil is hot , then add duck skin-side down. Cook for 3 minutes then turn them over and fry for 2 minutes more on the other side. Transfer duck to an oven proof dish and bake in the preheated oven for 8 minutes.',
                'Once duck is cooked, set it aside and let it rest for 5 minutes.',
                'Slice and serve with berry sauce and a glass of Fat Bastard Merlot'
            ]
        )
    ),
    "8" => array(
        "Color" => "f3d54e",
        "Video" => "false",
        "Images" => "4",
        "Recipe Name" => "Golden Peaches in a Wine Cloud",
        "Varietal" => "FAT <i>bastard</i> Chenin Blanc",
        "Author" => "Herman Lensing from SARIE KOS",
        "instagram" => "",
        "Titles" => " ",
        "Location" => "",
        "Website Link" => "",
        "Recipe" => array(
            "Ingredients Header" => "You will need:",
            "Ingredients" => [
                '200 g (250 ml) white sugar',
                '1 x 750 ml <b>FAT <i>bastard</i> Chenin Blanc</b>',
                '2 cinnamon stick',
                '5 ml vanilla essence',
                '6 yellow peaches, freshly halved',
                '4 egg yolk',
                '100 g (125 ml) caster sugar',
                '125 ml any sweet wine'
            ],
            "Additional Ingredients Header" => " ",
            "Additional Ingredients" => [
                
            ],
            "Additional Suggestions Header" => " ",
            "Additional Suggestions" => [
                'Tip: You can poach any fruit – even berries. Unleash your imagination!'
            ],
            "Method" => [
                'Combine white sugar, <b>FAT <i>bastard</i> Chenin Blanc</b>, cinnamon and vanilla in a medium size pot (preferably a beautiful yellow one like mine)',
                'Turn up the heat to boiling point and stir until sugar dissolved completely.',
                'Reduce heat to a simmer and add peaches to the wine.',
                'Poach for 8 – 12 minutes or until the peaches are soft but not mushy.',
                'Set aside to cool while preparing the wine cloud (a foam sauce is also known as Sabayon).',
                'Position a glass mixing bowl over a pot with simmering water - the base of the bowl should not touch the water.',
                'Combine the egg yolks, caster sugar and wine in the bowl and blend with handheld mixer until a thick foam forms, approximately 8-10 minutes.',
                'Remove the bowl from the heat and keep mixing until it reaches room temperature.',
                'Place cooked peaches on serving plate adding the liquid from the pot.',
                'Spoon the wine cloud over the peaches.',
                'Use a kitchen blowtorch to scorch the cloud until golden brown. Serve immediately.'
            ]
        )
    )
           
);

?>