<?php
	session_start();
	include '../../includes/config.php';
    include '../../includes/head.php';
    require 'bloggercopy.php';
?>

<?php 
// OUTDATED
/*
<?php
if($_REQUEST['source'] && $_REQUEST['source']=='mail')
{
      //$_SESSION['country']=$_REQUEST['country'];
  setcookie('is_legal', 'yes', time() + (7200), "/");
}

	session_start();
	include '../includes/config.php';
	include '../includes/head.php';
	*/
?>
<script>
   //if($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes'){$(location).attr('href', ara);}
</script>

<?php
	include '../../includes/header_nav.php';
?>
<style type="text/css">
	.top-single
	{
		margin-top:80px;
		margin-bottom:43px; 
	}
</style>

<div id="inspirationHeader">

    <img src="assets/header.jpg">

</div>

<div id="inspirationHeaderText" style="background-color: #ffffff !important;">

    <h2 id="inspirationHeaderTextHeader">FAT <i>bastard</i> wine and 9 South African food enthusiasts create 9 unique culinary creations</h2>

    <div style="width: 80%; margin: 0 auto;">

        <p id="inspirationHeaderTextDesktop1">The only thing equal to drinking a glass of FAT <i>bastard</i> wine, is indulging in a delicious meal paired perfectly with FAT <i>bastard</i> wines. <span class="yellowText">FAT <i>bastard</i> is TURNING 21</span> and in celebration of our anniversary we have invited South Africa’s <span class="yellowText">food enthusiasts to create a unique culinary creation</span></p>

        <p id="inspirationHeaderTextDesktop2">of their choice, which would <span class="yellowText">pair perfectly with the FAT <i>bastard</i> wine</span> they were asked to celebrate. Each dish should also reflect the colour of the capsule of the different FAT <i>bastard</i> cultivars. Prepare to be staggered by the amazing results.</p>

        <div class="clearfix"></div>

        <p id="inspirationHeaderTextMobile">The only thing equal to drinking a glass of FAT <i>bastard</i> wine, is indulging in a delicious meal paired perfectly with FAT <i>bastard</i> wines. <span class="yellowText">FAT <i>bastard</i> is TURNING 21</span> and in celebration of our anniversary we have invited South Africa’s <span class="yellowText">food enthusiasts to create a unique culinary creation</span> of their choice, which would <span class="yellowText">pair perfectly with the FAT <i>bastard</i> wine</span> they were asked to celebrate. Each dish should also reflect the colour of the capsule of the different FAT <i>bastard</i> cultivars. Prepare to be staggered by the amazing results.</p>

    </div>

</div>

<?php foreach($inspirationbloggercopyarray as $id => $blogger)  { ?>


    <a href="blogger.php?id=<?php echo $id; ?>">
        <div class="bloggerBox" style="<?php echo $id == "8" ? 'width: 100% !important;' : ''; ?>background-color: <?php echo "#" . $blogger['Color']; ?>">
            <div>

                <h3 class="bloggerBoxRecipeName"><?php echo $blogger['Recipe Name']; ?></h3>
                <h4 class="bloggerBoxRecipeVarietal"><?php echo $blogger['Varietal']; ?></h4>
                <h5 class="bloggerBoxRecipeAuthor"><?php echo $blogger['Author']; ?></h5>

            </div>
        </div>
        </a>

<?php } ?>

<div class="clearfix"></div>

<div id="bloggerPoem">

    <p>Live a colourful life <br> Paint the grey into <i>gold</i></p>

    <p>Live a colourful life <br>Be <i>brave</i>. Be <i>bold</i></p>

    <p>Live a colourful life <br>A life <i>outside</i> the ordinary</p>

    <p>Live a colourful life <br>Step towards the <i>extraordinary</i></p>

    <p>Live a colourful life <br>Have the courage to <i>believe</i></p>

    <p>Live a colourful life <br>Wear your <i>big heart</i> on your sleeve</p>

    <p>Live a colourful life <br>A life <i>rich</i> deep and true</i></p>

    <p id="bloggerPoemLast">Live a colourful life</p>

    <p id="bloggerPoemBig">Live it <i>large</i> and <br> just be you.</p>

    <img src="assets/poem.jpg" id="bloggerPoemImage">

</div>

<?php
 include '../../includes/page_pieces/wine_lightboxes.php';
 include '../../includes/page_pieces/wine_packshots.php';
include '../../includes/footer.php';

?>

 <script>
    
    if ($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes') {
      var ara_modal = document.getElementById('araModal');
      ara_modal.style.display = "block";

    }

  </script> 
  
<script>
	var is_safari = navigator.userAgent.indexOf("Safari") > -1;
if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)
 {

 }
</script>

<?php 
 ?>

