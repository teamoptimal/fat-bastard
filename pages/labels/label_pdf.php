<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Fatbastard Wine label</title>
  <meta name="description" content="Fatbastard Wine label">

  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>

<body style="font-family:times,sans-serif;background-color:transparent;">

	<div style="font-family:times,sans-serif;border:1px solid #ccc;width:389px;height:378px;padding:5px;margin:230px auto 0px;position:relative;">
		<p style="font-family:times,sans-serif;position:absolute;top:63px;right:20px;z-index:5;text-align:right;font-size:18px;line-height:22px;margin-bottom:0;color:#221F1F;">
			<?php echo $label_to; ?>
		</p>
		<p style="font-family:times,sans-serif;position:absolute;top:103px;left:45px;z-index:5;width:85%;font-size:13px;line-height:15px;color:#221F1F;padding:3px 0;">
			<?php 			 
			$new_copy = html_entity_decode($label_copy);
			echo $new_copy;
			?>			
		</p>
		<img src="<?php echo $image_url; ?>" alt="Custom Label" style="position:absolute;top:0;left:10px;z-index:1;width:377.952755906px;height:377.952755906px;"> 
		<!-- <img src="../../elements/labels/shiraz-01_label.png" alt="Custom Label" style="position:absolute;top:0;left:10px;z-index:1;width:377.952755906px;height:377.952755906px;"> -->
	</div>

</body>
</html>