<div class="desktop-live-large">
	<div class="live-large-container">
		<div>
			<div class="" role="tabpanel">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="live-large-nav create-nav">
						<a href="#details" aria-controls="details" role="tab" id="tb_details"><span class="smaller">01.</span> DETAILS</a>
					</li>
					<li role="presentation" class="live-large-nav details-nav">
						<a href="#wines" aria-controls="wines" role="tab" id="tb_wines"><span class="smaller">02.</span> WINES</a>
					</li>
					<li role="presentation" class="live-large-nav occasion-nav">
						<a href="#message" aria-controls="message" role="tab" id="tb_message"><span class="smaller">03.</span> MESSAGE</a>
					</li>
				<!-- 	<li role="presentation" class="live-large-nav design-nav ">
						<a href="#message" aria-controls="message" role="tab" id="tb_message"><span class="smaller">04.</span> MESSAGE</a>
					</li> -->
					<li role="presentation" class="live-large-nav share-nav ">
						<a href="#share" aria-controls="share" role="tab" id="tb_share"><span class="smaller">04.</span> SHARE</a>
					</li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
					<!-- create -->
					<div role="tabpanel" class="live-large-content tab-pane create active" id="create">
						<div class="top">
							<h1>Living Large</h1>
							<div class="left-right">
								<div class="left">
									<!-- <img src="../elements/hippo_postcard.png" alt=""> -->
								</div>
							    <div class="right">

									<p>FAT bastard is about living life filled to the brim. Here is a way you can share more with friends and family, every day.</p>
									
								<ol>
									<li>Choose your favourite FAT bastard.</li><br>
									<li>Compose a heartfelt letter, recite a side-splitting joke, devise a clever limerick or make-up a silly rhyme; write your friend a birthday message or SIMPLY scribble down a little note reminding yourself how marvellous you are.</li><br>
									<li>Download your very own printable FAT bastard label, created especially for you, slap it on a bottle of FAT bastard, and voila! A wine as unique as you are.</li><br>
									<!-- <li>In the spirit of sharing, click SHARE to display your unique bottle of FAT bastard wine on social media for the world to see. Because like we said, LIVING LARGE means never keeping anything bottled up inside.</li> -->
								</ol>
								<div style="display: block; position: absolute; bottom: 20px; left: 30px; z-index: 50;">
									<a class="fancy-close wine-fancy" rel="packshot" href="../elements/labels/mockup1.png" style="display:inline-block;margin-right:50px;">
										<div class="bottle-container" style="display:inline-block;">
											<img src="../elements/labels/mockup1.png" style="width:50px;" alt="Custom Label Mock-up example">
										</div>
									</a>

									<a class="fancy-close wine-fancy" rel="packshot" href="../elements/labels/mockup2.png" style="display:inline-block;">
										<div class="bottle-container" style="display:inline-block;">
											<img src="../elements/labels/mockup2.png" style="width:50px;" alt="Custom Label Mock-up example">
										</div>
									</a>
									<div>Click to enlarge examples.</div>
								</div>
								

								</div>
							</div>
						</div>
						<div class="bottom">
							<div class="hide-ie8">
								<div class="button-container">
									<a id="create-next" aria-controls="create" role="tab" data-toggle="tab">
										<button class="gold">Next</button>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- details -->
					<div role="tabpanel" class="live-large-content details tab-pane" id="details">
						<!--[if lt IE 9]>
						<p style="text-align:center;">Unfortunately your browser does not support this feature. <br/> Please upgrade or <a class="highlight" target="_blank" href="https://www.google.com/chrome/browser/desktop/index.html">switch</a> to a modern browser.</p>
						<![endif]-->
						<div class="hide-ie8">
							<form id="livelargeform" method="post">
								<div class="top">
									<h1>To Start, Please Complete The Form.</h1>
									<div style="width:50%; margin: 0 auto;">
										<h2>Who Might You Be?</h2>
										<div class="left" style="width:49%">
											<input class="text-input" placeholder="NAME *" type="text" name="user_name" id="user_name" required />
										</div>
										<div class="right" style="width:49%">
											<input class="text-input" placeholder="SURNAME *" type="text" name="user_surname" id="user_surname" required />
										</div>
										<input class="text-input" placeholder="EMAIL *" type="email" name="user_email" id="user_email" required />
										<input class="text-input" placeholder="CONTACT NUMBER *" type="text" name="user_tel" id="user_tel" required />
										<input type="hidden" name="day" id="day" value="<?php echo $_SESSION['day']; ?>"/>
										<input type="hidden" name="month" id="month" value="<?php echo $_SESSION['month']; ?>" />
										<input type="hidden" name="year" id="year" value="<?php echo $_SESSION['year']; ?>" />
										<input type="hidden" name="user_location" id="user_location" value="<?php echo $_SESSION['userLocation']; ?>" />
										<div class="clearfix"></div>
										<div class="updates-container">
											<p>Do you consent to FAT bastard keeping in touch?</p>
											<label style="font-size:17px;padding:10px;color:#4F4F4F;">
												<input type="checkbox" value="1" style="margin: 0 15px 15px 0;width: 20px;height: 20px;visibility: visible;" name="user_conditions_email" /> Email</label>
											<label style="font-size:17px;padding:10px;color:#4F4F4F;">
												<input type="checkbox" value="1" style="margin: 0 15px 15px 0;width: 20px;height: 20px;visibility: visible;" name="user_conditions_sms" /> Sms</label>

											<!-- <div class="confirm-cont">
												<div class="squaredTwo squaredTwo-conditions" style="margin:5px 5px 0 0;">
													<input type="checkbox" value="" style="margin:0 15px 15px 0;" id="squaredOne-conditions" name="user_conditions_email" />
													<label for="squaredTwo-conditions"></label>
												</div>
												<p style="margin:0;">Email</p>
												<div class="clearfix"></div>
											</div> -->

											<div class="right">


											</div>
										</div>

									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="bottom">
								<div class="hide-ie8">
									<div class="button-container left">
										<a id="details-back" aria-controls="details">
											<button class="gold">Back</button>
										</a>
									</div>
									<div class="button-container">
										<button value="Submit"  type="submit" name="submit" id="details-next" class="submit gold">Next</button>
										<!-- </a> -->
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- occasion -->
				<div role="tabpanel" class="live-large-content tab-pane" id="occasion">
					<!--[if lt IE 9]>
					<p style="text-align:center; font-size:21px; line-height:30px;">Unfortunately your browser does not support this feature. <br/> Please upgrade or <a class="highlight" target="_blank" href="https://www.google.com/chrome/browser/desktop/index.html">switch</a> to a modern browser.</p>
					<![endif]-->
					<div class="hide-ie8">
						<div class="top">
							<h1 class="occassion-heading">Choose From One Of These Label Designs.</h1>
							<div class="postcard-eg-container">
								<div class="postcard-eg label-eg" id="1" data-id="1">
									<img src="../elements/labels/cab-sauv-01.png" alt="Cabernet Sauvignon">
								</div>
								<div class="postcard-eg label-eg" id="2" data-id="2">
									<img src="../elements/labels/chardonnay-01.png" alt="Chardonnay">
								</div>
								<div class="postcard-eg label-eg" id="3" data-id="3">
									<img src="../elements/labels/merlot-01.png" alt="Merlot">
								</div>
								<div class="postcard-eg label-eg" id="5" data-id="5">
									<img src="../elements/labels/pino-01.png" alt="Pinotage">
								</div>
								<div class="postcard-eg label-eg" id="7" data-id="7">
									<img src="../elements/labels/sauv-blanc-01.png" alt="Sauvignon Blanc">
								</div>
								<div class="postcard-eg label-eg" id="8" data-id="8">
									<img src="../elements/labels/shiraz-01.png" alt="Shiraz">
								</div>
							</div>
						</div>
						<div class="bottom">
							<p style="margin-top: 0;">If you are happy with your label choice, please proceed.</p>
							<div class="clearfix"></div>
							<div class="button-container left">
								<a id="occasion-back" aria-controls="occasion">
									<button class="gold">Back</button>
								</a>
							</div>
							<div class="button-container">
								<a>
									<button id="occasion-next" class="gold">Next</button>
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- design -->
				<div role="tabpanel" class="live-large-content design tab-pane " id="design">
			
					<!--[if lt IE 9]>
					<p style="text-align:center; font-size:21px; line-height:30px;">Unfortunately your browser does not support this feature. <br/> Please upgrade or <a class="highlight" target="_blank" href="https://www.google.com/chrome/browser/desktop/index.html">switch</a> to a modern browser.</p>
					<![endif]-->
					<div class="hide-ie8">
						<div class="top">
							<h1 >Design Your Label.</h1>
							<div class="left-right">
								<div id="postcard" class="left postcard">
								</div>
								<div class="right">
									<p>To</p>
									<input class="text-input user-heading-input" type="text" name="user_heading" id="user_heading" maxlength="30" required />
									<p>Message</p>
									<textarea name="user_message_postcard" rows="5" placeholder="Type Message Here" id="bodyCopy" class="editor body-copy-input label_copy" maxlength="170"></textarea>
									<div id="characters"></div>
									<script>
									$(document).ready(function(){
										$('textarea').keyup(updateCount);
										$('textarea').keydown(updateCount);

										function updateCount() {
										    var cs = $(this).val().length;
										    var max = $(this).attr('maxlength');
										    $('#characters').text(max - cs);
										}
									})
									</script>
									<!-- <div class="left color-left-panel">
										<div class="picker-left-panel">
											<img height="150" width="150" src="../elements/text-color-smaller_2.png" alt="" class="leftColor" />
											<div id="result_test"></div>
										</div>
										<span class="color-left-text"><div class="left-color"></div><p>Left Panel</p></span>
									</div>
									<div class="right color-left-panel">
										<div class="picker-right-panel">
											<img height="150" width="150" src="../elements/text-color-smaller.png" alt="" class="rightColor" />
											<div id="result_test"></div>
										</div>
										<span class="color-right-text"><div class="right-color"></div><p>Right Panel</p></span>
									</div> -->
								</div>
							</div>
						</div>
						<div class="bottom">
							<p style="margin-top:0;">If you are happy with your label design, please proceed.</p>
							<div class="button-container left">
								<a id="design-back" aria-controls="design">
									<button class="gold">Back</button>
								</a>
							</div>
							<!-- form with hidden field to pass to save.php -->
							<form method="POST" enctype="multipart/form-data" action="save.php" id="myForm" encoding="multipart/form-data">
								<input type="hidden" name="img_val" id="img_val" value="" />
								<div class="button-container">
									<a aria-controls="design" role="tab" data-toggle="tab">
										<button id="design-next" class="gold design-next">Next</button>
									</a>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- share -->
				<div role="tabpanel" class="live-large-content tab-pane share" id="share">
					<!--[if lt IE 9]>
					<p style="text-align:center; font-size:21px; line-height:30px;">Unfortunately your browser does not support this feature. <br/> Please upgrade or <a class="highlight" target="_blank" href="https://www.google.com/chrome/browser/desktop/index.html">switch</a> to a modern browser.</p>
					<![endif]-->
					<div class="hide-ie8">

						<div class="top">
							<h1>YOU’RE DONE! DOWNLOAD YOUR LABEL.</h1>
							<p>This may take a few moments.</p>
							<div class="left-right user_share">
							</div>
						</div>
						<div class="bottom">
							<!-- <p style="margin-top:0;">If you are happy with your personalised label, please download it here.</p> -->
							<div class="button-container left">
								<a id="share-back" aria-controls="share">
									<button class="gold">Back</button>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$(".gold").click(function() {
		    $('html, body').animate({
		        scrollTop: $("ul.nav-tabs").offset().top
		    }, 500);
		});
	})
</script>
<!-- end #page -->
<footer class=" desktop-live-large-footer">
	<div class="footer-nav">
		<ul>
			<li><a class="home" alt="Home" href="<?= $base_path; ?>index.php">HOME</a> | </li>
			<li><a class="live-large" alt="Live Large" href="<?= $base_path; ?>pages/live_large.php">LIVE LARGE</a> | </li>
			<li><a class="bastards" alt="The FAT bastards" href="<?= $base_path; ?>pages/the_fat_bastards.php">THE FAT BASTARDS</a> | </li>
			<li><a class="get-in-touch" alt="Get In Touch" href="<?= $base_path; ?>pages/contact.php">GET IN TOUCH</a> | </li>
			<li><a class="tandc-mav" alt="Terms and Conditions" target="_blank"  href="http://www.fatbastardwine.co.za/tandc.html">TERMS &AMP; CONDITIONS</a></li>
		</ul>
	</div>
	<div class="copyright">
		<div class="copyright-inner">
			<p><i class="fa fa-copyright"></i> 2015 FAT bastard. South Africa. All rights reserved
				<span class="hide-mobile">
					<span class="divider">|</span>
					Website by <a target="_blank" href="http://designguru.co.za">Design Guru </a>&amp; <a target="_blank" href="http://www.derrickcapetown.com/">Derrick</a></span>
				</p>
				<a target="_blank" href="http://www.ara.co.za">
					<img class="ara-img" src="<?php echo $base_path;?>elements/ara.png" alt="">
				</a>
			</div>
		</div>
</footer>
</div>
</div>
<script src="<?= $base_path;?>scripts/jquery-ui.custom.js"></script>
<script src="<?= $base_path;?>scripts/jquery.mobile-menu.js"></script>
<script src="<?= $base_path;?>scripts/navtabs.js"></script>
<script src="<?= $base_path;?>scripts/jquery.fancybox.pack.js"></script>
<script src="<?= $base_path;?>scripts/jquery.tinycolorpicker.js"></script>
<script src="<?= $base_path;?>scripts/smooth-scroll.js"></script>
<script src="<?= $base_path;?>scripts/jquery.validate.js"></script>
<script src="<?= $base_path;?>scripts/placeholder.min.js"></script>
<script src="<?= $base_path;?>scripts/gevann.js"></script>
<script src="<?= $base_path;?>scripts/tableExport.js"></script>
<script src="<?= $base_path;?>scripts/jquery.base64.js"></script>

<script src="<?= $base_path;?>scripts/jquery.fancybox.js"></script>
<script src="<?= $base_path;?>scripts/theme.js"></script>

<script src="/scripts/label.js"></script>
<script src="/scripts/label-desktop-ui.js"></script>

<!-- <script src="/scripts/jsPDF-master/main.js"></script> -->
<script src="/scripts/jsPDF-master/dist/jspdf.debug.js"></script>
<!-- <script src="/scripts/sprintf/src/sprintf.js"></script> -->
<script src="/scripts/jsPDF-master/dist/jspdf.min.js"></script>
<!-- <script src="/scripts/jsPDF-master/jspdf.js"></script> -->

<!-- <script type="text/javascript" src="/scripts/jsPDF-master/libs/png_support/zlib.js"></script>
<script type="text/javascript" src="/scripts/jsPDF-master/libs/png_support/png.js"></script>
<script type="text/javascript" src="/scripts/jsPDF-master/plugins/addimage.js"></script>
<script type="text/javascript" src="/scripts/jsPDF-master/plugins/png_support.js"></script> -->

<!-- <script src="/scripts/jsPDF-master/plugins/addhtml.js"></script> -->
<script src="<?= $base_path;?>scripts/html2canvas.js"></script>
<!-- <script src="http://html2canvas.hertzen.com/build/html2canvas.js"></script> -->

<!-- <script src="<?= $base_path;?>scripts/filesaver/FileSaver.js"></script> -->

<script>
$('input, textarea').placeholder();
</script>

<script type="text/javascript">
  $( document ).ready(function() {

    var keys = {32: 1,37: 1, 38: 1, 39: 1, 40: 1};
        function preventDefault(e) {
      e = e || window.event;
      if (e.preventDefault)
          e.preventDefault();
      e.returnValue = false;  
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    function disableScroll() {
      if (window.addEventListener) // older FF
          window.addEventListener('DOMMouseScroll', preventDefault, false);
      window.onwheel = preventDefault; // modern standard
      window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
      window.ontouchmove  = preventDefault; // mobile
      document.onkeydown  = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null; 
        window.onwheel = null; 
        window.ontouchmove = null;  
        document.onkeydown = null;  
    }

    

    $('#age_form').on('submit', function(event) {
      event.preventDefault();

      var day = $('#ara_daydropdown').val(),
          month = $('#ara_monthdropdown').val(),
          year = $('#ara_yeardropdown').val(),
          dob = year+"-"+month+"-"+day;

          // dob = new Date(dob);
          dob = new Date(year,month,day);

      var today = new Date();
      var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
      
      if(age > 17) {
        ara_modal.style.display = "none";
        //enableScroll();

        document.cookie="is_legal=yes";

        $('.ara_age_not').hide();
      } else {
        $('.ara_age_not').show();
      }

    });


  });
</script>