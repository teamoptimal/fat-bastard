<?php
include '../includes/config.php';
// echo $_POST['img_val'];
// echo $_POST['img_val'];
//Get the base-64 string from data
$filteredData=substr($_POST['img_val'], strpos($_POST['img_val'], ",")+1);

$image_id = $_POST['image_id'];
$label_to = $_POST['label_to'];
$label_copy = $_POST['label_copy'];

//Decode the string
$unencodedData=base64_decode($filteredData);

//unique file name
$current_date = date('Ymdhis');
$user_image = '../user_images/'.$current_date.'_img.png';
$user_image_share = $base_path.'pages/user_images/'.$current_date.'_img.png';
//Save the image
file_put_contents('../user_images/'.$current_date.'_img.png', $unencodedData);
//file_put_contents($user_image, $unencodedData);


//Get image for pdf
switch ($image_id) {
    case 1:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/cab-sauv-01_label.png';
        $wine_name = 'Cabernet Sauvignon Label';
        break;
    case 2:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/chardonnay-01_label.png';
        $wine_name = 'Chardonnay Label';
        break;
    case 3:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/merlot-01_label.png';
        $wine_name = 'Merlot Label';
        break;
    case 4:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/pinot-noir-01_label.png';
        $wine_name = 'Pinot Noir Label';
        break;
    case 5:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/pino-01_label.png';
        $wine_name = 'Pinotage Label';
        break;
    case 6:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/pino-noir-rose-01_label.png';
        $wine_name = 'Pinot Noir Rosé Label';
        break;
    case 7:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/sauv-blanc-01_label.png';
        $wine_name = 'Sauvignon Blanc Label';
        break;
    case 8:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/shiraz-01_label.png';
        $wine_name = 'Shiraz Label';
        break;
}

require_once '../dompdf/autoload.inc.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

$dompdf = new Dompdf();

ob_start();
include 'labels/label_pdf.php';
$content = ob_get_clean();

$dompdf = new DOMPDF();
$dompdf->load_html($content);
$dompdf->render();

$output = $dompdf->output();

file_put_contents($_SERVER["DOCUMENT_ROOT"].'/user_pdf/'.$current_date.'_label.pdf', $output);

$user_pdf = '/user_pdf/'.$current_date.'_label.pdf';

?>

<script src="<?= $base_path;?>scripts/filesaver/FileSaver.js"></script>
<script type="text/javascript" src="/scripts/jsPDF-master/libs/png_support/zlib.js"></script>
<script type="text/javascript" src="/scripts/jsPDF-master/libs/png_support/png.js"></script>
<script src="/scripts/jsPDF-master/dist/jspdf.min.js"></script>
<script type="text/javascript" src="/scripts/jsPDF-master/plugins/addimage.js"></script>
<script type="text/javascript" src="/scripts/jsPDF-master/plugins/png_support.js"></script>
<script src="/scripts/jsPDF-master/dist/jspdf.debug.js"></script>
<script src="<?= $base_path;?>scripts/html2canvas.js"></script>
<script src="http://html2canvas.hertzen.com/build/html2canvas.js"></script>

<script>
function createPDF() {
    var doc = new jsPDF();
    var som = new Image();
    som.crossOrigin="anonymous";
    //var img_val = "https://dl.dropboxusercontent.com/u/75159335/1.png"; //TEST LINK
    var img_val = "<?php echo $user_image_share; ?>";
    som.src = img_val;

    //var imgData = 'data:image/png;base64,'+ Base64.encode(som);

    som.onload = function() {
        doc.addImage(this, 40, 60);  // no need to specify image format
        doc.save(<?php echo $current_date ?>+'_img.pdf'); 
    };
    
    // var imgData = 'data:image/jpeg;base64,'+ Base64.encode(som);
    //doc.addImage(som,'PNG', 10, 10);
    var out = doc.output('datauristring');

    //console.log(out);
    //console.log(doc.output('dataurlnewwindow'));
    //doc.save('Image.pdf');
    //$("#pdf_download_btn").attr('href', out);
    //$("#pdf_download_btn").attr('download', out);  
}

//createPDF();

</script>

            <!-- share -->
<!-- 
    <div class="live-large-content share" id="share-mobile">
        <div class="top">
            <h1>Share Your Postcard</h1>
            <div class="left-right">-->


                <div class="left">
                    <div class="postcard-share-mobile" id="postcard-share-mobile">
                        <img src="<?php echo $user_image; ?>" alt="<?php echo $wine_name; ?>" />
                    </div>
                </div>
            <div class="right">              

                <div class="share-box">
                    <h1><i class="highlight">Thank You <br>for <br> Sharing!</i></h1>
                </div>

                <div class="share-button"><a href="<?php echo $user_pdf; ?>" id="pdf_download_btn" dowload="<?php echo $user_pdf; ?>" target="_blank">
                    <div class="share-icon download download_label"></div>
                </a></div>

                <div class="share-button"><div id="share_button" class="share-icon fb fb_label"></div></div>

                <div class="share-button">
                    <a href="http://fatbastardwine.co.za" title="Check out the postcard I just created! @fatbastardSA" class="tweet" target="_blank">
                        <div class="share-icon twitter twitter_label"></div>
                    </a>
                </div>
                <!-- <div class="share-button">
                    <a class="lightbox" href="#lightbox"><div class="share-icon email"></div></a>
                </div> -->
                <!-- <div class="share-button">
                    <a href="<?php echo $user_image; ?>" download="<?php echo $user_image; ?>"><div class="share-icon download"></div></a>
                </div> -->
                <!-- <a href="javascript:void(0);" onclick="createPDF()" id="pdf_download_btn_mobile" target="_blank">
                    <div class="share-icon download"></div>
                </a> -->                
                </div>
            </div>
    <!--    </div>
        <div class="bottom">
            <p>If you are happy with your personalised postcard, please share.</p>
        </div>
    </div> -->

    
<script type="text/javascript">
    $(document).ready(function(){
    var user_image = "<?php echo $image_url; ?>";
    // var word = $( ".word" ).text();
    $('.fb').click(function(e){
    e.preventDefault();
    FB.ui(
    {
    method: 'feed',
    link: 'http://www.fatbastardwine.co.za/pages/live_large.php',
    picture: 'http://www.fatbastardwine.co.za/elements/hippo-small_label.png',
    caption: 'FAT bastard Label',
    description: 'Check out the label creator on the South African FAT bastard website!',
    }
    // ,
    //     function(response){
    //       if(response && response.post_id) {
    //         $(location).attr('href','www.demo.designguru.co.za/fatbastard/web/v3');
    //       }
    //       else {
    //         $(location).attr('href','http://www.demo.designguru.co.za/fatbastard/web/v3');
    //       }
    //     }
    );
    });

    $('a.tweet').click(function(e) {
        //We tell our browser not to follow that link
        e.preventDefault();
        //We get the URL of the link
        var loc = $(this).attr('href');
        //We get the title of the link
        var title = encodeURIComponent($(this).attr('title'));
        //We trigger a new window with the Twitter dialog, in the middle of the page
        window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 225) + ', left=' + $(window).width() / 2 + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    });
    });
    
    

</script>





