<?php

include_once('../../includes/dbal/dlinc.php');
include_once('../../includes/settings.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = false;

$e_updates='';
$m_updates='';

if(isset($_POST['user_conditions_email'])){ 
	$e_updates = 1;
}else{ 
	$e_updates = 0;
}

if(isset($_POST['user_conditions_sms'])){ 
	$m_updates = 1;
}else{ 
	$m_updates = 0;
}

$dl->insert('user_details', array(
	'fname'=>$_POST['user_name'],
	'sname'=>$_POST['user_surname'],
	'email'=>$_POST['user_email'],
	'contact'=>$_POST['user_tel'],
	'b_year'=>$_POST['year'],
	'b_month'=>$_POST['month'],
	'b_day'=>$_POST['day'],
	'province'=>$_POST['user_location'],
	'e_updates'=>$e_updates,
	'date_time_stamp'=>date('Y-m-d H:i:s')
));

var_dump($e_updates);
var_dump($m_updates);

$Birthday = $_POST['year'] . "-" . $_POST['month'] . "-" . $_POST['day'];

$fields['first_name'] = $_POST['user_name'];
$fields['last_name'] = $_POST['user_surname'];
$fields['email'] = $_POST['user_email'];
$fields['mobile']= $_POST['user_tel'];
$fields['dob']= $Birthday; //YYYY-MM-DD
$fields['opt_in_email'] = $e_updates;  //1 for checked; 0 for unchecked
$fields['opt_in_sms'] = $m_updates;  //1 for checked; 0 for unchecked
$fields['province']= $_POST['user_location']; //Take value from dropdown list
$fields['interaction_type'] = 'enter';  //This is a change from the previous version
$fields['interaction_source'] = 'web';
$fields['return_id'] = 0; 
//$fields['additional_info']= $_POST['user_message'];
$fields['id'] = '46017d83-7a6a-11e6-9e19-001e676a4028'; //this is the unique form ID for robertson winery.

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,"https://fb-node.optimalonline.co.za/form/consumer");
// curl_setopt($ch, CURLOPT_URL,"http://rw-node.node.local/form/consumer");
curl_setopt($ch, CURLOPT_REFERER, 'http://www.fatbastardwine.co.za');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$user_id = curl_exec ($ch);
curl_close ($ch);
echo 'User ID: '.$user_id;

?>