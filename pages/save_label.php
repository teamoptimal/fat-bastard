<?php
include '../includes/config.php';
//Get the base-64 string from data
$filteredData=substr($_POST['img_val'], strpos($_POST['img_val'], ",")+1);
$image_id = $_POST['image_id'];
$label_to = $_POST['label_to'];
$label_copy = $_POST['label_copy'];

//Decode the string
$unencodedData=base64_decode($filteredData);
//unique file name
$current_date = date('Ymdhis');
$user_image = '../user_images/'.$current_date.'_img.png';
$user_image_share = $base_path.'user_images/'.$current_date.'_img.png';
//Save the image
file_put_contents('../user_images/'.$current_date.'_img.png', $unencodedData);
// file_put_contents($user_image, $unencodedData);

//Get image for pdf
switch ($image_id) {
    case 1:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/cab-sauv-01_label.png';
        $wine_name = 'Cabernet Sauvignon Label';
        break;
    case 2:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/chardonnay-01_label.png';
        $wine_name = 'Chardonnay Label';
        break;
    case 3:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/merlot-01_label.png';
        $wine_name = 'Merlot Label';
        break;
    case 5:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/pino-01_label.png';
        $wine_name = 'Pinotage Label';
        break;
    case 7:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/sauv-blanc-01_label.png';
        $wine_name = 'Sauvignon Blanc Label';
        break;
    case 8:
        $image_url = $_SERVER["DOCUMENT_ROOT"].'/elements/labels/shiraz-01_label.png';
        $wine_name = 'Shiraz Label';
        break;
}

    // ob_start();
    // include 'labels/label_pdf.php';
    // $content = ob_get_clean();

    // require_once('html2pdf/vendor/autoload.php');
    // use Spipu\Html2Pdf\Html2Pdf;
    // use Spipu\Html2Pdf\Exception\Html2PdfException;
    // use Spipu\Html2Pdf\Exception\ExceptionFormatter;

    // $html2pdf = new Html2Pdf('P','A4','fr');
    // $html2pdf->WriteHTML($content);
    // $html2pdf->Output('exemple.pdf');

    require_once '../dompdf/autoload.inc.php';
    // reference the Dompdf namespace
    use Dompdf\Dompdf;
    use Dompdf\Options;

    // instantiate and use the dompdf class
    //$options = new Options();
    //$options->set('Attachment', 1);
    $dompdf = new Dompdf();

    ob_start();
    include 'labels/label_pdf.php';
    $content = ob_get_clean();

    // $dompdf->loadHtml($content);

    // // (Optional) Setup the paper size and orientation
    // $dompdf->setPaper('A4', 'landscape');

    // // Render the HTML as PDF
    // $dompdf->render();

    // // Output the generated PDF to Browser
    // $dompdf->stream("sample.pdf",array("Attachment"=>0));

    $dompdf = new DOMPDF();
    $dompdf->load_html($content);
    $dompdf->render();

    $output = $dompdf->output();

    file_put_contents($_SERVER["DOCUMENT_ROOT"].'/user_pdf/'.$current_date.'_label.pdf', $output);

    $user_pdf = '/user_pdf/'.$current_date.'_label.pdf';

    $myfile = fopen("/user_page/".$current_date.".php", "w");

    $data = file_get_contents("label_template.php");

    //$newFileContent = str_replace("USER_IMAGE", 'http://www.fatbastard.optimalonline.co.za/user_images/'.$current_date.'_img.png', $data);
    $newFileContent = str_replace("USER_IMAGE", 'http://www.fatbastardwine.co.za/user_images/'.$current_date.'_img.png', $data);

    //$newFileName = $_SERVER["DOCUMENT_ROOT"].'/user_page/'.$current_date.".php";
    //$newFileName = './user_page/'.$current_date.".php";
    //$newFileContent = '<?php echo "something..."; ?//>';

    file_put_contents($_SERVER["DOCUMENT_ROOT"].'/pages/user_page/'.$current_date.'.php', $newFileContent);

    // if (file_put_contents($newFileName, $newFileContent) !== false) {
    //     echo "File created (" . basename($newFileName) . ")";
    // } else {
    //     echo "Cannot create file (" . basename($newFileName) . ")";
    // }

    //fwrite(file,string,length);

?>
<script src="<?= $base_path;?>scripts/filesaver/FileSaver.js"></script>
<script type="text/javascript" src="/scripts/jsPDF-master/libs/png_support/zlib.js"></script>
<script type="text/javascript" src="/scripts/jsPDF-master/libs/png_support/png.js"></script>
<script src="/scripts/jsPDF-master/dist/jspdf.min.js"></script>
<script type="text/javascript" src="/scripts/jsPDF-master/plugins/addimage.js"></script>
<script type="text/javascript" src="/scripts/jsPDF-master/plugins/png_support.js"></script>
<script src="/scripts/jsPDF-master/dist/jspdf.debug.js"></script>
<script src="<?= $base_path;?>scripts/html2canvas.js"></script>
<script src="http://html2canvas.hertzen.com/build/html2canvas.js"></script>


<script>
function createPDF() {
    var doc = new jsPDF();

    var som = new Image();
    som.crossOrigin="anonymous";
    //var img_val = "https://dl.dropboxusercontent.com/u/75159335/1.png"; //TEST LINK
    var img_val = "<?php echo $base_path; ?>elements/labels/<?php echo $image_id; ?>.png";
    som.src = img_val;

    som.onload = function() {
        doc.addImage(this, 40, 60);  // no need to specify image format
        // doc.text(150, 100, "<?php echo $label_to; ?>");
        // doc.text(50, 110, "Lorem \n Ipsum");
        doc.save(<?php echo $current_date ?>+'_img.pdf'); 
    };

    var out = doc.output('datauristring');  
}

</script>
<div class="left">
    <div class="postcard-share" id="postcard-share">
        
        <img id="saved_user_image" alt="<?php echo $wine_name; ?>" src="<?php echo $user_image; ?>" />
    </div>
</div>
<div class="right">

    <div class="share-button" id="downloadLink">
        <!-- <a href="<?php echo $user_image; ?>" download="<?php echo $user_image; ?>"> -->
        <!-- <a href="download.php?file=<?php echo $user_image; ?>" download="<?php echo $user_image; ?>" target="_top">
            <div class="share-icon download"></div>
        </a>
        <br> -->
        <!-- <a href="javascript:void(0);" onclick="createPDF()" id="pdf_download_btn" target="_blank">
            <div class="share-icon download"></div>
        </a> --> 
        <a href="<?php echo $user_pdf; ?>" id="pdf_download_btn" dowload="<?php echo $user_pdf; ?>" target="_blank">
            <div class="share-icon download download_label"></div>
        </a>       
    </div>

    <div class="share-button"><div id="share_button" class="share-icon fb fb_label"></div></div>
    <div class="share-button">
        <a href="http://fatbastardwine.co.za" title="Check out the label I just created! @fatbastardSA" class="tweet" target="_blank">
            <div class="share-icon twitter twitter_label"></div>
        </a>
    </div>
    <!-- <div class="share-button"><a class="lightbox" href="#lightbox"><div class="share-icon email"></div></a></div> -->
    
        <div class="clearfix"></div>
        <div class="share-box">
            <h1><i class="highlight">Almost there. Share the <br>FAT bastard <br>love with your friends.</i></h1>

        </div>
    </div>
    <div id="lightbox" style="display:none;">
        <main class="contact-form fancy-lightbox">
        <section>
            <h1 class="highlight">Email Your Postcard.</h1>
            <!-- <form id="contactformshare" method="post" action="../includes/contactengineshare.php"> -->
            <form name="contactformshare" id="contactformshare" method="post">
                <div class="left">
                    <label for="user_name"></label>
                    <input class="text-input" placeholder="NAME" type="text" name="user_name" id="user_name" required/>
                </div>
                <div class="right">
                    <label for="user_surname"></label>
                    <input class="text-input" placeholder="SURNAME" type="text" name="user_surname" id="user_surname"  required/>
                </div>
                <label for="from_email"></label>
                <input class="text-input" placeholder="YOUR EMAIL" type="email" name="from_email" id="from_email" required/>
                <label for="user_tel"></label>
                <label for="user_email"></label>
                <input class="text-input" placeholder="YOUR FRIENDS EMAIL" type="email" name="user_email" id="user_email" required/>
                <textarea class="text-input" rows="8" placeholder="MESSAGE" type="text" name="user_message" id="user_message"  /></textarea>
                <input type="hidden" type="text" name="user_image" id="user_image" value="<?php echo $user_image_share; ?>"/>
                <div class="clearfix"></div>
                <p id="success"></p>
                <div class="submit-contact-button-container email-fancy">
                    <button type="submit" name="submit" value="Submit" id="submit" class="contact-button">SEND</button>
                </div>
                <div class="clearfix"></div>
            </form>
        </section>
        </main>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){


            $('#submit').click(function(e){

                $('#contactformshare').validate({
                    rules: {
                        user_name:{
                            required: true,
                            minlength: 2,
                        },
                        user_surname:{
                            required: true,
                            minlength: 2,
                        },
                        from_email:{
                            required: true,
                            email: true,
                        },
                        user_email:{
                            required: true,
                            email: true,
                        },
                        user_message:{
                            required: true,
                            minlength: 10,
                        }
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }

                });

                if($('#contactformshare').valid()) {
                    e.preventDefault();
                    $.ajax({
                        type: 'POST',
                        url: '../includes/contactengineshare.php',
                        data: $('#contactformshare').serialize(),
                    });
                    e.preventDefault();
                $('#success').text('Thanks. Your label has been sent.');
                setTimeout(function(){ $('#success').text(''); }, 4000);
                e.preventDefault();
                // $("form#contactform").submit();
                setTimeout(function(){ $.fancybox.close(); }, 4000);

                }else{

                }

            });
        });
    </script>

    <script type="text/javascript">

        $(document).ready(function(){
            var user_image = "<?php echo $image_url; ?>";
            // var word = $( ".word" ).text();
            $('.fb').click(function(e){
                e.preventDefault();
                FB.ui(
                {
                    method: 'feed',
                    link: 'http://www.fatbastardwine.co.za/pages/live_large.php',
                    picture: 'http://www.fatbastardwine.co.za/elements/hippo-small_label.png',
                    caption: 'FAT bastard Label',
                    description: 'Check out the label creator on the South African FAT bastard website!',
                }
                );
            });
            $('a.tweet').click(function(e) {
            //We tell our browser not to follow that link
                e.preventDefault();
                //We get the URL of the link
                var loc = $(this).attr('href');
                //We get the title of the link
                var title = encodeURIComponent($(this).attr('title'));
                //We trigger a new window with the Twitter dialog, in the middle of the page
                window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 225) + ', left=' + $(window).width() / 2 + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
            });
            
        });

        //Custom share
        // $(document).ready(function(){
        //     var user_image = "<?php echo $user_image_share; ?>";
        //     // var word = $( ".word" ).text();
        //     //picture: user_image,
        //     $('.fb').click(function(e){
        //     e.preventDefault();
        //     FB.ui(
        //     {
        //     appId: '350683798610500',
        //     xfbml: true,
        //     version: 'v2.2',
        //     method: 'feed',
        //     link: 'http://www.fatbastard.optimalonline.co.za/pages/user_page/<?php echo $current_date; ?>.php',
        //     picture: 'http://www.fatbastard.optimalonline.co.za/user_images/<?php echo $current_date; ?>_img.png',
        //     caption: 'FAT bastard Label',
        //     description: 'Check out the label I just created on the South African FAT bastard website!',
        //     }
        //     );
        //     });
        //     $('a.tweet').click(function(e) {
        //     //We tell our browser not to follow that link
        //     e.preventDefault();
        //     //We get the URL of the link
        //     var loc = $(this).attr('href');
        //     //We get the title of the link
        //     var title = encodeURIComponent($(this).attr('title'));
        //     //We trigger a new window with the Twitter dialog, in the middle of the page
        //     window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 225) + ', left=' + $(window).width() / 2 + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        //     });
        
        // });
    </script>
<script src="<?= $base_path;?>scripts/html2canvas.js"></script>
