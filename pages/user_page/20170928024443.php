<html>
<!DOCTYPE html>
<head>
  <meta http-equiv="Content-Type" >
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="keywords" content="fatbastard, FAT bastard, wine, south africa, stellenbosch, cape town, thierry and guy, live large, white, red, rose">
  <link rel="icon" href="http://www.fatbastardwine.co.za/favicon.ico" type="image/x-icon">
<!--   <link rel="shortcut icon" href="http://www.fatbastardwine.co.za/favicon-new.ico" type="image/x-icon"> -->
  <link rel="apple-touch-icon" href="http://www.fatbastardwine.co.za/apple-touch-icon-precomposed.png" />
  <title>FAT bastard Wines</title>
  <!-- <link rel="stylesheet" href="http://www.fatbastardwine.co.za/css/fonts/font.css"> -->
  <link rel="stylesheet" href="/css/main.css">
  <link rel="stylesheet" href="/css/overrides.css">
  <link rel="stylesheet" href="/css/fonts/minion.css">

  <script src="/scripts/jquery.js"></script>
  <script src="/scripts/jquery.cookie.js"></script>
  <script src="/scripts/jquery.validate.min.js"></script>
  <script src="/scripts/css_browser_selector.js"></script>

  	<meta property="og:url" content="https://www.fatbastardwine.co.za/pages/label_template.php?user_image=<?php $image; ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:site_name" content="Fat Bastard Wines"/>
	<meta property="og:title" content="Make your own Fat Bastard Wine Label!" />
	<meta property="og:description" content="Create and share your own Fat Bastard Wine Label!" />
	<meta property="og:image" content="http://www.fatbastardwine.co.za/user_images/20170928024443_img.png" />
	<meta property="og:image:width" content="302px" />
	<meta property="og:image:height" content="384px" />
</head>
<body>

	<script>
	  window.fbAsyncInit = function() {
	  FB.init({
	  //appId      : '340697149457885',
  		appId      : '350683798610500',
	  xfbml      : true,
	  version    : 'v2.2'
	  });
	  };
	  (function(d, s, id){
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) {return;}
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js";
	  fjs.parentNode.insertBefore(js, fjs);
	  }(document, 'script', 'facebook-jssdk'));
	  </script>
	  <script>
	  var ara = "http://www.fatbastardwine.co.za/" + 'ara.php';
	  console.log(ara);
	</script>
	<script>
	//if ($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes'){$(location).attr('href', ara);}
	</script>

	<div id="overlay"></div>
	<div id="Top"></div>
	<div class="nav-container">
		<nav class="social-nav">
			<ul class="social desktop">
				<li>BE SOCIAL WITH US</li>
				<li><a href="http://www.fatbastardwine.co.za/pages/news-signup.php"><img src="http://www.fatbastardwine.co.za/elements/newsletter-icon-2x.png" alt="" width="32"></a></li>
				<li><a target="_blank" href="https://www.facebook.com/FATbastardWineSA"><img src="http://www.fatbastardwine.co.za/elements/facebook-icon-2x.png" alt="" width="32"></a></li>
				<li><a target="_blank" href="https://twitter.com/FATbastardSA"><img src="http://www.fatbastardwine.co.za/elements/twitter-icon-2x.png" alt="" width="32"></a></li>
			</ul>
		</nav>
	</div>

	<div class="nav-container">
		<div class="nav">
			<nav class="navigation-nav">
				<button class="mm-toggle fa fa-navicon"></button>
				<div class="mobile-nav" id="mobile-menu">
					<ul>
						<li><a class="home" alt="Home" href="http://www.fatbastardwine.co.za/index.php">HOME</a></li>
						<li><a class="live-large" alt="Live Large" href="http://www.fatbastardwine.co.za/pages/live_large.php">LIVE LARGE</a></li>
						<li><a class="bastards" alt="The FAT bastards" href="http://www.fatbastardwine.co.za/pages/the_fat_bastards.php">THE FAT BASTARDS</a></li>
						<li><a class="get-in-touch" alt="Get In Touch" href="http://www.fatbastardwine.co.za/pages/contact.php">GET IN TOUCH</a></li>
						<li><a class="vib" alt="The Very Important Bastards" href="http://www.fatbastardwine.co.za/pages/very_important_bastards.php">VIB</a></li>
						
						<!-- <li><a target="_blank" href="https://www.facebook.com/FATbastardWineSA">JOIN FAT bastard ON FACEBOOK </a></li> -->
						<li class="mobile-links"><a href="http://www.fatbastardwine.co.za/pages/news-signup.php"><img src="http://www.fatbastardwine.co.za/elements/newsletter-icon-2x.png" alt="" width="29"></a>
						<a target="_blank" href="https://www.facebook.com/FATbastardWineSA"><img src="http://www.fatbastardwine.co.za/elements/facebook-icon-2x.png" alt="" width="29"></a>
						<a target="_blank" href="https://twitter.com/FATbastardSA"><img src="http://www.fatbastardwine.co.za/elements/twitter-icon-2x.png" alt="" width="29"></a></li>
					</ul>

				</div>
				<a alt="Home" href="http://www.fatbastardwine.co.za/index.php"><div class="hippo"></div></a>
				<div class="header-nav">
					<ul class="navigation">
						<li><a class="home" alt="Home" href="http://www.fatbastardwine.co.za/index.php?end">HOME</a></li>
						<li class="divider"> | </li>
						<li><a class="live-large" alt="Live Large" href="http://www.fatbastardwine.co.za/pages/live_large.php">LIVE LARGE</a></li>
						<li class="divider"> | </li>
						<li><a class="bastards" alt="The FAT bastards" href="http://www.fatbastardwine.co.za/pages/the_fat_bastards.php">THE FAT BASTARDS</a></li>
						<li class="divider"> | </li>
						<li><a class="vib" alt="The Very Important Bastards" href="http://www.fatbastardwine.co.za/pages/very_important_bastards.php">VIB</a></li>
						<li class="divider"> | </li>
						<li><a class="get-in-touch" alt="Get In Touch" href="http://www.fatbastardwine.co.za/pages/contact.php">GET IN TOUCH</a></li>

					</ul>
				</div>
			</nav>
		</div>
		<div class="clearfix"></div>
	</div>

	<div id="page">

		<style type="text/css">
			p.error {
			color: #EAB332;
			-webkit-font-smoothing: antialiased;
			margin: 0;
			font-size: 15px;
			line-height: 21px;}
			/*.fancybox-wrap{top: 80px !important;}*/
			.fancybox-skin{ background-color: #fff;}
			.fancybox-inner{overflow: hidden;}
			.fancybox-inner .contact-form textarea{
			border: 1px solid #A6A6A6;
			}
		</style>

		<div class="main-content live-large">
			<div>
				<p>When you're enjoying large, delicious mouthfuls of FAT bastard, you're bound to be keeping great company. There'll simply be no shortage of sharing and good old-fashioned fun.</p>
				<br>
				<p ><i class="highlight">But what about the days in between?</i></p>
				<br>
				<p>When you're <span class="highlight">LIVING LARGE</span>, a day would be incomplete without a welcoming embrace or a thigh-slapping good chuckle at the very least. </p>
			</div>
		</div>
		<div class="clearfix"></div>
		
		<div class="">
			<div class="live-large-container">
				<div>
					<div class="" role="tabpanel">

						<div class="tab-content">
							<!-- create -->
		 					<div role="tabpanel" class="live-large-content tab-pane create active label_share_box" id="create">
								<img src="http://www.fatbastardwine.co.za/user_images/20170928024443_img.png" class="label_shared_image">
							 </div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- end #page -->
		<footer class=" desktop-live-large-footer">
			<div class="footer-nav">
				<ul>
					<li><a class="home" alt="Home" href="http://www.fatbastardwine.co.za/index.php">HOME</a> | </li>
					<li><a class="live-large" alt="Live Large" href="http://www.fatbastardwine.co.za/pages/live_large.php">LIVE LARGE</a> | </li>
					<li><a class="bastards" alt="The FAT bastards" href="http://www.fatbastardwine.co.za/pages/the_fat_bastards.php">THE FAT BASTARDS</a> | </li>
					<li><a class="get-in-touch" alt="Get In Touch" href="http://www.fatbastardwine.co.za/pages/contact.php">GET IN TOUCH</a> | </li>
					<li><a class="tandc-mav" alt="Terms and Conditions" target="_blank"  href="http://www.fatbastardwine.co.za/tandc.html">TERMS &AMP; CONDITIONS</a></li>
				</ul>
			</div>
			<div class="copyright">
				<div class="copyright-inner">
					<p>
						<i class="fa fa-copyright"></i> 2015 FAT bastard. South Africa. All rights reserved
						<span class="hide-mobile">
						<span class="divider">|</span>
						Website by <a target="_blank" href="http://designguru.co.za">Design Guru </a>&amp; <a target="_blank" href="http://www.derrickcapetown.com/">Derrick</a></span>
					</p>
					<a target="_blank" href="http://www.ara.co.za">
						<img class="ara-img" src="http://www.fatbastardwine.co.za/elements/ara.png" alt="">
					</a>
				</div>
			</div>

			<div id="araModal" class="ara_modal">

				<!-- Modal content -->
				<div class="ara_model_outer_border">
					<div class="ara_modal-content">
						<div class="ara_top">
							<div class="ara_hippo"></div>
						</div>
					    <div class="ara_copy">
						    <div class="ara-copy">
						      <p>To enjoy FAT bastard, you need to be able to <span class="highlight">LIVE LIKE ONE</span>. <br> Living <span class="highlight" style="font-size: 28px;">LARGE</span> requires a salubrious take on the good life. </p>
						      <div class="spacer"></div>
						      <p><i>It comes with <span class="highlight">experience</span> so if you're not there yet, keep at it.</i></p>
						    </div>

						    <div class="ara_born">
						      	<h2>When were you born?</h2>
								<form id="age_form" name="age_form">
									<p class="ara_age_not">You are not old enough to enter this site.</p>

									<select id="ara_daydropdown" name="ara_birth_day" class="ara_input" required="">
									<option value="" selected="">DAY *</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
									<option value="31">31</option>
									</select>

									<select id="ara_monthdropdown" name="ara_birth_month" class="ara_input" required="">
									<option value="" selected="">MONTH *</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									</select>

									<select id="ara_yeardropdown" name="ara_birth_year" class="ara_input" required="">
									<option value="" selected="">YEAR *</option>
									<option value="2015">2015</option>
									<option value="2014">2014</option>
									<option value="2013">2013</option>
									<option value="2012">2012</option>
									<option value="2011">2011</option>
									<option value="2010">2010</option>
									<option value="2009">2009</option>
									<option value="2008">2008</option>
									<option value="2007">2007</option>
									<option value="2006">2006</option>
									<option value="2005">2005</option>
									<option value="2004">2004</option>
									<option value="2003">2003</option>
									<option value="2002">2002</option>
									<option value="2001">2001</option>
									<option value="2000">2000</option>
									<option value="1999">1999</option>
									<option value="1998">1998</option>
									<option value="1997">1997</option>
									<option value="1996">1996</option>
									<option value="1995">1995</option>
									<option value="1994">1994</option>
									<option value="1993">1993</option>
									<option value="1992">1992</option>
									<option value="1991">1991</option>
									<option value="1990">1990</option>
									<option value="1989">1989</option>
									<option value="1988">1988</option>
									<option value="1987">1987</option>
									<option value="1986">1986</option>
									<option value="1985">1985</option>
									<option value="1984">1984</option>
									<option value="1983">1983</option>
									<option value="1982">1982</option>
									<option value="1981">1981</option>
									<option value="1980">1980</option>
									<option value="1979">1979</option>
									<option value="1978">1978</option>
									<option value="1977">1977</option>
									<option value="1976">1976</option>
									<option value="1975">1975</option>
									<option value="1974">1974</option>
									<option value="1973">1973</option>
									<option value="1972">1972</option>
									<option value="1971">1971</option>
									<option value="1970">1970</option>
									<option value="1969">1969</option>
									<option value="1968">1968</option>
									<option value="1967">1967</option>
									<option value="1966">1966</option>
									<option value="1965">1965</option>
									<option value="1964">1964</option>
									<option value="1963">1963</option>
									<option value="1962">1962</option>
									<option value="1961">1961</option>
									<option value="1960">1960</option>
									<option value="1959">1959</option>
									<option value="1958">1958</option>
									<option value="1957">1957</option>
									<option value="1956">1956</option>
									<option value="1955">1955</option>
									<option value="1954">1954</option>
									<option value="1953">1953</option>
									<option value="1952">1952</option>
									<option value="1951">1951</option>
									<option value="1950">1950</option>
									<option value="1949">1949</option>
									<option value="1948">1948</option>
									<option value="1947">1947</option>
									<option value="1946">1946</option>
									<option value="1945">1945</option>
									<option value="1944">1944</option>
									<option value="1943">1943</option>
									<option value="1942">1942</option>
									<option value="1941">1941</option>
									<option value="1940">1940</option>
									<option value="1939">1939</option>
									<option value="1938">1938</option>
									<option value="1937">1937</option>
									<option value="1936">1936</option>
									<option value="1935">1935</option>
									<option value="1934">1934</option>
									<option value="1933">1933</option>
									<option value="1932">1932</option>
									<option value="1931">1931</option>
									<option value="1930">1930</option>
									<option value="1929">1929</option>
									<option value="1928">1928</option>
									<option value="1927">1927</option>
									<option value="1926">1926</option>
									<option value="1925">1925</option>
									<option value="1924">1924</option>
									<option value="1923">1923</option>
									<option value="1922">1922</option>
									<option value="1921">1921</option>
									<option value="1920">1920</option>
									<option value="1919">1919</option>
									<option value="1918">1918</option>
									<option value="1917">1917</option>
									<option value="1916">1916</option>
									</select>

									<input type="submit" id="age_submit" value="Submit">
								</form>
								<p class="ara_legal">
								Not for sale to persons under the age of 18. <br>
								Drink responsibly.
								</p>
								<p class="ara_web">
								To view the FAT bastard Website for Europe and Asia <a href="http://international.fatbastard.com/">click here</a>. <br>
								To view the FAT bastard Website for US and Canada <a href="http://fatbastard.com/?country=us">click here</a>.
								</p>
								<br style="clear:both;">
						    </div>
					  	</div>
					  <!-- <div class="contact-copy competition-copy" id="popup_container">
					  <p id="popup_heading">ARA</p>
					  <p id="popup_content"><i>A LUXURIOUS TRIP FOR TWO ABOARD</i></p>
					  <p id="popup_rovos_copy">THE ROVOS RAIL</p>
					  <hr id="enter_line">
					  <p id="enter_now"><a href="/pages/rovosrail.php"><i><span class="highlight">ENTER NOW!</span></i></a></p>
					  </div> -->
					</div>
				</div>

			</div>
			
		</footer>
	</div>

</div>

<script src="http://www.fatbastardwine.co.za/scripts/jquery-ui.custom.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/jquery.mobile-menu.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/navtabs.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/jquery.fancybox.pack.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/jquery.tinycolorpicker.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/smooth-scroll.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/jquery.validate.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/placeholder.min.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/gevann.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/tableExport.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/jquery.base64.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/html2canvas.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/jquery.fancybox.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/postcard.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/theme.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/postcard-desktop-ui.js"></script>

<script>
	$('input, textarea').placeholder();
</script>

<script type="text/javascript">
  $( document ).ready(function() {

    var keys = {32: 1,37: 1, 38: 1, 39: 1, 40: 1};
        function preventDefault(e) {
      e = e || window.event;
      if (e.preventDefault)
          e.preventDefault();
      e.returnValue = false;  
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    function disableScroll() {
      if (window.addEventListener) // older FF
          window.addEventListener('DOMMouseScroll', preventDefault, false);
      window.onwheel = preventDefault; // modern standard
      window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
      window.ontouchmove  = preventDefault; // mobile
      document.onkeydown  = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null; 
        window.onwheel = null; 
        window.ontouchmove = null;  
        document.onkeydown = null;  
    }

    if ($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes') {
      var ara_modal = document.getElementById('araModal');
      ara_modal.style.display = "block";
      //disableScroll();
      // $(location).attr('href', ara);
    }

    $('#age_form').on('submit', function(event) {
      event.preventDefault();

      var day = $('#ara_daydropdown').val(),
          month = $('#ara_monthdropdown').val(),
          year = $('#ara_yeardropdown').val(),
          dob = year+"-"+month+"-"+day;

          // dob = new Date(dob);
          dob = new Date(year,month,day);

      var today = new Date();
      var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
      
      if(age > 17) {
        ara_modal.style.display = "none";
        //enableScroll();

        // document.cookie="is_legal=yes";
        document.cookie="is_legal=yes;path=/;domain=fatbastardwine.co.za";

        $('.ara_age_not').hide();
      } else {
        $('.ara_age_not').show();
      }

    });


  });
</script>

<style type="text/css">
@media only screen and (max-width: 1050px){
	.right .left-color {
background-color: #960048;
}
.right .left-color, .right .right-color {
width: 22px;
height: 22px;
border-radius: 3px;
margin-right: 10px;
float: left;
}
.right .right-color {
background-color: #780E36;
}
.right .left-color, .right .right-color {
width: 22px;
height: 22px;
border-radius: 3px;
margin-right: 10px;
float: left;
}
}
</style>
</div>
<!-- end #page -->
<div class="mobile-live-large">
<footer>
<div class="footer-nav">
<ul>
	<li><a class="home" alt="Home" href="http://www.fatbastardwine.co.za/index.php">HOME</a> | </li>
	<li><a class="live-large" alt="Live Large" href="http://www.fatbastardwine.co.za/pages/live_large.php">LIVE LARGE</a> | </li>
	<li><a class="bastards" alt="The FAT bastards" href="http://www.fatbastardwine.co.za/pages/the_fat_bastards.php">THE FAT BASTARDS</a> | </li>
	<li><a class="get-in-touch" alt="Get In Touch" href="http://www.fatbastardwine.co.za/pages/contact.php">GET IN TOUCH</a> | </li>
	<li><a class="tandc-mav" alt="Terms and Conditions" target="_blank" href="http://www.fatbastardwine.co.za/tandc.html">TERMS &AMP; CONDITIONS</a></li>
</ul>
</div>
<div class="copyright">
<div class="copyright-inner">
	<p><i class="fa fa-copyright"></i> 2015 FAT bastard. South Africa. All rights reserved
		<span class="hide-mobile">
			<span class="divider">|</span>
					Website by <a target="_blank" href="http://designguru.co.za">Design Guru </a>&amp; <a target="_blank" href="http://www.derrickcapetown.com/">Derrick</a></span>
		</p>
		<a target="_blank" href="http://www.ara.co.za">
			<img class="ara-img" src="http://www.fatbastardwine.co.za/elements/ara.png" alt="">
		</a>
	</div>
</div>

	<div id="araModal" class="ara_modal">

	  <!-- Modal content -->
	  <div class="ara_model_outer_border">
	    <div class="ara_modal-content">
	      <div class="ara_top">
	        <div class="ara_hippo"></div>
	      </div>
	      <div class="ara_copy">
	        <div class="ara-copy">
	          <p>To enjoy FAT bastard, you need to be able to <span class="highlight">LIVE LIKE ONE</span>. <br> Living <span class="highlight" style="font-size: 28px;">LARGE</span> requires a salubrious take on the good life. </p>
	          <div class="spacer"></div>
	          <p><i>It comes with <span class="highlight">experience</span> so if you're not there yet, keep at it.</i></p>
	        </div>

	        <div class="ara_born">
	          <h2>When were you born?</h2>
	          <form id="age_form" name="age_form">

	            <p class="ara_age_not">You are not old enough to enter this site.</p>

	            <select id="ara_daydropdown" name="ara_birth_day" class="ara_input" required="">
	              <option value="" selected="">DAY *</option>
	              <option value="1">1</option>
	              <option value="2">2</option>
	              <option value="3">3</option>
	              <option value="4">4</option>
	              <option value="5">5</option>
	              <option value="6">6</option>
	              <option value="7">7</option>
	              <option value="8">8</option>
	              <option value="9">9</option>
	              <option value="10">10</option>
	              <option value="11">11</option>
	              <option value="12">12</option>
	              <option value="13">13</option>
	              <option value="14">14</option>
	              <option value="15">15</option>
	              <option value="16">16</option>
	              <option value="17">17</option>
	              <option value="18">18</option>
	              <option value="19">19</option>
	              <option value="20">20</option>
	              <option value="21">21</option>
	              <option value="22">22</option>
	              <option value="23">23</option>
	              <option value="24">24</option>
	              <option value="25">25</option>
	              <option value="26">26</option>
	              <option value="27">27</option>
	              <option value="28">28</option>
	              <option value="29">29</option>
	              <option value="30">30</option>
	              <option value="31">31</option>
	            </select>

	            <select id="ara_monthdropdown" name="ara_birth_month" class="ara_input" required="">
	              <option value="" selected="">MONTH *</option>
	              <option value="1">1</option>
	              <option value="2">2</option>
	              <option value="3">3</option>
	              <option value="4">4</option>
	              <option value="5">5</option>
	              <option value="6">6</option>
	              <option value="7">7</option>
	              <option value="8">8</option>
	              <option value="9">9</option>
	              <option value="10">10</option>
	              <option value="11">11</option>
	              <option value="12">12</option>
	            </select>

	            <select id="ara_yeardropdown" name="ara_birth_year" class="ara_input" required="">
	              <option value="" selected="">YEAR *</option>
	              <option value="2015">2015</option>
	              <option value="2014">2014</option>
	              <option value="2013">2013</option>
	              <option value="2012">2012</option>
	              <option value="2011">2011</option>
	              <option value="2010">2010</option>
	              <option value="2009">2009</option>
	              <option value="2008">2008</option>
	              <option value="2007">2007</option>
	              <option value="2006">2006</option>
	              <option value="2005">2005</option>
	              <option value="2004">2004</option>
	              <option value="2003">2003</option>
	              <option value="2002">2002</option>
	              <option value="2001">2001</option>
	              <option value="2000">2000</option>
	              <option value="1999">1999</option>
	              <option value="1998">1998</option>
	              <option value="1997">1997</option>
	              <option value="1996">1996</option>
	              <option value="1995">1995</option>
	              <option value="1994">1994</option>
	              <option value="1993">1993</option>
	              <option value="1992">1992</option>
	              <option value="1991">1991</option>
	              <option value="1990">1990</option>
	              <option value="1989">1989</option>
	              <option value="1988">1988</option>
	              <option value="1987">1987</option>
	              <option value="1986">1986</option>
	              <option value="1985">1985</option>
	              <option value="1984">1984</option>
	              <option value="1983">1983</option>
	              <option value="1982">1982</option>
	              <option value="1981">1981</option>
	              <option value="1980">1980</option>
	              <option value="1979">1979</option>
	              <option value="1978">1978</option>
	              <option value="1977">1977</option>
	              <option value="1976">1976</option>
	              <option value="1975">1975</option>
	              <option value="1974">1974</option>
	              <option value="1973">1973</option>
	              <option value="1972">1972</option>
	              <option value="1971">1971</option>
	              <option value="1970">1970</option>
	              <option value="1969">1969</option>
	              <option value="1968">1968</option>
	              <option value="1967">1967</option>
	              <option value="1966">1966</option>
	              <option value="1965">1965</option>
	              <option value="1964">1964</option>
	              <option value="1963">1963</option>
	              <option value="1962">1962</option>
	              <option value="1961">1961</option>
	              <option value="1960">1960</option>
	              <option value="1959">1959</option>
	              <option value="1958">1958</option>
	              <option value="1957">1957</option>
	              <option value="1956">1956</option>
	              <option value="1955">1955</option>
	              <option value="1954">1954</option>
	              <option value="1953">1953</option>
	              <option value="1952">1952</option>
	              <option value="1951">1951</option>
	              <option value="1950">1950</option>
	              <option value="1949">1949</option>
	              <option value="1948">1948</option>
	              <option value="1947">1947</option>
	              <option value="1946">1946</option>
	              <option value="1945">1945</option>
	              <option value="1944">1944</option>
	              <option value="1943">1943</option>
	              <option value="1942">1942</option>
	              <option value="1941">1941</option>
	              <option value="1940">1940</option>
	              <option value="1939">1939</option>
	              <option value="1938">1938</option>
	              <option value="1937">1937</option>
	              <option value="1936">1936</option>
	              <option value="1935">1935</option>
	              <option value="1934">1934</option>
	              <option value="1933">1933</option>
	              <option value="1932">1932</option>
	              <option value="1931">1931</option>
	              <option value="1930">1930</option>
	              <option value="1929">1929</option>
	              <option value="1928">1928</option>
	              <option value="1927">1927</option>
	              <option value="1926">1926</option>
	              <option value="1925">1925</option>
	              <option value="1924">1924</option>
	              <option value="1923">1923</option>
	              <option value="1922">1922</option>
	              <option value="1921">1921</option>
	              <option value="1920">1920</option>
	              <option value="1919">1919</option>
	              <option value="1918">1918</option>
	              <option value="1917">1917</option>
	              <option value="1916">1916</option>
	            </select>

	            <input type="submit" id="age_submit" value="Submit">
	          </form>
	          <p class="ara_legal">
	            Not for sale to persons under the age of 18. <br>
	            Drink responsibly.
	          </p>
	          <p class="ara_web">
	            To view the FAT bastard Website for Europe and Asia <a href="http://international.fatbastard.com/">click here</a>. <br>
	            To view the FAT bastard Website for US and Canada <a href="http://fatbastard.com/?country=us">click here</a>.
	          </p>
	          <br style="clear:both;">
	        </div>
	      </div>
	      <!-- <div class="contact-copy competition-copy" id="popup_container">
	      <p id="popup_heading">ARA</p>
	      <p id="popup_content"><i>A LUXURIOUS TRIP FOR TWO ABOARD</i></p>
	      <p id="popup_rovos_copy">THE ROVOS RAIL</p>
	      <hr id="enter_line">
	      <p id="enter_now"><a href="/pages/rovosrail.php"><i><span class="highlight">ENTER NOW!</span></i></a></p>
	      </div> -->
	    </div>
	  </div>

	</div>

</footer>
<script src="http://www.fatbastardwine.co.za/scripts/jquery-ui.custom.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/jquery.mobile-menu.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/navtabs.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/jquery.fancybox.pack.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/jquery.tinycolorpicker.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/smooth-scroll.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/jquery.validate.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/placeholder.min.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/tableExport.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/jquery.base64.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/html2canvas.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/jquery.fancybox.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/postcard.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/theme.js"></script>
<script src="http://www.fatbastardwine.co.za/scripts/postcard-mobile-ui.js"></script>
</div>
<script>
$('input, textarea').placeholder();
</script>

<script type="text/javascript">
  $( document ).ready(function() {

    var keys = {32: 1,37: 1, 38: 1, 39: 1, 40: 1};
        function preventDefault(e) {
      e = e || window.event;
      if (e.preventDefault)
          e.preventDefault();
      e.returnValue = false;  
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    function disableScroll() {
      if (window.addEventListener) // older FF
          window.addEventListener('DOMMouseScroll', preventDefault, false);
      window.onwheel = preventDefault; // modern standard
      window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
      window.ontouchmove  = preventDefault; // mobile
      document.onkeydown  = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null; 
        window.onwheel = null; 
        window.ontouchmove = null;  
        document.onkeydown = null;  
    }

    if ($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes') {
      var ara_modal = document.getElementById('araModal');
      ara_modal.style.display = "block";
      //disableScroll();
      // $(location).attr('href', ara);
    }

    $('#age_form').on('submit', function(event) {
      event.preventDefault();

      var day = $('#ara_daydropdown').val(),
          month = $('#ara_monthdropdown').val(),
          year = $('#ara_yeardropdown').val(),
          dob = year+"-"+month+"-"+day;

          // dob = new Date(dob);
          dob = new Date(year,month,day);

      var today = new Date();
      var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
      
      if(age > 17) {
        ara_modal.style.display = "none";
        //enableScroll();

        // document.cookie="is_legal=yes";
        document.cookie="is_legal=yes;path=/;domain=fatbastardwine.co.za";

        $('.ara_age_not').hide();
      } else {
        $('.ara_age_not').show();
      }

    });


  });
</script><script>
	$('a.live-large').css({
		'font-weight': '800',
		'color': '#EAB332'
	});
</script>
<script>
$('input, textarea').placeholder();
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61835092-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=10404058; 
var sc_invisible=1; 
var sc_security="c8ee0ffd"; 
var sc_https=1; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="website
statistics" href="http://statcounter.com/"
target="_blank"><img class="statcounter"
src="http://c.statcounter.com/10404058/0/c8ee0ffd/1/"
alt="website statistics"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide