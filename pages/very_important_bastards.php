<?php
	include '../includes/config.php';
	include '../includes/head.php';
	// $_SESSION['vib-home'] = 'very_important_bastards';
?>
<script>
    //if ($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes'){$(location).attr('href', ara);}
</script>
<?php
	include '../includes/header_nav.php';
?>
<div class="main-content bastards-copy vib-copy">
<div>
	<!-- <p>The only thing better than a <span class="highlight sm-c">Glass Of Wine</span> with wonderful colour <br class="mobile-hide">and rich, round palate is another glass of the <i class="highlight">same wine.</i></p> -->
<!-- <p>
	The VIB, or <span class="highlight large">'Very Important bastards'</span>, <br>is an exclusive club for the crème de la crème of FAT bastard’s partners, comprising our top-selling restaurants in the country—the ones who really know how to <span class="highlight sm-c">LIVE LARGE</span>. <br><br> If you’re ever in the area, be sure to stop by at one of these exceptional watering holes for <i class="highlight">great wine</i>, delicious food and fantastic service.
</p> -->
<p>
	Much like a glass of <span class="highlight fat">FAT <span class="fb">bastard</span></span>, we believe that life should be filled to the brim and enjoyed in large, delicious gulps.
</p>
<p>
	Sign up to <span class="highlight sm-c">LIVE LARGE</span> as a <span class="highlight">VIB (<em>Very Important Bastard</em>)</span> and you’ll join an 
	exclusive club that is reserved for the crème de la crème of FAT bastards in South 
	Africa. You’ll be amongst the first to know about fantastic <span class="highlight fat">FAT <span class="fb">bastard</span>.</span> 
	<span class="highlight">competitions, special offers and exciting promotions</span> at our VIB partner 
	restaurants –everything you need to ensure your life overflows with good stuff.
</p>
</div>
<div class="clearfix"></div>
</div>
<section class="vib-section">
	<div class="wines vib-container">
		<div class="vib-top-links clearfix">
			<div>
				<a href="vib_restaurants.php">
					<img src="../elements/vib-restaurants.jpg" style="width:100%;">
					<span>VIB RESTAURANTS</span>
				</a>
			</div>
			<div>
				<a href="vib_signup.php">
					<img src="../elements/vib-sign-up.jpg" style="width:100%;">
					<span>SIGN UP</span>
				</a>
			</div>
			<div>
				<a href="vib_of_the_month.php">
					<img src="../elements/vib-month.jpg" style="width:100%;">
					<span>VIB OF THE MONTH</span>
				</a>
			</div>
		</div>
		<!-- <div class="wine-headline">
			<h1>GALLERY</h1>
		</div>

		<div class="vib-gallery clearfix">
			<a class="vib-thumbs" href="../elements/vib_gallery/1.jpg" rel="group1"><img src="../elements/vib_gallery/1.jpg"></a>
			<a class="vib-thumbs" href="../elements/vib_gallery/2.jpg" rel="group1"><img src="../elements/vib_gallery/2.jpg"></a>
			<a class="vib-thumbs" href="../elements/vib_gallery/3.jpg" rel="group1"><img src="../elements/vib_gallery/3.jpg"></a>
			<a class="vib-thumbs" href="../elements/vib_gallery/4.jpg" rel="group1"><img src="../elements/vib_gallery/4.jpg"></a>
		</div> -->

		<style>
			@media (max-width: 1050px) {
				.fancybox-nav span {
					top: calc(50% - 13px);
				}
			}

			@media (max-width: 750px) {
				.fancybox-overlay {
					background-color: rgba(255, 255, 255, 0.92);
				}

				.fancybox-wrap {
					top: calc(50% - 160px) !important;
					width: 80% !important;
					left: 10% !important;
					position: fixed !important;
				}

				.fancybox-nav span {
					display: block;
					width: 16px;
					height: 25px;
					top: calc(50% - 13px);
				}
			}
		</style>

		<div class="clearfix"></div>
		</div>
	</div>
</section>

<?php
include '../includes/footer.php';
?>
<script>
	$('a.vib').css({
		'font-weight': '800',
		'color': '#EAB332'
	});

	$(".vib-thumbs").fancybox();

	$('.wine-container button').hover(function(){
		$('.background-strip').removeClass('active');
		$(this).parents('.wine-container').find('.background-strip').toggleClass('active');
	});
// $('.thumbs img').click(function(){
// 	var src = $(this).attr('src').toString();//.replace('-thumb.jpg', '.jpg');
// 	console.log(src);
// 		var width = $(this).parents('.wine-lightbox').find('.large-img').innerWidth();
// 		var height = $(this).parents('.wine-lightbox').find('.large-img').innerHeight();
// 		$(this).parents('.wine-lightbox').find('.large-img').attr('src', src);
// 		$(this).parents('.wine-lightbox').find('.large-img').css({'width': width, 'height': height});
// });
</script>
