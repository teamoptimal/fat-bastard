<?php
	session_start();
	include '../../includes/config.php';
	include '../../includes/head.php';
	//$_SESSION['vib'] = 'turn-tender';
?>
<script>
   if($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes'){$(location).attr('href', ara);}
</script>

<?php
	include '../../includes/header_nav.php';
?>
<style type="text/css">
	.wine-container
	{
		float:none;
		max-width:960px;
		margin:59px auto;
		width:auto; 
	}
	.stamp
	{
		float:left;
		margin:33px 0 0 !important; 
	}
	.wine-container.vib .background-strip
	{
		width:100%;
	}
	.wine-container.vib
	{
		width:69%;
		float:none;
		margin:0 auto; 
	}
	.gold
	{
		float: right;
	    font-size: 22px;
	    padding: 13px 0;
	    text-transform: uppercase;
	    width:205px;
	    height:50px !important; 
	}
	.gold:hover
	{
		border-color:#EAB332 !important;
		background-color:#EAB332 !important;
		color:#fff !important;  
	}
	.has-btn
	{
	    display: block;
	    width:100%;
	}
	@media(min-width:1001px){
		.wine-container.vib .background-strip
		{
			width:95.8%;
		}
	}
	@media only screen and (min-device-width: 667px){
		.wine-container .stamp
		{
			margin: 33px 0 0 47%;
		}
	}
	@media(min-width:1037px){
		.has-btn
		{
			width:88%;
		}
	}
</style>
	<div class="main-content bastards-copy vib-copy">
		<div>
			<p>The VIB, or <span class="highlight large">'Very Important bastards'</span>, <br>is an exclusive club for the crème de la crème of FAT bastard’s partners, comprising our top-selling restaurants in the country—the ones who really know how to <span class="highlight sm-c">LIVE LARGE</span>. <br><br> If you’re ever in the area, be sure to stop by at one of these exceptional watering holes for <i class="highlight">great wine</i>, delicious food and fantastic service.</p>	<br>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="wine-headline hidden-desktop">
		<h1>The FAT bastards</h1>
	</div>

	<section class="vib-section">
		<div class="wines vib-container">
			<div class="bottom"><!-- bottom -->
				<div class="wine-lightbox vib" style="" id="tender">
					<div class="left">
						<div class="bottle-container">
							<img width="267" height="183" class="large-img" src="../../pieces/vib/bigmouth2.jpg" alt="">
							<ul class="social-icons thumbs">
								<li><img style="height: auto !important; " width="97" height="97" src="../../pieces/vib/bigmouth3.jpg" alt="" /></li>
									<li><img style="height: auto !important;" width="97" height="97" src="../../pieces/vib/bigmouth4.jpg" alt="" /></li>
								<li><img style="height: auto !important;"width="97" height="80" src="../../pieces/vib/bigmouth5.jpg" alt="" /></li>
							</ul>
						</div>
					</div>
					<div class="right">
						<div class="top">
							<div class="heading">
								<h1>The Big Mouth</h1>
							</div>
						</div>
						<div class="middle">
						<p>Located at Nelson Mandela Square The Big Mouth is a contemporary take on the classic diner infused with culinary influences and ingredients from around the globe.</p>
							<p><b>Address</b><br> Shop No 13 & 14 of Nelson Mandela Square at Sandton City, Corner of Maude and 5th Streets, Sandton, 2146 <br>
							<b>Phone</b><br>063 293 8869</p>
						</div>
						<div class="middle">
							<!-- <a href="http://www.turnntender.co.za">turnntender.co.za</a> <br> -->
							<a class="highlight" href="http://www.thebigmouth.co.za/" target="_blank">www.thebigmouth.co.za</a>
							<ul class="social-icons">
								<li><a href="https://www.facebook.com/thebigmouthdiner/" target="_blank"><img width="40" height="40" src="../../elements/vib-social/fb.png" alt="" /></a></li>
								
							</ul>
						</div>
					</div>
				</div>
			</div><!-- bottom -->
			<div class="clearfix"></div>
			<p class="has-btn vib-restaurant-btn"><a href="../vib_restaurants.php"><button class="gold">Back to vib</button></a></p>

			<div class="vib-bottom-links vib-top-links clearfix">
				<div>
					<a href="../vib_restaurants.php">
						<img src="../../elements/vib-restaurants.png" style="width:100%;">
						<span>VIB RESTAURANTS</span>
					</a>
				</div>
				<div>
					<a href="../vib_signup.php">
						<img src="../../elements/vib-sign-up.png" style="width:100%;">
						<span>SIGN UP</span>
					</a>
				</div>
				<div>
					<a href="../vib_of_the_month.php">
						<img src="../../elements/vib-month.png" style="width:100%;">
						<span>VIB OF THE MONTH</span>
					</a>
				</div>
			</div>
		</div>
		<!-- </div> -->

		<!-- <div class="wine-container stamp-container">
			<img class="stamp" src="../../elements/stamp_3.png" alt="">
		</div> -->

		<div class="clearfix"></div>
	</section>

		<!-- </div>
	</div> -->
<?php
include '../../includes/footer.php';
?>
<script>
	$('a.vib').css({
		'font-weight': '800',
		'color': '#EAB332'
	});
</script>

<?php 
 ?>

