<?php
	session_start();
	include '../../includes/config.php';
	include '../../includes/head.php';
	//$_SESSION['vib'] = 'moo-moos';
?>
<script>
   //if($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes'){$(location).attr('href', ara);}
</script>

<?php
	include '../../includes/header_nav.php';
?>
<style type="text/css">
	.wine-container
	{
		float:none;
		max-width:960px;
		margin:59px auto;
		width:auto; 
	}
	.stamp
	{
		float:left;
		margin:33px 0 0 !important; 
	}
	.wine-container.vib .background-strip
	{
		width:100%;
	}
	.wine-container.vib
	{
		width:69%;
		float:none;
		margin:0 auto; 
	}
	.gold
	{
		float: right;
	    font-size: 22px;
	    padding: 13px 0;
	    text-transform: uppercase;
	    width:205px;
	    height:50px !important; 
	}
	.gold:hover
	{
		border-color:#EAB332 !important;
		background-color:#EAB332 !important;
		color:#fff !important;  
	}
	.has-btn
	{
	    display: block;
	    width:100%;
	}
	@media(min-width:1001px){
		.wine-container.vib .background-strip
		{
			width:95.8%;
		}
	}
	@media only screen and (min-device-width: 667px){
		.wine-container .stamp
		{
			margin: 33px 0 0 47%;
		}
	}
	@media(min-width:1037px){
		.has-btn
		{
			width:88%;
		}
	}
</style>
	<div class="main-content bastards-copy vib-copy">
		<div>
			<p>The VIB, or <span class="highlight large">'Very Important bastards'</span>, <br>is an exclusive club for the crème de la crème of FAT bastard’s partners, comprising our top-selling restaurants in the country—the ones who really know how to <span class="highlight sm-c">LIVE LARGE</span>. <br><br> If you’re ever in the area, be sure to stop by at one of these exceptional watering holes for <i class="highlight">great wine</i>, delicious food and fantastic service.</p>	<br>
		</div>
		<div class="clearfix"></div>
	</div>

	<section class="vib-section">
		<div class="wine-headline hidden-desktop">
			<h1>The FAT bastards</h1>
		</div>

		<div class="wines vib-container">
			<div class="bottom"><!-- bottom -->
				<div class="wine-lightbox vib" style="" id="tender">
					<div class="left">
						<div class="bottle-container">
							<img width="267" height="183" class="large-img" src="../../pieces/vib/moo-moos/1.png" alt="">
							<ul class="social-icons thumbs">
								<li><a class="social-icons-thumbs" href="../../pieces/vib/moo-moos/moo_moo1_big.jpg" rel="group1"><img width="97" height="97" src="../../pieces/vib/moo-moos/moo_moo1.jpg" alt="" /></a></li>
								<li><a class="social-icons-thumbs" href="../../pieces/vib/moo-moos/moo_moo2_big.jpeg" rel="group1"><img width="97" height="97" src="../../pieces/vib/moo-moos/moo_moo2.jpg" alt="" /></a></li>
								<li><a class="social-icons-thumbs" href="../../pieces/vib/moo-moos/moo_moo3_big.jpg" rel="group1"><img width="97" height="97" src="../../pieces/vib/moo-moos/moo_moo3.jpg" alt="" /></a></li>
							</ul>
						</div>

					</div>
					<div class="right">
						<div class="top">
							<div class="heading">
								<h1>MooMoo</h1>
							</div>
						</div>
						<div class="middle">
						<p>MooMoo’s philosophy is all about having fun while you eat and enjoying a glass (or bottle) of great wine while doing it. They pride themselves in offering the best beef around, which they age and cut themselves, and with their in house “Wall of Wine” stocking both well-known and lesser known wines, dining at MooMoo’s is an experience not to be missed.</p>
							<h5 class="no-margin">BROOKLYN - PRETORIA</h5>
							<p><b>Address</b><br> Cnr. Veale & Middle street<br>
							<b>Phone</b><br>+27 (0)12 346 8888</p>
							<p>Monday to Thursday 9am-10pm / Friday to Saturday 9am to 10:30pm / Sunday 9am-9pm</p>
							<p>RESERVATIONS MONDAYS - THURSDAYS ONLY</p>
						</div>
						<div class="middle">
							<!-- <a href="http://www.turnntender.co.za">turnntender.co.za</a> <br> -->
							<a class="highlight" href="http://www.moo-moo.co.za" target="_blank">http://www.moo-moo.co.za</a>
							<ul class="social-icons">
								<li><a href="https://www.facebook.com/MooMooHerd/" target="_blank"><img width="40" height="40" src="../../elements/vib-social/fb.png" alt="" /></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div><!-- bottom -->
			<div class="clearfix"></div>
			<p class="has-btn vib-restaurant-btn"><a href="../vib_restaurants.php"><button class="gold">Back to vib</button></a></p>

			<div class="vib-bottom-links vib-top-links clearfix">
				<div>
					<a href="../vib_restaurants.php">
						<img src="../../elements/vib-restaurants.png" style="width:100%;">
						<span>VIB RESTAURANTS</span>
					</a>
				</div>
				<div>
					<a href="../vib_signup.php">
						<img src="../../elements/vib-sign-up.png" style="width:100%;">
						<span>SIGN UP</span>
					</a>
				</div>
				<div>
					<a href="../vib_of_the_month.php">
						<img src="../../elements/vib-month.png" style="width:100%;">
						<span>VIB OF THE MONTH</span>
					</a>
				</div>
			</div>
		</div>

		<!-- <div class="wine-container stamp-container">
			<img class="stamp" src="../../elements/stamp_3.png" alt="">
		</div> -->
		<style>
			@media (max-width: 1050px) {
				.fancybox-nav span {
					top: calc(50% - 13px);
				}
			}

			@media (max-width: 750px) {
				.fancybox-overlay {
					background-color: rgba(255, 255, 255, 0.92);
				}

				.fancybox-wrap {
					top: calc(50% - 160px) !important;
					width: 80% !important;
					left: 10% !important;
					position: fixed !important;
				}

				.fancybox-nav span {
					display: block;
					width: 16px;
					height: 25px;
					top: calc(50% - 13px);
				}
			}
		</style>
		<div class="clearfix"></div>
	</section>

<?php
include '../../includes/footer.php';
?>
<script>
	$('a.vib').css({
		'font-weight': '800',
		'color': '#EAB332'
	});

	$(document).ready(function() {
		$(".social-icons-thumbs").fancybox();
	});	
</script>

<?php 
 ?>

