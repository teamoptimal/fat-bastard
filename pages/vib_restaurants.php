<?php
	include '../includes/config.php';
	include '../includes/head.php';
	// $_SESSION['vib-home'] = 'very_important_bastards';
?>
<script>
    //if ($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes'){$(location).attr('href', ara);}
</script>
<?php
	include '../includes/header_nav.php';
?>
	<div class="main-content bastards-copy vib-copy">
		<div>
			<p>
				To <span class="highlight sm-c">LIVE LARGE</span>, you need to gallop to your local watering hole and take a chunk-size bite out of life. 
				We unreservedly encourage you to try one of our <span class="highlight">VIB restaurants</span>. These restaurants have been handpicked 
				for their incomparable service, delectable food, and for being the perfect locale to enjoy <span class="highlight"><em>a bottle or 2</em></span> of 
				<span class="highlight fat">FAT <span class="fb">bastard</span></span> by the hearty mouthful.
			</p>
			<p style="text-transform:uppercase;">Select your favourite VIB restaurant</p>
		</div>
		<div class="clearfix"></div>
	</div>

	<section class="vib-section">
		<div class="wine-headline hidden-desktop">
			<h1>Very Important bastards</h1>
		</div>
		<div class="wines vib-container">
			<div class="clearfix">

				<div>
					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/doppio-zero/3.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >Doppio Zero</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/doppio-zero.php" rel="vib" class="vib-fancy"><button class="sauvignon">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="wine-divider"></div>

					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/fishmonger-rosebank/2.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >Fishmonger</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/fishmonger.php" rel="vib" class="vib-fancy"><button class="pinot-rose">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="wine-divider"></div>

					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/jbs-corner-melrose-arch/2.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >Jb's Corner</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/jb-corner.php" rel="vib" class="vib-fancy"><button class="cab-sav">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="wine-divider"></div>

				</div>

				<div>

					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/the-baron/2.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >The Baron</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/the-baron.php" rel="vib" class="vib-fancy"><button class="merlot">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="wine-divider"></div>

					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/the-bull-run/2.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >Bull Run</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/bull-run.php" rel="vib" class="vib-fancy"><button class="shiraz">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="wine-divider"></div>

					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/the-harbour-fish-market/2.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >Harbour Fish Market</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/harbour-fish-market.php" rel="vib" class="vib-fancy"><button class="fish-grill">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="wine-divider"></div>

				</div>

				<div>
					
					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/trumps/2.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >Trumps Grillhouse</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/trumps-grillhouse.php" rel="vib" class="vib-fancy"><button class="trumps">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="wine-divider"></div>

					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/turn-n-tender/2.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >Turn 'n Tender</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/turn-tender.php" rel="vib" class="vib-fancy"><button class="tender">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="wine-divider"></div>

					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/moo-moos/3_2.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >MooMoo</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/moo-moos.php" rel="vib" class="vib-fancy"><button class="tender">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="wine-divider"></div>
				</div>

					<div>

					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/ilgusto1.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >I’L Gusto Ristorante</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/ilgusto.php" rel="vib" class="vib-fancy"><button class="trumps">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="wine-divider"></div>

					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/bridgestreet1.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >Bridge Street Brewery</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/bridgestreet.php" rel="vib" class="vib-fancy"><button class="tender">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="wine-divider"></div>

					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/mullerhouse3.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >The Muller House</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/mullerhouse.php" rel="vib" class="vib-fancy"><button class="tender">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="wine-divider"></div>

				</div>

				<div class="clearfix"></div>
				
				<div style="height: 10px !important; width: 100% !important;"> </div>
				<div style="height: auto;">

					<div class="wine-container vib" style="">
						<div class="wine-left"><img src="../pieces/vib/bigmouth1.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >The Big Mouth</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/bigmouth.php" rel="vib" class="vib-fancy"><button class="tender">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/jamies1.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >Jamie’s Italian</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/jamies.php" rel="vib" class="vib-fancy"><button class="tender">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="wine-container vib">
						<div class="wine-left"><img src="../pieces/vib/butcher1.jpg" alt=""></div>
						<div class="wine-right">
							<div class="top-double">
								<div class="background-strip fb-chardonnay"><h1 class="double" >The Butcher</h1></div>
							</div>
							<div class="bottom">
								<div class="wine-button-container" style="font-size: 1rem !important;"><a href="vib/butcher.php" rel="vib" class="vib-fancy"><button class="tender">Read More</button></a></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

				</div>

				<!-- <div class="wine-container vib stamp-container"><img class="stamp" src="../elements/stamp_3.png" alt=""></div> -->

			</div>

			<div class="clearfix"></div>

			<!-- <h3 class="vib-apply-here">Don’t see your restaurant? <a href="vib_signup.php" class="highlight">APPLY HERE</a></h3> -->

			<div class="vib-bottom-links vib-top-links clearfix">
				<div>
					<a href="vib_restaurants.php">
						<img src="../elements/vib-restaurants.png" style="width:100%;">
						<span>VIB RESTAURANTS</span>
					</a>
				</div>
				<div>
					<a href="vib_signup.php">
						<img src="../elements/vib-sign-up.png" style="width:100%;">
						<span>SIGN UP</span>
					</a>
				</div>
				<div>
					<a href="vib_of_the_month.php">
						<img src="../elements/vib-month.png" style="width:100%;">
						<span>VIB OF THE MONTH</span>
					</a>
				</div>
			</div>

		</div>
	</section>	
</div>
<?php
include '../includes/page_pieces/vib_lightboxes.php';
include '../includes/page_pieces/wine_packshots.php';
include '../includes/footer.php';
?>
<script>
	$('a.vib').css({
		'font-weight': '800',
		'color': '#EAB332'
	});

	$('.wine-container button').hover(function(){
		$('.background-strip').removeClass('active');
		$(this).parents('.wine-container').find('.background-strip').toggleClass('active');
	});
$('.thumbs img').click(function(){
	var src = $(this).attr('src').toString().replace('-thumb.jpg', '.jpg');
	console.log(src);
		var width = $(this).parents('.wine-lightbox').find('.large-img').innerWidth();
		var height = $(this).parents('.wine-lightbox').find('.large-img').innerHeight();
		$(this).parents('.wine-lightbox').find('.large-img').attr('src', src);
		$(this).parents('.wine-lightbox').find('.large-img').css({'width': width, 'height': height});
});
</script>
