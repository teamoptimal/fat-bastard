<?php

//social channels and interests should only include those options selected and should be lowercase
if(isset($_POST['social_channels']))
  $social_channel = $_POST['social_channels'];
else
  $social_channel = NULL;

if(isset($_POST['interests']))
  $interests = $_POST['interests'];
else
  $interests = NULL;

if(isset($_POST['first_name']))
  $fields['first_name'] = $_POST['first_name'];
else
  $fields['first_name'] = NULL;

if(isset($_POST['last_name']))
  $fields['last_name'] = $_POST['last_name'];
else
  $fields['last_name'] = NULL;

if(isset($_POST['email']))
  $fields['email'] = $_POST['email'];
else
  $fields['email'] = NULL;

if(isset($_POST['mobile']))
  $fields['mobile'] = $_POST['mobile'];
else
  $fields['mobile'] = NULL;

if(isset($_POST['dob']))
  $fields['dob'] = $_POST['dob'];
else
  $fields['dob'] = NULL;

if(isset($_POST['gender']))
  $fields['gender'] = $_POST['gender'];
else
  $fields['gender'] = NULL;

if(isset($_POST['opt_in_email']))
  $fields['opt_in_email'] = $_POST['opt_in_email'];
else
  $fields['opt_in_email'] = NULL;

if(isset($_POST['opt_in_sms']))
  $fields['opt_in_sms'] = $_POST['opt_in_sms'];
else
  $fields['opt_in_sms'] = NULL;

if(isset($_POST['country']))
  $fields['country'] = $_POST['country'];
else
  $fields['country'] = NULL;

if(isset($_POST['province']))
  $fields['province'] = $_POST['province'];
else
  $fields['province'] = NULL;

if(isset($_POST['city']))
  $fields['city'] = $_POST['city'];
else
  $fields['city'] = NULL;

if(isset($_POST['suburb']))
  $fields['suburb'] = $_POST['suburb'];
else
  $fields['suburb'] = NULL;

$fields['interaction_type'] = 'enter';

 //This is a change from the previous version

$fields['interaction_source'] = 'web';

$fields['return_id'] = 0; 

if(isset($_POST['accept_terms']))
	$fields['additional_info']="Terms Accepted: Yes";
else
	$fields['additional_info']="Terms Accepted: No";

$fields['id'] = 'bc74f09b-192a-11e7-ace9-001e676a4028'; //this is the unique form ID for robertson winery.

if(is_array($social_channel) || is_object($social_channel))
{
foreach($social_channel as $key=>$value) 
  {
	  $fields["social_channel[$key]"]=$value;
  }
}

if(is_array($interests) || is_object($interests))
{
foreach($interests as $key=>$value) 
  {
    $fields["interest[$key]"]=$value;
  }
}

//echo '<pre>';
//var_dump($fields);
//exit;

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,"https://fb-node.optimalonline.co.za/form/consumer");
// curl_setopt($ch, CURLOPT_URL,"http://fb-node.node.local/form/consumer");
curl_setopt($ch, CURLOPT_REFERER, 'http://www.fatbastardwine.co.za');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$user_id = curl_exec ($ch);
curl_close ($ch);
//echo 'User ID: '.$user_id;
//var_dump($user_id);
//exit;
?>
<?php
	$SERVER_ip = $_SERVER['HTTP_HOST'];
$base_path = '';

if($SERVER_ip=='197.221.14.97'){
  $base_path = 'http://www.fatbastardwine.co.za/';
}
elseif($SERVER_ip=='demo.designguru.co.za' || $SERVER_ip=='www.demo.designguru.co.za' || $SERVER_ip=='www.designguru.co.za/demo' || $SERVER_ip=='designguru.co.za/demo'){ //demo path
  $base_path = 'http://demo.designguru.co.za/fatbastard/web/v4/';
}
else{ //client
  $base_path = 'http://www.fatbastardwine.co.za/';
}
	
?>
<?php
error_reporting(0);

session_start();
  if(!isset($_SESSION['day'])){

  $_SESSION['day']=$_GET['day'];
  $_SESSION['month']=$_GET['month'];
  $_SESSION['year']= $_GET['year'];
  $_SESSION['userLocation']=$_GET['userLocation'];
}
  $_SESSION['user_name']=$_POST['user_name'];
  $_SESSION['user_surname']=$_POST['user_surname'];
  $_SESSION['user_email']=$_POST['user_email'];
  $_SESSION['user_tel']=$_POST['user_tel'];

  include '../includes/config.php';
  include '../includes/head.php';
  //$_SESSION['vib'] = 'ciao';
?>
<link rel="stylesheet" href="/pages/livelarge_exp/css/livelarge.css">
<script>
   //if($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes'){$(location).attr('href', ara);}
</script>

<?php
  include '../includes/header_nav.php';
?>
<style type="text/css">
  .wine-container
  {
    float:none;
    max-width:960px;
    margin:59px auto;
    width:auto; 
  }
  .stamp
  {
    float:left;
    margin:33px 0 0 !important; 
  }
  .wine-container.vib .background-strip
  {
    width:100%;
  }
  .wine-container.vib
  {
    width:69%;
    float:none;
    margin:0 auto; 
  }
  .gold
  {
    float: right;
      font-size: 22px;
      padding: 13px 0;
      text-transform: uppercase;
      width:205px;
      height:50px !important; 
  }
  .gold:hover
  {
    border-color:#EAB332 !important;
    background-color:#EAB332 !important;
    color:#fff !important;  
  }
  .has-btn
  {
      display: block;
      width:100%;
  }
  @media(min-width:1001px){
    .wine-container.vib .background-strip
    {
      width:95.8%;
    }
  }
  @media only screen and (min-device-width: 667px){
    .wine-container .stamp
    {
      margin: 33px 0 0 47%;
    }
  }
  @media(min-width:1037px){
    .has-btn
    {
      width:88%;
    }
  }
</style>
  <div class="main-content bastards-copy vib-copy">
    <div>
      <p>
        <span class="highlight"><em>Hurrah!</em></span> We’re delighted that you’re joining our pod. For your entrance to be made as grand as can be, we’d love to know a little
        more about you. Please complete the form below and let’s get out there and <span class="highlight sm-c">LIVE LARGE</span> together.
      </p>
    </div>
    <div class="clearfix"></div>
  </div>
    <section class="vib-section">
      <div class="wines vib-container">
        
        <h1 class="highlight" style="font-size: 70px; text-align:center;">Thank you!</h1>

        <hr>

        <h3 class="vib-apply-here">Your submission has been successful.</h3>

        <div class="vib-bottom-links vib-top-links clearfix">
          <div>
            <a href="vib_restaurants.php">
              <img src="../elements/vib-restaurants.png" style="width:100%;">
              <span>VIB RESTAURANTS</span>
            </a>
          </div>
          <div>
            <a href="vib_signup.php">
              <img src="../elements/vib-sign-up.png" style="width:100%;">
              <span>SIGN UP</span>
            </a>
          </div>
          <div>
            <a href="vib_of_the_month.php">
              <img src="../elements/vib-month.png" style="width:100%;">
              <span>VIB OF THE MONTH</span>
            </a>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </section>
<?php
include '../includes/footer.php';
?>
<script>
  $('a.vib').css({
    'font-weight': '800',
    'color': '#EAB332'
  });
</script>