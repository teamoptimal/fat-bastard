
<div class="overlay_where_to_buy">
    <div class="where_to_buy_pop_up">
    <div id="target">
    Here
    </div>
        
    </div>
   
</div>

<script>
    $(document).ready(function(){
        var map;
        var gmarkers = [];

        $('.where_to_buy_btn').on('click', function(event){
            event.stopPropagation();

            $('.overlay_where_to_buy').show();
            $('.where_to_buy_pop_up').show();
            initGeolocation();
        });

        $('.where_to_buy_pop_up').on('click',function(event){
            event.stopPropagation();
        });

        $('.btn_close_find').on('click', function(event){
            event.stopPropagation();
            $('.overlay_where_to_buy').hide();
            $('.where_to_buy_pop_up').hide();
        })

        $('html').on('click', function(){
            $('.overlay_where_to_buy').hide();
            $('.where_to_buy_pop_up').hide();
        })

        $('.settings_toggle').on('click', function(){
            if($('.settings').hasClass('display_none')){
            
                $('.settings').removeClass('display_none');

                if($(window).width() > 1750){
                    $('.outlet_results').css('height', '400px');
                }else{
                    $('.outlet_results').css('height', '220px');
                }
            
            }else{
                if($(window).width() > 1750){
                        
                    $('.outlet_results').css('height', '534px');
                }else{
                    $('.outlet_results').css('height', '330px');
                }
                if($(window).width() < 700){
                    $('.outlet_results').css('height', '370px');
                }
                $('.settings').addClass('display_none');
            }
        
        });

        // Initialize and add the map
        $('.outlet_results').on('mouseenter','.outlet_box', function(){
            var self = $(this);
            gmarkers.map(function (el) {
                if (el.title === self.data('name')) {
                    el.setIcon('https://maps.google.com/mapfiles/ms/icons/blue-dot.png');
                }
            });
        });

        $('.outlet_results').on('mouseleave','.outlet_box', function(){
            var self = $(this);
            gmarkers.map(function (el) {
                if (el.title === self.data('name')) {
                    el.setIcon('https://maps.google.com/mapfiles/ms/icons/red-dot.png');
                }
            });
        });
  
        var useProduct = false;
        var productStockCode;
        $('.producer_block').text('');
        $('.producer_block').append('Producer: ' +  '<span>' +$('#dropdownMenu2').text() +  '</span>');
        <?php if(isset($product_detail)){ ?>
            useProduct = true;
            productStockCode = "<?php echo $product_detail->stock_code ?>";
            productName = "<?php echo $product_detail->name ?>";
            $('.producer_block').text('');
            $('.producer_block').append('Product: ' +  '<span>' + productName +  '</span>' );
        <?php } ?>

        // alert(useProduct);
        // alert(productStockCode);

        var current_search_type = 'USE_GPS';
             
        $('.current_toggle').click(function(){
            $('.use_selected').addClass('display_none');
            $('.use_current').removeClass('display_none');
            current_search_type = 'USE_GPS';
            $('.outlet_results').empty();
            $('.outlet_results').addClass('display_none');
        })
       

        $('.selected_toggle').click(function(){
            $('.use_selected').removeClass('display_none');
            $('.use_current').addClass('display_none');
            current_search_type = 'USE_PROVINCE';
            $('.outlet_results').empty();
            $('.outlet_results').addClass('display_none');
        })

        $('#province_select').on('change', function(){
            var selected_province =  $('#province_select option:selected').val();
          
            $.ajax({
                type: "GET",
                beforeSend: function(request) {
                    $('.searching_loader').removeClass('display_none');
                    request.setRequestHeader("X-Proxy-To", 'https://vinimark.mobi/whereToBuy/api/SearchOptions/ProvincesAndTowns');
                    request.setRequestHeader ("Authorization", "Basic dmluaW1hcms6ZWZiNjI5Yjk=");
                },
                url: "/proxy",
                    success: function(data) { 
                        $('.searching_loader').addClass('display_none');
                       
                        $('#city_select').empty();

                        if(selected_province !== '*'){
                            
                            var city_by_province = data.Towns.filter(function (city) {
                            return city.Province === selected_province;
                            });

                            city_by_province.map(function (city) {
                                $('#city_select').append('<option value="' + city.Name + '">' + city.Name + '</option>');
                            });

                        }else{
                         
                            data.Towns.map(function (town) {
                                $('#city_select').append('<option value="' + town.Name + '">' + town.Name + '</option>');
                            });
                        }

                        console.log()
                       
                      
                       
                        sortUL("#city_select");
                    }
            });
        })



        $.ajax({
            url: "/proxy.php",
            data: { customUserIdentifier: "test" },
            type: "GET",
            beforeSend: function(xhr){
                xhr.setRequestHeader('Authorization', 'Basic dmluaW1hcms6ZWZiNjI5Yjk=');
                xhr.setRequestHeader("X-Proxy-URL", 'https://vinimark.mobi/whereToBuy/api/SearchOptions/ProvincesAndTowns');
            },
            success: function(data) { console.log(data) }
        });
        
        // $.ajax({
        //     type: "GET",
        //     beforeSend: function(request) {
        //         $('.searching_loader').removeClass('display_none');
        //         request.setRequestHeader("X-Proxy-URL", 'https://vinimark.mobi/whereToBuy/api/SearchOptions/ProvincesAndTowns?customUserIdentifier=test');
        //         request.setRequestHeader ("Authorization", "Basic dmluaW1hcms6ZWZiNjI5Yjk=");
        //     },
            
        //     url: "/proxy.php",
        //         success: function(data) { 
        //             console.log(data);
        //             $('.searching_loader').addClass('display_none');
        //             data.Provinces.map(function (province) {
        //                 $('#province_select').append('<option value="' + province.Name + '" >' + province.Name + '</option>');
        //             });

        //             data.Towns.map(function (town) {
        //                 $('#city_select').append('<option value="' + town.Name + '">' + town.Name + '</option>');
        //             });

        //             sortUL("#city_select");
        //         },
        //         error: function(XMLHttpRequest, textStatus, errorThrown) { 
        //                 alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        //         }   
        // });
       
        $('.btn_find_outlets').on('click', function(e){
            e.preventDefault();
            $('.outlet_results').empty();
            var selected_province =  $('#province_select option:selected').val();
            var selected_city =  $('#city_select option:selected').val();
            var selected_brand =  $('#dropdownMenu2').text();
            var current_lat = document.getElementById('lat').value;
            var current_long = document.getElementById('long').value;
         
            if(current_search_type === 'USE_PROVINCE'){
                if(useProduct !== true){
                    var url = 'https://vinimark.mobi/whereToBuy/api/FindOutlets/BySearch_ForSupplier';
                    var data_string = "province="+ selected_province +"&town="+ selected_city +"&supplier=" + selected_brand;
                    console.log(url + '?' + data_string);
                }else{
                    var url = 'https://vinimark.mobi/whereToBuy/api/FindOutlets/BySearch';
                    var data_string = "province="+ selected_province +"&town="+ selected_city + "&supplier=" + selected_brand + "&stockCode=" + productStockCode + '&barCode';
                    console.log(url + '?' + data_string);
                }
               
            }

            if(current_search_type === 'USE_GPS'){
                if(useProduct !== true){
                    var url = 'https://vinimark.mobi/whereToBuy/api/FindOutlets/ByCoordinates_ForSupplier';
                    var data_string = "latitude=" + current_lat + "&longitude=" + current_long + "&supplier=" + selected_brand;
                }else{
                    var url = 'https://vinimark.mobi/whereToBuy/api/FindOutlets/ByCoordinates'
                    var data_string = "latitude=" + current_lat + "&longitude=" + current_long + "&stockCode=" + productStockCode + '&barCode';
              
                }
            }
            
            $.ajax({
                type: "GET",
                beforeSend: function(request) {
                    $('.searching_loader').removeClass('display_none');
                    request.setRequestHeader("X-Proxy-To", url);
                    request.setRequestHeader ("Authorization", "Basic dmluaW1hcms6ZWZiNjI5Yjk=");
                },
                data: data_string,
                url: "/proxy",
                    success: function(data) { 
                        // console.log(data)
                        if($(window).width() > 1750){
                            $('.outlet_results').css('height', '534px');
                        }else{
                            $('.outlet_results').css('height', '330px');
                        }
                        if($(window).width() < 700){
                            $('.outlet_results').css('height', '370px');
                        }
                        
                        $('.outlet_results').removeClass('display_none');
                        $('.searching_loader').addClass('display_none');
                       
                        if(data.Outlets.length > 0 && data !== null){
                            // console.log("-----");
                            // console.log(data);
                            var zoom = 10;

                            var check_for_retail = data.Outlets.filter(isRetail => isRetail.IsRetail === true);
                            console.log(check_for_retail);

                         if(check_for_retail.length !== 0)
                         {
                            // The location of Uluru
                            if(data.Outlets.length > 17){
                                zoom = 6;
                            }
                            var outletCount = data.Outlets.length -1;
                            outletCount = outletCount / 2;
                            var uluru = {lat: check_for_retail[0].Latitude, lng: check_for_retail[0].Longitude};
                            
                            // The map, centered at Uluru
                            map2 = new google.maps.Map(document.getElementById('map_wtp'), {zoom: zoom, center: uluru});
                         }

                            data.Outlets.map(function (outlet) {
                                var nname = outlet.Name.replace(' ', '-');
                                var nname = nname.replace(' - ', '-');
                                var uluru = { lat: outlet.Latitude, lng: outlet.Longitude };

                                var outlet_box = $('<div class="outlet_box" data-name="' + nname + '">');
                                var online_box = $('<div>');
                                var website_link = outlet.Website.replace('https://', '');
                                website_link = website_link.replace('/', '');
                                var location;
                                var distance;
                                var from_central = '';

                                if (outlet.Latitude !== 0) {
                                    outlet_box = $('<div class="outlet_box"  data-name="' + nname + '" data-lat="' + outlet.Latitude + '" data-long="' + outlet.Longitude + '" >');
                                    location = $('<p style="margin-bottom: 0"></p>');
                                    location.append('Location: ' + '<a target="_blank" style="font-size: 12px; color: #009BD6" href="https://www.google.com/maps/place/' + outlet.Latitude + ', ' + outlet.Longitude + '">' + outlet.Latitude + ',' + outlet.Longitude + '</a>');

                                    var marker = new google.maps.Marker({ title: nname, icon: 'https://maps.google.com/mapfiles/ms/icons/red-dot.png', position: uluru, map: map2, animation: google.maps.Animation.DROP });
                                    marker.addListener('click', function () {
                                        var loca_link = 'https://www.google.com/maps/place/' + outlet.Latitude + ', ' + outlet.Longitude;
                                        window.open(loca_link, 'gps_marker_link');
                                    });
                                    // Push your newly created marker into the array:
                                    gmarkers.push(marker);
                                } else {
                                    location = $('<p>');
                                }

                                if (outlet.DistanceFromSearchPoint_Metres !== 0) {
                                    if (current_search_type === 'USE_PROVINCE') {
                                        from_central = ' (from central location)';
                                    }

                                    distance = $('<p style="margin-bottom: 0">Distance: ' + (outlet.DistanceFromSearchPoint_Metres / 1000).toFixed(1) + ' km' + from_central + '</p>');
                                } else {
                                    distance = $('<p>');
                                }

                                var address_box = $('<div>');
                                address_box.append('<p style="margin-bottom: 0">' + outlet.AddressLine1 + '</p>').append('<p style="margin-bottom: 0">' + outlet.AddressLine2 + '</p>').append('<p style="margin-bottom: 0">' + outlet.AddressLine3 + '</p>').append('<p style="margin-bottom: 0">' + outlet.AddressLine4 + '</p>').append('<p style="margin-bottom: 0">' + outlet.AddressLine5 + '</p>').append('<p style="margin-bottom: 0">' + outlet.AddressLine6 + '</p>').append('<p style="margin-bottom: 0">' + outlet.AddressPostalCode + '</p>').append(location).append(distance);

                                if (outlet.IsOnline) {
                                    online_box.append('<p style="font-weight: bold;display: inline-block;margin-bottom: 0">' + outlet.Name + '</p> ' + '<i class="fa fa-laptop" aria-hidden="true" style="font-size: 13px"></div> <span>Online</span>');
                                }

                                if (outlet.IsRetail == true) {
                                    online_box.append('<p style="font-weight: bold;display: inline-block;margin-bottom: 0">' + outlet.Name + '</p> ' + '<i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 13px"></div style="font-size: 12px;"> <span>Retail</span>');
                                }

                                if (outlet.IsRestaurant == true) {
                                    online_box.append('<p style="font-weight: bold;display: inline-block;margin-bottom: 0">' + outlet.Name + '</p> ' + '<i class="fa fa-glass" aria-hidden="true" style="font-size: 13px"></div> </div style="font-size: 12px;"> <span>Restaurant</span>');
                                }

                                if (outlet.Website === '') {
                                    $('.outlet_results').append(outlet_box.append(online_box).append(address_box));
                                } else {
                                    $('.outlet_results').append(outlet_box.append(online_box).append('<span style="font-size: 12px">Website:</span> <a style="color: #009BD6;font-size: 12px" target="_blank" href="' + outlet.Website + '">' + website_link + '</a>').append(address_box));
                                }
                            });
                            
                            $('.outlet_results').append('<small>'+ data.Disclaimer + '</small>');
                            $('.settings').addClass('display_none');
                          
                        
                        }else{
                           
                            $('.outlet_results').css('height', 'auto');
                            $('.outlet_results').append('<p>Sorry, we could not find any outlets in your area.</p>')
                            
                        }
                        
                    },

                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                    }     
            });
        })

        function initGeolocation()
        {
            if( navigator.geolocation ){

                var options = {
                    enableHighAccuracy: true,
                    timeout: 10000,
                    maximumAge: 0
                };
                // Call getCurrentPosition with success and failure callbacks
                navigator.geolocation.getCurrentPosition( success, fail, options );

                
            }else{
                $('.location_checkbox').addClass('display_none');
                $('.location_province').attr('checked', true);
                alert("geolocation: false");
            }
        }

        function success(position)
        {
           
            document.getElementById('long').value = position.coords.longitude;
            document.getElementById('lat').value = position.coords.latitude;
                    
            // The location of Uluru
            var uluru = {lat: position.coords.latitude, lng: position.coords.longitude};
            // The map, centered at Uluru
            map = new google.maps.Map(
                document.getElementById('map_wtp'), {zoom: 10, center: uluru});
            // The marker, positioned at Uluru
            var marker = new google.maps.Marker({icon: 'https://maps.google.com/mapfiles/ms/icons/red-dot.png', position: uluru, map: map});
              
        }

        function fail( fail )
        {
          
            console.log(fail.message);
           alert('Error Message: ' + fail.message);
           alert('Error Code: ' + fail.code);

            // The location of Uluru
            var uluru = {lat: -33.9396748, lng: 18.850258};

            // The map, centered at Uluru
            map = new google.maps.Map(
                document.getElementById('map_wtp'), {zoom: 10, center: uluru});
            
            // Could not obtain location
            $('.location_checkbox').addClass('display_none');
            $('.location_province').attr('checked', true);
            $('.use_selected').removeClass('display_none');
            $('.use_current').addClass('display_none');
            current_search_type = 'USE_PROVINCE';
            $('.searching_loader').addClass('display_none');
        }

        

        // var lastScrollTop = 0;
        // $('.outlet_results').scroll(function(event){
        // var st = $(this).scrollTop();
        // if (st > lastScrollTop){
        //     // downscroll code
           
        //     $('.wtb-top').addClass('display_none');
        // } else {
        //     // upscroll code
        //     $('.wtb-top').removeClass('display_none');
        // }
        // lastScrollTop = st;
        // });

        function sortUL(selector) {
            $(selector).children("option").sort(function(a, b) {
                var upA = $(a).text().toUpperCase();
                var upB = $(b).text().toUpperCase();
                return (upA < upB) ? -1 : (upA > upB) ? 1 : 0;
            }).appendTo(selector);

            var newBal = $(selector).val($("#city_select option:first").val());
            city_select.text(newBal)
            
        }
        
        
    });
</script>

<!-- Replace the value of the key parameter with your own API key. -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_wdPZDg6SRVsrB2poUeDAm38U_WTuxdw">
</script>