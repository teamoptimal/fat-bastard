<?php
	session_start();
	include '../../includes/config.php';
	include '../../includes/head.php';
	$_SESSION['yes'] = 'fat-bastard-chardonnay';
?>
<script>
   //if($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes'){$(location).attr('href', ara);}
</script>

<?php
	include '../../includes/header_nav.php';
?>
<div class="main-content bastards-copy">
<div>
	<h1 class="highlight mobile-hide">The FAT bastards</h1>
	<p>The only thing better than a <span class="highlight sm-c">Glass Of Wine</span> with wonderful colour <br class="mobile-hide">and rich, round palate is another glass of the <i class="highlight">same wine.</i></p>
	<br>
</div>
<div class="clearfix"></div>
</div>
<div class="wine-headline hidden-desktop">
	<h1>The FAT bastards</h1>
</div>
<div class="wines">
<div class="wine-lightbox" style="display:block;" id="fbChardonay">
	<div class="left">Chenin Blanc
		<a class="fancy-close wine-fancy" rel="packshot1" href="#chenin_packshot">
			<div class="download">
			</div>
		</a>
		<a class="fancy-close wine-fancy" rel="packshot" href="#chenin_packshot">
			<div class="bottle-container">
				<img src="../../elements/wines/large/chenin.jpg" alt="">
			</div>
		</a>
	</div>
	<div class="right">
		<div class="top">
			<div class="heading fb-chardonnay">
				<h1>Chenin Blanc</h1>
			</div>
		</div>
		<div class="middle" style="font-size: 1rem !important;">
			<p>A FAT bastard as lively as a golden ray of sunshine.</p>
            <p>We’ve created some delightful wines in our time, but this Chenin Blanc is truly special. From the outset our aim was to produce something extraordinary. We took our best wines from the year’s ripe harvest and blended them together to create a lively wine with intense fruit flavour and a charisma to match.</p>
            <p>We harvested our highest quality grapes in the coolness of the morning and pressed them immediately to allow the delicate aromas and fruity flavours that are naturally present in the grapes to be expressed in the wine and experienced every time our FAT bastard Chenin Blanc caresses your lips.</p>
            <p>It all comes together in that very first sip: juicy yellow peach and a hint of vanilla dance across the palate and bring with them a tangy note of fresh citrus on the finish. A truly sumptuous sip of sunshine.</p>
		</div>
		<div class="bottom">
            <!--
			<h3>TASTING NOTE
			<br class="hidden-desktop">
			<a target="_blank" href="<?php echo $base_path; ?>pdf/tasting_notes/chardonnay/tasting_note/chardonnay2018.pdf"><span class="year fb-chardonnay">2018</span></a>
			<div class="archive-break"></div>
			<span class="archive-container">
				<span class="archive">ARCHIVE</span>
					<a target="_blank" href="<?php echo $base_path; ?>pdf/tasting_notes/chardonnay/tasting_note/FAT-B-chard-17.pdf"><span class="year fb-chardonnay">2017</span></a>
					<a target="_blank" href="<?php echo $base_path; ?>pdf/tasting_notes/chardonnay/archive/chard-16-nps.pdf"><span class="year fb-chardonnay">2016</span></a>
					<a target="_blank" href="<?php echo $base_path; ?>pdf/tasting_notes/chardonnay/archive/chard-15-nps.pdf"><span class="year fb-chardonnay">2015</span></a>
				<div class="archive-set">
					<a target="_blank" href="<?php echo $base_path; ?>pdf/tasting_notes/chardonnay/archive/FAT-B-chard-14.pdf"><span class="year fb-chardonnay">2014</span></a>
					<a target="_blank" href="<?php echo $base_path; ?>pdf/tasting_notes/chardonnay/archive/FAT-B-chard-13-mrt14.pdf"><span class="year fb-chardonnay">2013</span></a>
					<a target="_blank" href="<?php echo $base_path; ?>pdf/tasting_notes/chardonnay/archive/ROB-tn-fb-chard-12-2.pdf"><span class="year fb-chardonnay">2012</span></a>
				</div>
				<div class="archive-set">
					<a target="_blank" href="<?php echo $base_path; ?>pdf/tasting_notes/chardonnay/archive/ROB-tn-fb-chard-11.pdf"><span class="year fb-chardonnay">2011</span></a>
				</div>
				</div>
			</span>
			
			</h3> -->
		</div>
	</div>
</div>

<div class="to-wine">
<a href="../the_fat_bastards.php"><button class="gold">Back to wines</button></a>
</div>

<div class="wine-container stamp-container">
<img class="stamp" src="../../elements/stamp_3.png" alt="">
</div>

</div>

<div class="clearfix"></div>
		</div>
	</div>
<?php
include '../../includes/page_pieces/wine_lightboxes.php';
include '../../includes/page_pieces/wine_packshots.php';
include '../../includes/footer.php';
?>
<script>
	$('a.bastards').css({
		'font-weight': '800',
		'color': '#EAB332'
	});
</script>

<?php 
 ?>

