<?php
	include '../../includes/config.php';
	include '../../includes/head.php';
session_start();
$_SESSION['yes'] = 'goldenreserve';
?>
<script>
   //if($.cookie('is_legal') === '' || $.cookie('is_legal') === 'undefined' || $.cookie('is_legal') !== 'yes'){$(location).attr('href', ara);}
</script>

<?php
	include '../../includes/header_nav.php';
?>
<div class="main-content bastards-copy">
<div>
	<h1 class="highlight mobile-hide">The FAT bastards</h1>
	<p>The only thing better than a <span class="highlight sm-c">Glass Of Wine</span> with wonderful colour <br class="mobile-hide">and rich, round palate is another glass of the <i class="highlight">same wine.</i></p>
	<br>
</div>
<div class="clearfix"></div>
</div>
<div class="wine-headline hidden-desktop">
	<h1>The FAT bastards</h1>
</div>
<div class="wines">
<div class="wine-lightbox" style="display:block;" id="shiraz">
	<div class="left">
		<a class="fancy-close wine-fancy" rel="packshot1" href="#golden-reserve_packshot">
			<div class="download">
			</div>
		</a>
		<a class="fancy-close wine-fancy" rel="packshot" href="#golden-reserve_packshot">
			<div class="bottle-container">
				<img src="../../elements/wines/large/golden-reserve.jpg" alt="">
			</div>
		</a>
	</div>
	<div class="right">
		<div class="top">
			<div class="heading pinotage">
				<h1>Golden Reserve</h1>
			</div>
		</div>
		<div class="middle" style="font-size: 1rem !important;">
			<p>The Cabernet Sauvignon / Merlot blend was made from specially selected grapes with the aim to produce ripe-flavoured and silky textured wine.</p>
			<p>All fruit was gently pressed to retain fruit aromas and character. The wine is aged in small oak barrels to add greater weight and complexity.</p>
			<p>The wine has intense, dark ruby colour. Luxurious aromas and flavours of dark berry fruit and plum entice the senses. The palate of the wine is rich and full with silky and lasting finish.</p>
		</div>
		<div class="bottom">
			<h3>TASTING NOTE <br class="hidden-desktop"> 
				<a target="_blank" href="<?php echo $base_path; ?>pdf/tasting_notes/greserve/tasting_note/g-reserve-17-nps.pdf">
					<span class="year pinot-rose">2017</span>
				</a>
			<div class="archive-break"></div>
			</h3>
		</div>
	</div>
</div>

<div class="to-wine">
<a href="../the_fat_bastards.php"><button class="gold">Back to wines</button></a>
</div>

<div class="wine-container stamp-container">
<img class="stamp" src="../../elements/stamp_3.png" alt="">
</div>

</div>

<div class="clearfix"></div>
		</div>
	</div>
<?php
include '../../includes/page_pieces/wine_lightboxes.php';
include '../../includes/page_pieces/wine_packshots.php';
include '../../includes/footer.php';
?>
<script>
	$('a.bastards').css({
		'font-weight': '800',
		'color': '#EAB332'
	});
</script>

<?php 
 ?>

