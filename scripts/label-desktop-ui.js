    //desktop ui
$(document).ready(function(){
    // FIRST NEXT BUTTON
    $('#create-next').click(function(e) {
        e.preventDefault();
        $('#create').removeClass('active');
        $('.create-nav').addClass('active');
        $('.create-nav').addClass('yellow');
        $('#details').addClass('active');
    });

    // BACK BUTTON ON DETAILS TAB
    $('#details-back').click(function(e) {
        $('.create-nav').removeClass('active');
        $('#details').removeClass('active');
        $('.create-nav').removeClass('yellow');
        $('#create').addClass('active');
        $('.details-nav').removeClass('active');
        $('.details-nav').removeClass('yellow');
    });

    // NEXT BUTTON ON DETAILS TAB
    $('#details-next').click(function(e) {
        $('#create').removeClass('active');
        $('.create-nav').removeClass('active');
        $('.create-nav').removeClass('yellow');
        $('#details').removeClass('active');
        $('.details-nav').addClass('active');
        $('.details-nav').addClass('yellow');
        $('#occasion').addClass('active');
        $('.occasion-nav').removeClass('active');
        $('.occasion-nav').removeClass('yellow');
        
    });

    // BACK BUTTON ON WINES TAB -- TO DOOO
    $('#occasion-back').click(function(e) {
        $('.occasion-nav').removeClass('active');
        $('.occasion-nav').removeClass('yellow');
        $('#occasion').removeClass('active');
        $('.details-nav').removeClass('active');
        $('.details-nav').removeClass('yellow');
        $('.create-nav').addClass('active');
        $('.create-nav').addClass('yellow');
        $('#details').addClass('active');

        /*$('.occasion-nav').removeClass('active');
        $('.occasion-nav').removeClass('yellow');
        $('#occasion').removeClass('active');
        $('.details-nav').removeClass('active');
        $('.details-nav').removeClass('yellow');
        $('.create-nav').addClass('active');
        $('.create-nav').addClass('yellow');
        $('#details').addClass('active');
        */
    });

    // NEXT BUTTON ON WINES TAB
    $('#occasion-next').click(function(e) {
        $('.details-nav').removeClass('active');
        $('.details-nav').removeClass('yellow');
        $('#wines').removeClass('active');
        $('.occasion-nav').addClass('active');
        $('.occasion-nav').addClass('yellow');
        $('#design').addClass('active');
    });

    // BACK BUTTON ON MESSAGE TAB - TO DOOOO
    $('#design-back').click(function(event) {
        $('.occasion-nav').removeClass('active');
        $('.occasion-nav').removeClass('yellow');
        $('#design').removeClass('active');
        $('.details-nav').addClass('active');
        $('.details-nav').addClass('yellow');
        $('#occasion').addClass('active');
    });

$('.design-next').click(function(event) {
    $('#postcard-share').empty();
    html2canvas($('#postcard'), {
        onrendered: function(canvas) {
            var postcardBase64 = canvas.toDataURL();
            var postcardFinal = new Image();
            postcardFinal.src = postcardBase64;

            $('#img_val').val(canvas.toDataURL("image/png"));
            img_val = $('#img_val').val();
            imgString = 'img_val='+img_val+'';

            image_id = $('.postcard-eg.active').data('id');
            label_to = $('.user-heading-input').val();
            data_copy = $('.label_copy').val();
            label_copy = data_copy.replace(/\n/g, "<br>");

            // $.post( "save.php", { img_val: img_val} );
            //$('.tab-content').load('save.php?img_val='+img_val);
            $.ajax({
                type: 'POST',
                url: 'mobile_save_label.php',
                data: { img_val: img_val, image_id: image_id, label_to:label_to, label_copy: label_copy } ,//'img_val='+img_val,
                success: function(data) {   
                   // alert(data);
                    //$('.tab-content').html(data);
                    $('.user_share').html(data);
                },
                error:function(data)
                {
                    alert("Unable to render image as png");
                }
            });
            postcardFinal.src = img_val;
            // $('#postcard-share').append(postcardFinal);
            $('img[src="data:,"]').css("display","none");
        }
    });

    $('.occasion-nav').removeClass('active');
    $('.occasion-nav').removeClass('yellow');
    $('#design').removeClass('active');
    $('.share-nav').addClass('yellow');
    $('.share-nav').addClass('active');
    $('#share').addClass('active');
    $('.share-nav').css({
        'box-shadow': '0px 2px 4px rgba(208, 208, 208, 1)',
        'margin': '0'
    });
    $('.live-large-nav a').addClass('amazing');
    $('.live-large-nav a').css('cursor', 'pointer');
    $('.live-large-nav a').css('pointer-events', 'auto');
    $('.live-large-nav a').attr('data-toggle', 'tab');

});

    // BACK BUTTON ON SHARE TAB - TO DOOO
    $('#share-back').click(function(event) {
        $('.share-nav').removeClass('yellow');
        $('.share-nav').removeClass('active');
        $('#share').removeClass('active');
        $('.occasion-nav').addClass('yellow');
        $('.occasion-nav').addClass('active');
        $('#design').addClass('active');
    
    });

});

